Title: projet-revue

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Projet de revue en ligne","width":"1032px","height":"125px","transform":"translate(0px, 60px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681464683867","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## \u00c9quipe","width":"1032px","height":"99.4px","transform":"translate(0px, 948px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681464720469","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Comit\u00e9 de r\u00e9daction\n**Membres**\nC\u00e9cile Fournel, C\u00e9dric Mazet, Wafa Abida, \u00c9lise Garraud, Adrien Payet.\n\n**Responsabilit\u00e9**\nLe comit\u00e9 de r\u00e9daction \u00e9labore la question ouverte annuelle, acte la publication des textes re\u00e7us en faisant au besoin appel au conseiller scientifique.","width":"504px","height":"303.5px","transform":"translate(0px, 1068px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681464808287","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Conseiller scientifique\nPierre-Damien Huyghe\n\n**Responsabilit\u00e9**\nEn cas de difficult\u00e9 \u00e0 d\u00e9cider, le comit\u00e9 de r\u00e9daction peut faire appel au conseiller scientifique pour trancher.","width":"504px","height":"249.5px","transform":"translate(528px, 1068px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681465126397","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Ligne \u00e9ditoriale\nCritique, modernit\u00e9.\n\nPas de th\u00e8me, plut\u00f4t des questions.","width":"504px","height":"181.4px","transform":"translate(0px, 216px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681466284937","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Principe de publication\nUne question est ouverte chaque ann\u00e9e, associ\u00e9es \u00e0 la publication d'un premier groupe de textes \u00e9crits par des personnes invit\u00e9es voire des membres de l'\u00e9quipe de la revue (facultatif).\nLes questions ouvertes ne sont jamais referm\u00e9es, il est toujours possibles qu'elles accueillent de nouveaux textes.\nInformation par mail \u00e0 chaque publication (type newsletter).\nCombien de fourn\u00e9es de textes par ann\u00e9e ?","width":"504px","height":"437.8px","transform":"translate(528px, 216px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681466370470","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Types d'\u00e9crits\n- Critiques d'\u0153uvres\n- Relectures critiques\n- Actualisations de textes anciens\n- Critiques du vocabulaire en usage","width":"504px","height":"221px","transform":"translate(0px, 432px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681466931629","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Nom\n\"Cahier de la modernit\u00e9\"\n--> \"cahier\", est-ce bien moderne ?\n--> Veut-on afficher le mot de \"modernit\u00e9\" ?","width":"504px","height":"199.4px","transform":"translate(528px, 672px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681468228504","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Statut : association ?","width":"504px","height":"148.8px","transform":"translate(0px, 684px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681468502851","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Support : site internet\nImprimable, exportable (a minima PDF et epub).","width":"500px","height":"194.8px","transform":"translate(1056px, 216px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681468530201","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Rubriques\n2 rubriques articul\u00e9es :\n- Actualit\u00e9 de l'inactuel\n- Inactualit\u00e9 de l'actuel\n\nAutres rubriques envisag\u00e9es :\n- Critique d'\u0153uvres \/ d'objets ?","width":"500px","height":"280.4px","transform":"translate(1056px, 432px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1681468897112","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: true

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-04-14

----

Uuid: cL11iRfWJfd6sdUC

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris