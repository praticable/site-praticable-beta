Title: 2 ateliers à Ethics by design

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# 2 ateliers","width":"600px","height":"125px","transform":"translate(72px, 312px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679321568528","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Le vendredi 12 mai 2017, nous avons organis\u00e9 et anim\u00e9 deux ateliers \u00e0 l\u2019\u00e9v\u00e9nement [Ethics by design](https:\/\/2017.ethicsbydesign.fr\/), la premi\u00e8re conf\u00e9rence d\u00e9di\u00e9e \u00e0 la conception num\u00e9rique durable en France, men\u00e9 par Designers \u00c9thiques et Flupa, \u00e0 l\u2019\u00c9cole Normale Sup\u00e9rieur de Lyon, sur le design de l\u2019attention et les formes de commerce en design.\n\n> Peut-on tout faire sur Internet ? Les entreprises n\u00e9es avec le num\u00e9rique peuvent-elles s\u2019affranchir des contraintes fiscales, juridiques, sociales qui incombent ? Les designers sont-ils des manipulateurs ? Sommes-nous vol\u00e9s de notre temps et de notre attention comme le sugg\u00e8re [Tristan Harris](https:\/\/www.ted.com\/talks\/tristan_harris_how_better_tech_could_protect_us_from_distraction?language=fr) ? Qu\u2019est ce qu\u2019un bon design ? Autant de questions autour de l\u2019Ethics by Design que nous vous proposons d\u2019aborder en vous accueillant \u00e0 l\u2019\u00c9cole Normale Sup\u00e9rieure de Lyon, au cours d\u2019une journ\u00e9e de conf\u00e9rences et d\u2019ateliers \u00e0 destination des professionnels du num\u00e9rique et du monde de la recherche.","width":"600px","height":"440.5px","transform":"translate(72px, 468px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679321865088","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/lgdV80qAqK6NNcn9"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"600px","height":"200.333px","transform":"translate(72px, 84px)","zindex":0,"iscover":"true"},"id":"c8f1aa74-f96a-40ac-8ff2-3dfda08f3e72","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## Du commerce _\u00e9thique_\n\nLe premier atelier, intitul\u00e9 \u00ab Du commerce _\u00e9thique_ \u00bb, a pour objectif de recueillir des pratiques et formes de commerce en design, souhaitables pour l\u2019ensemble des collaborateurs du designer. Cet atelier a d\u00e9but\u00e9 par une \u00e9tape d\u2019identification des diff\u00e9rents probl\u00e8mes du commerce en design. Une fois retranscrites (sur les surfaces de la [Tomette](https:\/\/praticable.fr\/productions\/tomette)), les participants les ont regroup\u00e9s en quatre cat\u00e9gories : acculturation, communication, finance et gestion. Ils ont par la suite proposer et \u00e9changer des solutions pour pallier aux probl\u00e8mes pr\u00e9c\u00e9demment identifi\u00e9s en remplissant des fiches \u00ab bonnes pratiques \u00bb.\n\nL\u2019atelier a pour mission de recueillir des pratiques qui tendent \u00e0 maintenir et \u00e0 am\u00e9liorer des formes de commerce en design. \u00c0 l\u2019issue de l\u2019atelier, nous avons produit un document qui pr\u00e9sente les astuces, outils, m\u00e9thodes et pratiques qui peuvent faciliter et \u00e9quiper les temps de commerce entre les designers et ses collaborateurs.\n\n> \u00ab Commercer, ce n\u2019est pas seulement vendre ou parvenir \u00e0 vendre, c\u2019est aussi \u00e9changer. \u00ab Avoir commerce avec quelqu\u2019un \u00bb est une expression qui s\u2019entendait autrefois. Cette expression est aujourd\u2019hui peu usit\u00e9e. Elle n\u2019est pas n\u00e9cessairement p\u00e9rim\u00e9e dans le principe. N\u2019oublions pas au reste que le commerce ne se passe pas seulement entre des humains, il se passe aussi avec les objets. Ainsi me semble-t-il envisageable de proposer comme occasions de commerce des situations techniques qui ne sont pas li\u00e9es \u00e0 l\u2019assignation des usages. \u00bb\n>\n><cite>-- Pierre-Damien Huyghe, [_Plaidoyer pour une technique hospitalisable_] (http:\/\/laviemanifeste.com\/archives\/5263), Paris, 2011, consult\u00e9 le 10 mai 2017.<\/cite>","width":"600px","height":"863.9px","transform":"translate(72px, 984px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679322036275","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### La restitution de l\u2019atelier\n\nCe recueil restitue les bonnes pratiques qui ont \u00e9t\u00e9 propos\u00e9es dans le cadre de l\u2019atelier \u00ab Les formes de commerce \u00e9thique en design \u00bb. Apr\u00e8s avoir identifi\u00e9 les probl\u00e8mes qui survenaient dans le commerce en design, les participants de l\u2019atelier les ont regroup\u00e9s en quatre grandes cat\u00e9gories :\n\n- L\u2019acculturation\n- La communication\n- La finance\n- La gestion\n\nIls ont par la suite commenc\u00e9 \u00e0 proposer des solutions pour pallier aux probl\u00e8mes pr\u00e9c\u00e9demment identifi\u00e9s en remplissant des fiches \u00ab bonnes pratiques \u00bb. Libre \u00e0 chacun des participants de compl\u00e9ter, d\u2019enrichir et de partager ce document !","width":"600px","height":"472.1px","transform":"translate(72px, 1884px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679323393882","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/as0MENUAKY9IkZky"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"402.4px","transform":"translate(72px, 2376px)","zindex":0,"iscover":"false"},"id":"82b9d53a-9c13-4f82-a7e9-a374aa29f7a3","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/oKQUCWaCy4gacoUk"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"402.4px","transform":"translate(384px, 2376px)","zindex":0,"iscover":"false"},"id":"f0ce6726-d356-4193-925f-020d690211ee","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"##### Les outils de l\u2019atelier","width":"372px","height":"79px","transform":"translate(72px, 2808px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679326568738","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/pbyBhQddBUhtrIOi"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p><a href=\"https:\/\/praticable.fr\/interventions\/ateliers-ethics-by-design\/praticable-outil-presentation-commerce-design.pdf\" rel=\"noopener noreferrer\">Pr\u00e9sentation de l\u2019atelier<\/a><\/p>","link":"","ratio":"","crop":"false","width":"288px","height":"473.183px","transform":"translate(72px, 2904px)","zindex":0,"iscover":"false"},"id":"572280f8-886a-43e7-9330-ad59d70322a2","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/Z85M7fiagAfmauva"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p><a href=\"https:\/\/praticable.fr\/interventions\/ateliers-ethics-by-design\/praticable-outil-fiche-commerce-design.pdf\">Pratique de commerce en design<\/a><\/p>","link":"","ratio":"","crop":"false","width":"288px","height":"494.183px","transform":"translate(384px, 2904px)","zindex":0,"iscover":"false"},"id":"4de51bd4-1ea2-4921-b85e-54265de8d919","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## Cartographie des pathologies de l\u2019attention\n\nLe deuxi\u00e8me atelier, intitul\u00e9 \u00ab Cartographie des pathologies de l\u2019attention \u00bb, que nous avons organis\u00e9 avec la Fing, a pour objectif de cartographier les grandes transformations de l\u2019attention \u00e0 l\u2019\u00e8re num\u00e9rique en utilisant la Tomette.\nDe nouvelles souffrances, nouvelles pathologies, nouvelles addictions \/ De nouvelles zones de confort, bien \u00eatre \/ De nouvelles valeurs, nouvelles r\u00e9tributions \/ De nouvelles m\u00e9triques, nouveaux indicateurs \/ Quelles sont les formes de l\u2019attention (sociale, individuelle, collective, conjointes, organisationnelles) \/ Qui sont les acteurs ? \/ Quelles solutions (d\u00e9connexion, \u00e9cologie informationnelle, afficher le temps,\u2026) \/ De nouveaux m\u00e9tiers (magiciens, coach du temps,\u2026).","width":"600px","height":"541.2px","transform":"translate(720px, 984px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679333946274","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-03-20

----

Uuid: gSKdJR10MNev7htx

----

Coverthumb: https://praticable.fr/media/pages/interventions/ateliers-ethics-by-design/0ee3397fc9-1679321918/ethicsbydesign-couverture-2.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris