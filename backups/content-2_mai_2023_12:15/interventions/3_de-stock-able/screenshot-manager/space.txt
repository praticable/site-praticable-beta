Title: Screenshot Manager

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Screenshot Manager","width":"576px","height":"200px","transform":"translate(72px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"d74ece74-d036-412d-8a2e-3e07ec8f5e26","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Faciliter le tri, la suppression et la qualification des screenshots","width":"576px","height":"247.6px","transform":"translate(72px, 336px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1680596837019","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Le projet\n\n\u00c0 l\u2019occasion d\u2019une r\u00e9flexion autour du stockage dans notre quotidien, l\u2019introspection et l\u2019analyse de nos propres usages nous ont amen\u00e9s \u00e0 questionner la pellicule des smartphones, et la part importante des m\u00e9dias dans les donn\u00e9es que nous stockons quotidiennement. La galerie d\u2019images est compos\u00e9e de diff\u00e9rents types de prises de vue, parmi lesquelles on retrouve les captures d\u2019\u00e9cran ou screenshots. Il s\u2019agit de l\u2019enregistrement de l'image affich\u00e9e \u00e0 l'\u00e9cran, sous la forme d'un fichier graphique. La capture d\u2019\u00e9cran est la \u00ab copie d\u2019une copie \u00bb, objet de diverses utilisations qui la rendent souvent d\u00e9nu\u00e9e d\u2019affect et ainsi plus facilement jetable. En r\u00e8gle g\u00e9n\u00e9rale, la capture d\u2019\u00e9cran a un statut \u00e9ph\u00e9m\u00e8re selon son usage, mais son stockage a tendance \u00e0 devenir p\u00e9renne, tendant alors vers une accumulation de fichiers.\n\n##### Comment peut-on alors favoriser le tri des captures d\u2019\u00e9cran en repensant leur valeur et usage ?\n\nDeux types d\u2019actions sont ainsi propos\u00e9s pour r\u00e9pondre \u00e0 cette probl\u00e9matique : interroger l\u2019usage au moment du _screenshot_ et faciliter le tri de ces images p\u00eale-m\u00eale.\n\n1. La premi\u00e8re piste consiste \u00e0 proposer de nouveaux choix lors de la capture, avec la possibilit\u00e9 d\u2019affecter une date limite \u00e0 l\u2019image par une programmation de suppression 30 jours apr\u00e8s la prise. Cette option permet \u00e0 l\u2019usager de qualifier l\u2019image comme \u00e9tant temporaire selon son emploi, et de s\u2019en lib\u00e9rer automatiquement. De plus, le moment de la capture est optimis\u00e9 par une facilitation du partage dans l\u2019interface, \u00e9vitant ainsi de garder des fichiers destin\u00e9s uniquement \u00e0 l\u2019envoi.\n2. La seconde piste pour faciliter le tri est de rendre l\u2019action plus interactive. La cr\u00e9ation d\u2019une fen\u00eatre de tri offre ainsi la possibilit\u00e9 de faire une s\u00e9lection plus ais\u00e9e de l\u2019album de captures d\u2019\u00e9cran. L\u2019interface repense la pr\u00e9sentation des images pour donner plus de lisibilit\u00e9 que le format grille et invite l\u2019usager \u00e0 se lib\u00e9rer du contenu inutile par un geste de balayage de l\u2019\u00e9cran.","width":"576px","height":"1101px","transform":"translate(72px, 612px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"00c7516d-29a4-49c6-8141-e40e6e0f39b4","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/RqUSzR7FGRqCRFX8"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"450px","height":"2px","transform":"translate(696px, 492px)","zindex":0,"iscover":"true"},"id":"926d52fe-9642-4fe7-b99d-841e7d6ac25b","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"##### Infos\n- \u00c9tudiant\u00b7e\u00b7s : **Pandora Bec & Rayane Benhizia**\n- [Licence CC BY NC SA](https:\/\/creativecommons.org\/licenses\/by-nc-sa\/2.0\/fr\/)\n\n##### Livrables\n\n- [Wireframe - Prises de captures d\u2019\u00e9cran](https:\/\/www.figma.com\/file\/FVF5aP1IENtMueJ14AAn6e\/Prise-de-capture-d'%C3%A9cran?t=4FvjMt01e80FZArw-6)\n- [Wireframe - Tri des captures d\u2019\u00e9cran](https:\/\/www.figma.com\/file\/jpzH75ap4HvxYjn8d4mfCa\/Tri-de-capture-d'%C3%A9cran?t=4FvjMt01e80FZArw-6)","width":"288px","height":"349.75px","transform":"translate(696px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"0314d4c6-9e65-4b2d-b334-b1709b234119","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-04-04

----

Uuid: a9EOHNBiXSdJrjX5

----

Coverthumb: https://praticable.fr/media/pages/interventions/de-stock-able/screenshot-manager/78d8be8f3f-1680597179/screenshot-manager-typologie-captures-ecran.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris