Title: Croc Data

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Croc Data","width":"576px","height":"125px","transform":"translate(72px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"9e861dfb-a38f-4253-afae-08baa3ff2efc","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Nourrir une plante carnivore avec des pages web","width":"576px","height":"198.2px","transform":"translate(72px, 264px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"56c52755-fa6b-45b5-9d3c-c7477684b51b","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Le projet\n\nAujourd\u2019hui nous chargeons des pages web sans m\u00eame savoir ce que cette action implique du point de vue du poids des donn\u00e9es. Ce probl\u00e8me d\u2019ignorance et d'invisibilisation des diff\u00e9rences entre certaines pages web emp\u00eache l\u2019utilisateur de se servir consciemment de sa navigation internet. Notamment, il ignore l'impact li\u00e9 aux choix de navigation sur certaines pages web.\n\nIl existe plusieurs raisons pour lesquelles certaines pages sont plus lourdes, mais elles sont pour la plupart trop techniques pour \u00eatre saisies par un plus grand nombre. Il est possible en revanche de se faire une id\u00e9e des diff\u00e9rences en comparant les pages web entre elles par le biais de la visualisation associ\u00e9e \u00e0 certaines actions.\n\nCroc Data est une extension de navigateur qui vise \u00e0 montrer aux utilisateurs les diff\u00e9rents poids des pages web lors de leur navigation internet. Cette extension incite \u00e0 nourrir une \u201ccr\u00e9ature plante carnivore\u201d avec des pages web plus ou moins volumineuses. Plus la page est lourde, plus la cr\u00e9ature grossie, et au contraire moins la page est volumineuse moins la cr\u00e9ature est satisfaite.\n\nL\u2019enjeu est de sensibiliser sans positionnement moralisateur par la satire sur la diff\u00e9rence de poids des pages internet que l\u2019on parcourt tout au long de la journ\u00e9e. Face au _green washing_ de certains navigateurs qui jouent sur la bonne conscience des utilisateurs en s\u2019engageant sur la conversion de donn\u00e9es en action \u00e9cologique; ici Croc Data prend le contre pied en poussant l\u2019utilisateur \u00e0 trouver les pages les plus lourdes sur le web. Prendre le probl\u00e8me \u00e0 l\u2019inverse en abordant une posture cynique devient bien plus int\u00e9ressant d\u2019un point de vue divertissant et r\u00e9cr\u00e9atif, et ainsi l\u2019implication de l\u2019utilisateur est bien plus sollicit\u00e9e. Ainsi la connaissance des pages lourdes par le biais de la comparaison est engag\u00e9e, l\u2019utilisateur ressort plus conscient sur les diff\u00e9rences de donn\u00e9es existantes.","width":"576px","height":"1023.5px","transform":"translate(72px, 492px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"db53462e-5784-477b-a0dd-a860e73a3a79","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### Infos\n- \u00c9tudiant\u00b7e\u00b7s : **Jeanne Guillory, Marie Sarah Legendre et Jeanne Truffy**\n- [Licence CC BY NC SA](https:\/\/creativecommons.org\/licenses\/by-nc-sa\/2.0\/fr\/)","width":"384px","height":"169.75px","transform":"translate(696px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"65e65eca-fa0e-434d-ae12-651f76e78845","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Pour le futur... \n\n- D\u00e9velopper une unit\u00e9 de mesure en fonction de la taille de la plante.\n- Cr\u00e9ation d'une plateforme de t\u00e9l\u00e9chargement pour l'installation de croc data.\n- Faire diverses enqu\u00eates utilisateur pour la viabilit\u00e9 de notre concept.\n- Animation du croc data qui vient manger l'URL du site pour mieux comprendre le principe de monstre mangeur de data","width":"576px","height":"320px","transform":"translate(72px, 1548px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1680604785499","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/ho2o4fkmKpG8D2ZC"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"864px","height":"2px","transform":"translate(696px, 960px)","zindex":0,"iscover":"false"},"id":"73502972-6732-40c5-8377-3d5d643fae9f","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/heoJqTi7HteFYwks"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"588px","height":"2px","transform":"translate(696px, 312px)","zindex":0,"iscover":"true"},"id":"ca99e011-99db-4ddc-9921-fc68f0da3e6f","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/Nx4Jlrp1brMnqzud"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"1116px","height":"2px","transform":"translate(696px, 552px)","zindex":0,"iscover":"false"},"id":"32c970cd-0550-4ab2-9062-1724aad8a254","isHidden":false,"type":"image"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-04-04

----

Uuid: f7YYBpX9Tx4R769V

----

Coverthumb: https://praticable.fr/media/pages/interventions/de-stock-able/croc-data/f92c108593-1680605342/croc-data-evolution-plante.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris