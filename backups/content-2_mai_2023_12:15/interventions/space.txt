Title: interventions

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Interventions","width":"576px","height":"125px","transform":"translate(48px, 84px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1677841567385","isHidden":false,"type":"markdown"},{"content":{"title":"design populaire","cover":"https:\/\/praticable.fr\/media\/pages\/interventions\/design-populaire\/55019343db-1679064421\/praticable-logo-france-design-week.png","intro":"Intervention \u00e0 la [**France Design Week**](https:\/\/francedesignweek.fr\/), \u00e0 Tours, en septembre 2022.","width":"264px","height":"489.5px","transform":"translate(936px, 264px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/interventions\/design-populaire","tags":"","linkedspaces":[]},"id":"interventions\/design-populaire","isHidden":false,"type":"representative"},{"content":{"title":"Empreinte du num\u00e9rique","cover":"https:\/\/praticable.fr\/media\/pages\/interventions\/empreinte-du-numerique\/764dfcce3d-1678892940\/cables-carte.jpg","intro":"","width":"400px","height":"381.733px","transform":"translate(48px, 264px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/interventions\/empreinte-du-numerique","tags":"","linkedspaces":[]},"id":"interventions\/empreinte-du-numerique","isHidden":false,"type":"representative"},{"content":{"title":"documentons le design sur wikip\u00e9dia","cover":"https:\/\/praticable.fr\/media\/pages\/interventions\/documentons-le-design-sur-wikipedia\/6a728d755e-1679317655\/praticable-design-wikipedia.jpg","intro":"","width":"400px","height":"435.8px","transform":"translate(48px, 684px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/interventions\/documentons-le-design-sur-wikipedia","tags":"","linkedspaces":[]},"id":"interventions\/documentons-le-design-sur-wikipedia","isHidden":false,"type":"representative"},{"content":{"title":"2 ateliers \u00e0 Ethics by design","cover":"https:\/\/praticable.fr\/media\/pages\/interventions\/ateliers-ethics-by-design\/0ee3397fc9-1679321918\/ethicsbydesign-couverture-2.jpg","intro":"","width":"420px","height":"265.65px","transform":"translate(1248px, 264px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/interventions\/ateliers-ethics-by-design","tags":"","linkedspaces":[]},"id":"interventions\/ateliers-ethics-by-design","isHidden":false,"type":"representative"},{"content":{"title":"(d\u00e9)stock-able","cover":"https:\/\/praticable.fr\/media\/pages\/interventions\/de-stock-able\/3d3b3808a3-1680608132\/destockable-cover.jpg","intro":"##### Workshop du 06\/03 au 10\/03\/2023 au Lyc\u00e9e F. Magendie \u00e0 Bordeaux, avec le [DNMAD Design objet et innovation sociale](https:\/\/dnmademagendie.wixsite.com\/portesouvertes\/dn-made-objet) (3e ann\u00e9e) et le [Master 1 - Interaction, innovation, service](https:\/\/master.designbordeaux.fr\/#) de l'Universit\u00e9 Bordeaux Montaigne.","width":"420px","height":"713.35px","transform":"translate(480px, 264px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/interventions\/de-stock-able","tags":"","linkedspaces":["page:\/\/txzvdcXFt69CDNxP","page:\/\/f7YYBpX9Tx4R769V","page:\/\/Oa8uIdWVSKYqzlUo","page:\/\/Qyb2ED4uOWenIVVZ","page:\/\/tlKbdrcEOVZJActH","page:\/\/a9EOHNBiXSdJrjX5"]},"id":"interventions\/de-stock-able","isHidden":false,"type":"representative"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-03-03

----

Uuid: s5jdGKCHyy0LKz7f

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris