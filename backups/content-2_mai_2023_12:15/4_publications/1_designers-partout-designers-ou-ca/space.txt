Title: designers partout, designers où ça ?

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Designers partout, design o\u00f9 \u00e7a ?","width":"720px","height":"200px","transform":"translate(84px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676043576974","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### Infos\n- Auteur : **Adrien Payet**\n- Date : Janvier 2020\n- [Medium](https:\/\/medium.com\/@adrienpayet\/designers-partout-design-o%C3%B9-%C3%A7a-454d8882228e)","width":"276px","height":"175px","transform":"translate(840px, 360px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676478266291","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"> \u00ab On appelle trop le design aujourd\u2019hui. Ne risque-t-il pas de se perdre \u00e0 vouloir r\u00e9pondre \u00e0 toutes les adresses qu\u2019on lui fait, \u00e0 tous les clins d\u2019\u0153il qu\u2019on lui adresse ? \u00bb.\n>\n> <cite>-- Pierre-Damien Huyghe, [_De la fiction \u00e0 la distanciation_](https:\/\/cutt.ly\/6stJqEe), 2018.<\/cite>","width":"720px","height":"165px","transform":"translate(84px, 360px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676477359685","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"On entend de plus en plus de non-designers pr\u00e9tendre appliquer des m\u00e9thodes de design. En m\u00eame temps, les designers interviennent de plus en plus comme consultants en id\u00e9ation. Et le design, dans tout \u00e7a ?","width":"720px","height":"131px","transform":"translate(84px, 552px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1676043652624","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Le 11 mars 2019, en titre d\u2019un [s\u00e9minaire](https:\/\/design-en-seminaire.ensci.com\/doku.php), l\u2019ENSCI posait une question inqui\u00e8te : \u201cDesign partout, designers nulle part ?\u201d Dans le texte introductif, les organisateurs constataient \u00ab une forme de flou, voire d\u2019incompr\u00e9hension, sur ce qu\u2019est le _design_ \u00bb en m\u00eame temps qu\u2019un inqui\u00e9tant \u00ab recours croissant au _design_ dans des espaces de pratiques qui ne semblent pas former de _designers_ \u00bb.\n\nL\u2019ENSCI est visiblement pr\u00e9occup\u00e9e par la tendance du design \u00e0 se r\u00e9pandre dans les \u00e9coles de commerce. Pr\u00e9occupation compr\u00e9hensible. Pas moins, cela dit, que l\u2019int\u00e9r\u00eat du commerce pour le design, dont l\u2019histoire est jalonn\u00e9e de succ\u00e8s commerciaux. La traduction du titre de l\u2019autobiographie de Raymond Loewy sonnait d\u00e9j\u00e0 comme une le\u00e7on aux marchands : _La laideur se vend mal_ (1953). Plus proche de nous, la r\u00e9ussite commerciale d\u2019Apple et la valeur du design dans les propos de Steve Jobs ont s\u00fbrement contribu\u00e9 \u00e0 renforcer dans les esprits le lien \u2013 la d\u00e9sormais quasi-confusion \u2013 entre design et innovation, et par cons\u00e9quent entre design et efficacit\u00e9 commerciale. Le design symbolise l\u2019innovation. Si bien que de nos jours, si la laideur se vend mal, le design, lui, se vend plut\u00f4t bien.","width":"720px","height":"464.5px","transform":"translate(84px, 720px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676043699155","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## La vogue probl\u00e9matique du design\n\nDesign de service, design d\u2019organisation, legal design, design fiction etc. Par d\u00e9clinaisons, le design \u00e9tend son offre et son panel de clients potentiels. Non seulement aux \u00e9coles de commerces mais \u00e0 tout type d\u2019organisations ; y compris de plus en plus aux administrations publiques, dont l\u2019int\u00e9r\u00eat s\u2019est officialis\u00e9 en grande pompe le 11 d\u00e9cembre dernier avec l\u2019inauguration, par le Minist\u00e8re de la Culture, des Assises du design.\n\n\u201cDesign partout, designers nulle part ?\u201d, demandait donc l\u2019ENSCI, et en effet des managers, des professionnels du marketing, des consultants, des communicants\u2026 bref, des non-designers font ce qu\u2019ils appellent du _design thinking_. En m\u00eame temps, la pr\u00e9sence de designers est tout de m\u00eame de plus en plus demand\u00e9e dans les \u00e9coles de commerces, dans tout type d\u2019entreprise ou d\u2019administration, sur tout type de projet. Et l\u2019on s\u2019en r\u00e9jouirait tout \u00e0 fait, si les designers n\u2019\u00e9taient pas trop souvent sollicit\u00e9s pour aider d\u2019autres \u00e0 essayer de penser un peu comme eux plut\u00f4t que pour faire du design, ce qui n\u2019est pas la m\u00eame chose. On entend donc bien des non-designers pr\u00e9tendre penser comme des designers. Mais on remarque aussi, et de plus en plus, des designers faire autre chose que du design.","width":"720px","height":"608.8px","transform":"translate(84px, 1236px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676043810937","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Le design courtis\u00e9 comme symbole\n\nIl semble qu\u2019on sollicite les designers parfois m\u00eame seulement pour \u00eatre l\u00e0. _\u00catre l\u00e0\u2026_ pour que l\u2019organisation puisse se targuer de travailler avec des designers. \u00catre l\u00e0 pour servir de caution innovation, pour labelliser les organisations. Et voil\u00e0 le probl\u00e8me \u00e0 son comble, lorsque le design tient moins sa valeur de son m\u00e9tier que de ce qu\u2019il symbolise, et qu\u2019on l\u2019exerce moins qu\u2019on l\u2019arbore. Sort qui, soit dit en passant, n\u2019est pas sans rappeler la fa\u00e7on dont certaines entreprises int\u00e8grent des philosophes, plut\u00f4t comme caution intellectuelle ou \u00e9thique quant \u00e0 eux.\n\nLa faute \u00e0 la demande, alors ? S\u00fbrement en partie, mais cela n\u2019arriverait pas sans le consentement tacite, voire la complicit\u00e9 de designers complaisants (et de philosophes, et d\u2019autres\u2026), ravis d\u2019\u00eatre courtis\u00e9s quelle qu\u2019en soit la raison. On les voit alors munis d\u2019outils color\u00e9s pour rafra\u00eechir des professionnels affadis, animer des ateliers durant lesquels ils s\u2019efforcent de faire faire \u00e0 d\u2019autres ce qu\u2019ils risquent de n\u2019\u00eatre bient\u00f4t plus capables de faire eux-m\u00eames, par manque de pratique : du design. Le m\u00e9tier s\u2019entretient par l\u2019exercice, ou s\u2019ab\u00eeme.","width":"720px","height":"554.8px","transform":"translate(84px, 1896px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676478922415","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## La r\u00e9duction du design \u00e0 une m\u00e9thode d\u2019id\u00e9ation\n\nAlors, du design, qu\u2019en est-il au juste ? L\u2019ENSCI relevait \u201cune forme de flou, voire d\u2019incompr\u00e9hension, sur ce qu\u2019est le _design_ \u201d. Ce qui s\u00e9vit, en particulier, c\u2019est une r\u00e9duction du design \u00e0 une _m\u00e9thode d\u2019id\u00e9ation_. Il y a l\u00e0 deux mots et deux probl\u00e8mes : r\u00e9duction du design \u00e0 une m\u00e9thode, r\u00e9duction du design \u00e0 une id\u00e9ation, au sens d\u2019un processus de production d\u2019id\u00e9es. Au sujet du premier probl\u00e8me, je me contenterai de renvoyer \u00e0 la r\u00e9flexion de Matthieu Savary (User Studio) dans l\u2019article [_Le design, une m\u00e9thode ?_](http:\/\/www.userstudio.fr\/blog\/le-design-peut-il-etre-reduit-a-une-methode\/). Quant au second \u2013 r\u00e9duction du design \u00e0 une id\u00e9ation \u2013, il pose d\u2019autant plus probl\u00e8me qu\u2019il est symptomatique d\u2019une humanit\u00e9 qui peine \u00e0 faire la diff\u00e9rence entre l\u2019id\u00e9e d\u2019une chose et la chose elle-m\u00eame.\n\nLa difficult\u00e9 n\u2019est pas neuve. C\u2019est en fait un vieux probl\u00e8me. Le mot de l\u2019ancien grec _eidos_, qu\u2019employait notamment Platon, h\u00e9site en fran\u00e7ais entre forme et id\u00e9e, selon les traductions. Comme si la forme n\u2019\u00e9tait qu\u2019id\u00e9ale, ou id\u00e9elle. Si tel \u00e9tait le cas, les designers, travailleurs de formes, pourraient peut-\u00eatre se contenter d\u2019avoir des id\u00e9es. Pourtant \u2013 et c\u2019est d\u2019ailleurs un enjeu majeur du fameux passage dit \u201cdes trois lits\u201d qui se situe au livre X de la _R\u00e9publique_ de Platon \u2013 de l\u2019id\u00e9e de la chose \u00e0 la chose elle-m\u00eame, ou de la forme abstraite \u00e0 la forme r\u00e9alis\u00e9e, il n\u2019y a pas rien. Il y a, en fait, un travail, qu\u2019un nombre pr\u00e9occupant de designers n\u00e9gligent ; pr\u00e9f\u00e9rant plut\u00f4t, moyennant _sprints_ et _hackathons_, courir (c\u2019est bien l\u2019image) aux solutions id\u00e9ales. Et pourquoi pas s\u2019y t\u00e9l\u00e9porter _imm\u00e9diatement_, et supprimer tout bonnement le probl\u00e8me du chemin qui s\u00e9pare de la solution \u00e0 construire.","width":"720px","height":"771.3px","transform":"translate(84px, 2496px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676044181170","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## La valeur des \u00e9tapes interm\u00e9diaires\n\nIm-m\u00e9diatement, c\u2019est-\u00e0-dire sans m\u00e9diation, aussit\u00f4t, instantan\u00e9ment, en br\u00fblant les \u00e9tapes, en faisant fi, comme si de rien n\u2019\u00e9tait du travail interm\u00e9diaire. De cette imm\u00e9diatet\u00e9, Platon \u2013 le m\u00eame philosophe qui, dans la _R\u00e9publique_, distingue entre la chose, son id\u00e9e et son image \u2013 semblait contenir la critique dans le dialogue du _Phil\u00e8be_. C\u2019est du moins ce que note Jank\u00e9l\u00e9vitch dans un [cours de 1960 sur l\u2019imm\u00e9diat](https:\/\/www.youtube.com\/watch?v=WrrCTf8qyyc&amp;t=181s). En lecteur attentif \u00e0 la r\u00e9currence d\u2019un adverbe qui signifie en grec \u201cimm\u00e9diatement\u201d, dont il rel\u00e8ve d\u2019ailleurs aussi la curieuse pr\u00e9sence dans la _R\u00e9publique_, Jank\u00e9l\u00e9vitch remarque (citation tronqu\u00e9e) :\n\n> Ce passage dans lequel Platon raille les impatients, les amateurs, les improvisateurs, les hommes press\u00e9s. Et il se sert plusieurs fois de suite d\u2019un adverbe qui veut dire en grec directement, imm\u00e9diatement, sans interm\u00e9diaire, tout de suite, s\u00e9ance tenante.\n> Donc Platon philosophe, dans ces pages, contre ceux qui passent en l\u2019esp\u00e8ce directement \u00e0 l\u2019unit\u00e9. C\u2019est-\u00e0-dire encore, ceux qui vont \u00e0 l\u2019un en br\u00fblant les \u00e9tapes, en escamotant les stations interm\u00e9diaires et, comme dit Platon, les moyens termes dont les hommes impatients et press\u00e9s veulent faire l\u2019\u00e9conomie. Les anges, qui sont des anges, peuvent se le permettre. Mais les hommes, qui ne sont pas des anges, en sont r\u00e9duits \u00e0 faire les anges. Celui qui veut faire l\u2019ange est un charlatant, tout simplement.\n\nLa suite ne m\u00e9nage pas moins ce \u201ccharlatan\u201d impatient dont il est question, tour \u00e0 tour qualifi\u00e9 d\u2019amateur, de faiseur de tours de passe-passe, de pitre, de clown, d\u2019acrobate, de marchand d\u2019orvi\u00e9tan\u2026 Jank\u00e9l\u00e9vitch pr\u00e9cise le probl\u00e8me par deux comparaisons. D\u2019abord avec un gu\u00e9risseur qui pr\u00e9tend gu\u00e9rir alors qu\u2019il ne conna\u00eet pas l\u2019organisme. Ensuite avec Icare, qui se br\u00fble les ailes pour n\u2019avoir pas fait l\u2019effort de sortir du labyrinthe comme il se doit, c\u2019est-\u00e0-dire en l\u2019\u00e9tudiant, en faisant des plans etc.\n\nPour gu\u00e9rir et pour sortir du labyrinthe, il manque \u00e0 ce gu\u00e9risseur et \u00e0 Icare une connaissance de l\u2019objet qui pose probl\u00e8me, la patience pour l\u2019\u00e9tudier, le go\u00fbt de ce travail. Par cons\u00e9quent, les solutions de ces charlatans n\u2019en sont pas, bien qu\u2019elles en aient l\u2019air. Les rem\u00e8des du gu\u00e9risseur n\u2019en ont que l\u2019air bien qu\u2019en r\u00e9alit\u00e9 ils ne gu\u00e9rissent pas et Icare le press\u00e9 ne s\u2019en sort au d\u00e9part qu\u2019apparemment, pour en fin de compte d\u00e9gringoler. Idem de bon nombre de ces pr\u00e9tendues solutions issues de s\u00e9ances d\u2019id\u00e9ation collectives. Elles en ont peut-\u00eatre parfois l\u2019air, du moins la pr\u00e9tention, mais elles n\u2019en sont pas tant qu\u2019elles ne sont pas r\u00e9alis\u00e9es, ce qui souvent ne sera pas fait. Et l\u2019on se contentera volontiers de ces id\u00e9es folles \u00e0 peine esquiss\u00e9es sur un _paperboard_ sous pr\u00e9texte qu\u2019innover c\u2019est savoir abandonner des id\u00e9es, comme disait Steve Jobs. Mais c\u2019est le m\u00e9tier qu\u2019on abandonne.","width":"720px","height":"1182.3px","transform":"translate(84px, 3312px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676477883101","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## Exercer patiemment le m\u00e9tier pour ne pas \u00eatre des charlatans\n\nPlaton pointe un risque qui nous regarde tous. Lorsqu\u2019il se r\u00e9alise, par impatience nous dit Platon, on a affaire \u00e0 des solutions qui n\u2019en sont pas et du savoir-faire qui manque de s\u2019entretenir, du m\u00e9tier qui se perd. Le risque existait de son temps et il existe toujours du n\u00f4tre pour la simple raison qu\u2019il rel\u00e8ve d\u2019une nature humaine capable de prendre les id\u00e9es et les paroles pour des faits, de se tromper et de tromper les autres. Nous pouvons toujours \u00eatre des charlatans, faire des promesses ou jouer \u00e0 imaginer des solutions plut\u00f4t que de faire laborieusement. Cela arrive en particulier lorsque, confondant profession et m\u00e9tier, nous continuons \u00e0 professer sans exercer nos m\u00e9tiers.\n\nLe design a bien le vent en poupe. Est-ce une raison suffisante pour se r\u00e9jouir ? Le design doit faire son m\u00e9tier, pas se r\u00e9pandre. Tant mieux s\u2019il peut faire l\u2019un et l\u2019autre. Mais \u00e0 choisir entre les deux : le m\u00e9tier.\n\n> \u00ab Ce qu\u2019il nous faut, c\u2019est une culture de menuisier \u00bb.\n>\n> <cite>-- Adolf Loos, _Ornement et crime_, Rivages, 2015.<\/cite>","width":"720px","height":"623.2px","transform":"translate(84px, 4536px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676478139438","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-01-09

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: 2WNqasM2GFBImzcE