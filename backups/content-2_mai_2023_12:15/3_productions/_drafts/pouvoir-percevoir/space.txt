Title: pouvoir percevoir

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"image":["file:\/\/YfVfC3ItatWT7uvs"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"732px","height":"518.517px","transform":"translate(48px, 432px)","zindex":0,"iscover":"true"},"id":"2abfdf6a-0bb0-423c-8d0e-b93e40ffbef5","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"# pourvoir percevoir","width":"732px","height":"125px","transform":"translate(48px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676649530780","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Enqu\u00eate et cahier d'id\u00e9es pour am\u00e9liorer l'**accessibilit\u00e9 des arts visuels et num\u00e9riques** pour les malvoyants et non-voyants.","width":"732px","height":"104px","transform":"translate(48px, 276px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1676649390474","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## multiplier les fa\u00e7ons de percevoir l'oeuvre\n\nPour nous, il s\u2019agit de travailler \u00e0 **\u00e9tendre le champ des points de vue possibles sur l\u2019\u0153uvre**. Chaque individu a une fa\u00e7on singuli\u00e8re de perce\u00b7voir. Chaque individu a une relation singuli\u00e8re \u00e0 l\u2019art. \n\nAu travers de nos propositions d\u2019id\u00e9es, nous chercherons \u00e0 multiplier les perceptions possibles : par la **multiplication de possibilit\u00e9s de r\u00e9glage de sa propre fa\u00e7on de percevoir**, par la **multiplication d\u2019entr\u00e9es, acc\u00e8s** \u00e0 des points de vue de l\u2019\u0153uvre et son exposition.\n\nPour cela nous travaillons sur des propositions \u2028de formes de m\u00e9diation, permettant de **singuli\u00e8rement se disposer face \u00e0 l\u2019\u0153uvre**, \u2028de singuli\u00e8rement percevoir et appr\u00e9cier l\u2019\u0153uvre, de singuli\u00e8rement r\u00e9gler l\u2019\u0153uvre, (et pourquoi pas r\u00e9gler l\u2019exposition aussi). Les publics, avec leurs propres caract\u00e9ristiques seront libres de s\u2019en saisir\u00a0ou non. \n\nCes propositions de points de vue diff\u00e9renci\u00e9s vont \u00e0 l\u2019**encontre d\u2019une pens\u00e9e qui suppose une v\u00e9rit\u00e9 unique de l\u2019\u0153uvre**. Au contraire, et en coh\u00e9rence avec l\u2019esprit qui guide la conception des expositions \u00e0 la Ga\u00eet\u00e9 Lyrique, nous proposons de favoriser des perceptions vari\u00e9es, amateures et\u00a0libres.\n\nNos propositions non-directives sont adaptables. Elles s\u2019appuient sur la **sollicitation des sens**. Elles sont sensorielles. Il pourra s\u2019agir d\u2019appareils, d\u2019\u00e9quipements, d\u2019exp\u00e9riences, de fa\u00e7ons d\u2019employer le langage... Elles seront utilisables en pr\u00e9sence de l\u2019\u0153uvre, de fa\u00e7on instantan\u00e9e mais aussi pourquoi pas avant ou apr\u00e8s l\u2019exp\u00e9rience\u00a0artistique.","width":"624px","height":"824.8px","transform":"translate(840px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676643587992","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### infos\n\n- Date : **10\/2022 \u219201\/2023**\n- Design : [Praticable](https:\/\/praticable.fr\/)\n- Nous tenons \u00e0 remercier chaleureusement Porteurs de sens, Julia et B\u00e9r\u00e9nice de la [Gait\u00e9 Lyrique](https:\/\/gaite-lyrique.net\/), [Visual System](https:\/\/visualsystem.org\/) et [Pierre-Damien Huyghe](http:\/\/pierredamienhuyghe.fr\/1.html) pour leur soutien et leur contribution \u00e0 ce projet.\n\n#### livrables\n\n- [Pr\u00e9sentation - PDF - 16,95 MB ](https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/29cae8f6ab-1677511032\/praticable-pouvoir-percevoir-livrable.pdf)\n- [Pr\u00e9sentation - En ligne - Figma](https:\/\/www.figma.com\/proto\/LxsSQbF5tZ5TIcMtlfBG1I\/Gait%C3%A9-Lyrique---livrable?page-id=0%3A1&node-id=103%3A2075&viewport=2902%2C-490%2C0.07&scaling=min-zoom)\n\n#### notes audio\n\nReformulation de la demande\n\n<audio\n        controls\n        src=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/284515e818-1677506202\/praticable-pouvoir-percevoir-demande.m4a\">\n            <a href=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/284515e818-1677506202\/praticable-pouvoir-percevoir-demande.m4a\">\n                Download audio\n            <\/a>\n    <\/audio>\n\nNote sur l'accessibilit\u00e9\n\n<audio\n        controls\n        src=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/a207945f32-1677506202\/praticable-pouvoir-percevoir-accessibilite-art.m4a\">\n            <a href=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/a207945f32-1677506202\/praticable-pouvoir-percevoir-accessibilite-art.m4a\">\n                Download audio\n            <\/a>\n    <\/audio>\n\nParti pris\n\n<audio\n        controls\n        src=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/817b943991-1677506202\/praticable-pouvoir-percevoir-partis-pris.m4a\">\n            <a href=\"https:\/\/praticable.fr\/media\/pages\/productions\/pouvoir-percevoir\/817b943991-1677506202\/praticable-pouvoir-percevoir-partis-pris.m4a\">\n                Download audio\n            <\/a>\n    <\/audio>","width":"432px","height":"769.8px","transform":"translate(840px, 996px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1676649610582","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/ga49IDDkxCooIpXx"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"348px","height":"377.833px","transform":"translate(48px, 996px)","zindex":0,"iscover":"false"},"id":"ec397b99-b95e-48e5-947e-278c66087eea","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/xZgDB0iLxork4x4Q"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"348px","height":"377.833px","transform":"translate(432px, 996px)","zindex":0,"iscover":"false"},"id":"e6d593db-0c97-4db3-b1e2-c0e41397d96f","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/AbJGeKom4nnYkq5l"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"732px","height":"518.517px","transform":"translate(48px, 1980px)","zindex":0,"iscover":"false"},"id":"65e80861-1b4a-4183-86f8-5881768a2851","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/kXam328ZYbR5UXI0"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"732px","height":"518.517px","transform":"translate(48px, 1416px)","zindex":0,"iscover":"false"},"id":"7007ade7-e8c2-4b26-ab43-61cf08bcd91f","isHidden":false,"type":"image"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-01-30

----

Coverthumb: https://praticable.fr/media/pages/productions/pouvoir-percevoir/331787943e-1675094720/praticable-pouvoir-percevoir-livrable-couverture.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: uuf2fjVoPmPjtr5S