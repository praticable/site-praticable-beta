Title: Tomette

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Tomette","width":"360px","height":"125px","transform":"translate(168px, 96px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667905622190","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Entre le tableau blanc et le jeu de construction, la Tomette permet de cartographier vos id\u00e9es. Sans m\u00e9thodologie pr\u00e9d\u00e9finie, cet outil est utilisable par tous et pour tout type de projet en devenant le support des \u00e9changes collectifs. Intuitif, il permet de projeter et de structurer des id\u00e9es collectivement et facilement dans l\u2019espace tout s\u2019adaptant librement \u00e0 une multitude de situation.","width":"615px","height":"212px","transform":"translate(168px, 252px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667905645838","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"iframe:https:\/\/player.vimeo.com\/video\/270040229?h=8266a28a37","width":"585px","height":"440px","transform":"translate(1515px, 1500px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667905709048","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"- Conception : **Praticable**\n- Ann\u00e9e : **2015-2022**\n- Licence : **[CC BY-NC-SA](https:\/\/creativecommons.org\/licenses\/by-nc-sa\/3.0\/fr\/)**","width":"324px","height":"189.25px","transform":"translate(168px, 480px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667906038926","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/tomette\/tomette-collectif-bam-1.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"735px","height":"2px","transform":"translate(840px, 255px)","zindex":0,"iscover":"false"},"id":"9cc63c63-767c-4e43-ad11-c7609654175e","isHidden":false,"type":"image"},{"content":{"image":["productions\/tomette\/tomette-collectif-bam-cartographier.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"585px","height":"2px","transform":"translate(1515px, 855px)","zindex":0,"iscover":"false"},"id":"751ac5ee-9070-4c6e-8095-a9f3789992ad","isHidden":false,"type":"image"},{"content":{"image":["productions\/tomette\/tomette-collectif-bam-scenariser.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"585px","height":"2px","transform":"translate(1515px, 1290px)","zindex":0,"iscover":"false"},"id":"dcb47d5d-ad6c-49ff-b7c1-d0f95c0b649e","isHidden":false,"type":"image"},{"content":{"image":["productions\/tomette\/tomette-collectif-bam-structurer.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"585px","height":"2px","transform":"translate(1515px, 1080px)","zindex":0,"iscover":"false"},"id":"4cb40814-fcdd-463f-902a-a8395fb1f007","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"###  Design de l\u2019outil Tomette","width":"600px","height":"90.5px","transform":"translate(165px, 750px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667907139777","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### Un outil de co-cr\u00e9ation\n\nNous avons con\u00e7u l\u2019outil Tomette comme un support ludique et intuitif qui mod\u00e9lise et spatialise les r\u00e9flexions lors d\u2019ateliers collaboratifs. Sa capacit\u00e9 d\u2019\u00e9l\u00e9vation en 3D permet de **structurer et de spatialiser des sch\u00e9mas de pens\u00e9e** complexe (des mind-maps, des r\u00e9seaux, des syst\u00e8mes\u2026).\n#### Un outil de facilitation\nLe kit est compos\u00e9 de **surfaces Velleda** con\u00e7ues \u00e0 l\u2019\u00e9chelle de la main pouvant \u00eatre agenc\u00e9es librement, de **tiges aimant\u00e9es** offrant un niveau de lecture en 3 dimensions et de **magnets color\u00e9s** permettant de **d\u00e9finir collectivement des r\u00e8gles ou des significations** propres aux besoins d\u2019ateliers. La Tomette ne d\u00e9tient volontairement ni m\u00e9thodologie pr\u00e9\u00e9tablie, ni mode d\u2019emploi. Sa forme, simple et honn\u00eate, favorise l\u2019appropriation et offre **une infinit\u00e9 d\u2019usages \u00e0 imaginer selon les contextes et les probl\u00e9matiques de chaque projet.** La restitution de l\u2019atelier est dynamique, une vid\u00e9o permet aux participants d\u2019archiver leur travail tout en le commentant.","width":"600px","height":"609.2px","transform":"translate(165px, 855px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667907151758","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### Dimensions\nl.16,5 cm\nL.30 cm\nh.20 cm\n#### Composition\n**1** bo\u00eete \u00e0 outils\n**80** tomettes\n**100** tiges\n**300** signifiants\n#### Conseil et entretien\n- Un kit correspond \u00e0 des formats d\u2019ateliers de 1 \u00e0 10 personnes.\n- Les tomettes et les signifiants sont inscriptibles avec des feutres pour tableau blanc.","width":"495px","height":"552.3px","transform":"translate(840px, 1095px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912189066","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Contactez-nous \u00e0 **tomette@praticable.fr**","width":"408px","height":"77.5px","transform":"translate(168px, 636px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912235361","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"L\u2019outil pour une cr\u00e9ation libre, tangible et collaborative.\nInscrivez vos id\u00e9es, agencez-les, effacez, structurez et restituez votre atelier. Le kit est transportable en dehors de vos murs, apportez-le \u00e0 tous vos ateliers.\nPrix : **400\u20ac TTC***\n\n*Frais de livraison non compris","width":"585px","height":"221px","transform":"translate(840px, 855px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912307766","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"- **La tomette** est inscriptible et effa\u00e7able se manipule, et se compose.\n- **Cartographier** un service, un business model, des id\u00e9es \u00e9mergentes...","width":"510px","height":"159px","transform":"translate(2115px, 855px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912457460","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"- **Les signifiants** organisent, notifient et indiquent.\n- **Sc\u00e9nariser** une strat\u00e9gie, un d\u00e9bat, des besoins...","width":"480px","height":"134.25px","transform":"translate(2115px, 1290px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912578874","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"- **La tige** connecte, structure et fa\u00e7onne en 3D.\n- **Structurer** une offre, une interface num\u00e9rique, des devis...","width":"540px","height":"134.25px","transform":"translate(2115px, 1080px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912658282","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"###  Acheter un Kit","width":"390px","height":"90.5px","transform":"translate(840px, 750px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667912844955","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"_**Paris en Tomette**_ a \u00e9t\u00e9 \u00e9crite pour donner \u00e0 voir des possibilit\u00e9s d\u2019usages et de pratiques permises par la Tomette. Elle est stock\u00e9e sur une cl\u00e9 usb contenue dans un kit achet\u00e9 et est r\u00e9serv\u00e9e aux clients Tomette.","width":"585px","height":"158px","transform":"translate(2115px, 1500px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667913705623","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: true

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: Les Tomettes sont un outil physique de cartographie d'idées, en 2D et 3D.

----

Author: 

----

Published: 2022-11-08

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: WAjHEuC1hjdJVlYc