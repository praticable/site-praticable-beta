Title: design graphique

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"image":["file:\/\/U94Qr1yX7DhrtGLK"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"420px","height":"2px","transform":"translate(744px, 36px)","zindex":0,"iscover":"true"},"id":"6a1ad753-fd55-494d-86dc-4730e35dc6b0","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"# design graphique","width":"696px","height":"125px","transform":"translate(72px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670519172125","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"> \u00ab La parole est un m\u00e9dium froid de faible d\u00e9finition parce que l'auditeur re\u00e7oit peu et doit beaucoup compl\u00e9ter. Les m\u00e9dias chauds, au contraire, ne laissent \u00e0 leur public que peu de blancs \u00e0 remplir ou \u00e0 compl\u00e9ter. Les m\u00e9dias chauds, par cons\u00e9quent, d\u00e9couragent la participation ou l'ach\u00e8vement alors que les m\u00e9dias froids, au contraire, les favorisent. \u00bb\n>\n> <cite>-- [**Marshall McLuhan**, \u00ab Pour comprendre les m\u00e9dias \u00bb, 1964](https:\/\/www.seuil.com\/ouvrage\/pour-comprendre-les-medias-marshall-mcluhan\/9782757850145)<\/cite>","width":"612px","height":"249px","transform":"translate(72px, 288px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670601419216","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## sobre et praticable\n\nSur la forme, le site ne pr\u00e9sente aucun \u00e9l\u00e9ments cosm\u00e9tiques. Il est **simple**, sans fioritures, sans d\u00e9coration. Son aspect sobre laisse place \u00e0 de **possibles pratiques formelles**. Les couleurs sont le noir, le blanc et le bleu. Cette derni\u00e8re signifie les interactions du site. Elle est utilis\u00e9e pour les hyperliens, au survol des boutons et des notes. Les ratios de contraste, les tailles de boutons et de polices respectent les normes d\u2019**accessibilit\u00e9** du RGAA.\n\nLes typographiques utilis\u00e9es sont la Fira-sans pour le texte, et la Fira-code pour les titres, les liens, l\u2019interface et le code. Elles ont \u00e9t\u00e9 con\u00e7ues pour Firefox OS et la fondation Mozilla. La Fira-code comprend une large palette de ligatures, de glyphes et unicodes, pour \u00e9viter la cr\u00e9ation et chargement d\u2019\u00e9ventuels pictogrammes. En r\u00e9sum\u00e9, le site est **sobre** aussi bien techniquement que formellement.","width":"612px","height":"505.4px","transform":"translate(72px, 600px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670839597119","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/BR97BBFNRqDHBtfv"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"900px","height":"2px","transform":"translate(720px, 600px)","zindex":0,"iscover":"false"},"id":"15c4ac16-4726-4ad6-bf80-e3e62cd901f4","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/NMt7vSRUwb6yWSUS"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"612px","height":"2px","transform":"translate(72px, 1152px)","zindex":0,"iscover":"false"},"id":"fc7871bd-24ea-408f-ad8b-36e20eaf2e3e","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/yp3ZSBr4pc3RhRSL"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"312px","height":"2px","transform":"translate(720px, 1152px)","zindex":0,"iscover":"false"},"id":"8d2bb4b9-c9c0-4223-9bb5-e01f88ec21ee","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### fichiers\n\nCe fichier, intitul\u00e9 _design graphique_ regroupe l'**ensemble des \u00e9l\u00e9ments et styles graphiques**. Il s'agit d'une librairie  [Figma](https:\/\/fr.wikipedia.org\/wiki\/Figma) (un logiciel de design d'interface), utilis\u00e9 dans l'_appareil design_ et divers documents.","width":"564px","height":"214px","transform":"translate(1200px, 36px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670597315910","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/RNoiyPaJE9botNPZ"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"348px","height":"2px","transform":"translate(1200px, 276px)","zindex":0,"iscover":"false"},"id":"d2b16729-22f9-4719-b057-78c0facf66c3","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"##### liens [Figma]\n\n- [**Design graphique**](https:\/\/www.figma.com\/community\/file\/1182380581211745349)\n- [**Appareil design**](https:\/\/www.figma.com\/community\/file\/1182362301387512864)\n- [**Maquettes**](https:\/\/www.figma.com\/community\/file\/1182675903043500401)\n-  [**Prototype**](https:\/\/www.figma.com\/proto\/YQM55mIk7eKm2FDOOVMDfj\/maquettes---site-praticable?hotspot-hints=0&amp;kind=&amp;node-id=778%3A6694&amp;page-id=199%3A5341&amp;scaling=contain&amp;starting-point-node-id=778%3A6694&amp;viewport=670%2C438%2C0.04)","width":"252px","height":"206.5px","transform":"translate(1584px, 228px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670597303007","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-12-08

----

Coverthumb: https://praticable.fr/media/pages/productions/site-praticable/design-graphique/11f6735dcc-1670519222/cover-design-graphique-site-praticable.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: nTDmPrqYh0c4owkd