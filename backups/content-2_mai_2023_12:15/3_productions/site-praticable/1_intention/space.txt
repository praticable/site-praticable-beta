Title: intention

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# intention","width":"444px","height":"125px","transform":"translate(96px, 108px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670506402659","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Cette note d'intention fait suite au texte [**qu'est ce que (se) pr\u00e9senter ?**](https:\/\/praticable.fr\/studio\/a-propos\/vers-autre-chose-qu-une-marque\/qu-est-ce-que-se-presenter) \u00e9crit \u00e0 l'occasion de notre changement de nom.","width":"396px","height":"131.5px","transform":"translate(708px, 300px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670592744045","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### d\u00e9couvrir formellement le c\u0153ur technique des productions\n\nQue faire de ce qu\u2019on ne peut pas strictement et compl\u00e8tement pr\u00e9senter ? Faut-il se r\u00e9signer \u00e0 ne rien en dire et ne rien en montrer ? L\u2019architecte **Philippe Rahm** propose une r\u00e9ponse sur [son site web](http:\/\/www.philipperahm.com\/).\n\nIl ne peut pas y pr\u00e9senter strictement \u2013 im-m\u00e9diatement, sans m\u00e9diation \u2013 ses b\u00e2timents. Plut\u00f4t que de faire comme si de rien n\u2019\u00e9tait et de montrer des images de synth\u00e8se dites \u00ab photo-r\u00e9alistes \u00bb, les images de Philippe Rahm exposent ce qui ne se ver- raient pas autrement. Elles ne sont pas re-dondantes, re-pr\u00e9sentatives, ni imitatives. Elles d\u00e9couvrent diverses mesures climatiques de l\u2019humidit\u00e9, des temp\u00e9ratures, des lumi\u00e8res, des mouvements atmosph\u00e9riques... Les ph\u00e9nom\u00e8nes thermodynamiques au c\u0153ur des productions de Philippe Rahm organisent la rubrique _Work_ de son site web.\n\nQuels sont les op\u00e9rations au c\u0153ur de notre travail ? Dans cet esprit, nous pourrions organiser notre travail d\u2019apr\u00e8s les principes de (d\u00e9)r\u00e9glabilit\u00e9, (d\u00e9)codabilit\u00e9, r\u00e9parabilit\u00e9 etc.","width":"588px","height":"664px","transform":"translate(96px, 300px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670506648278","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/XUil80fbSrve4DFt"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"216px","height":"2px","transform":"translate(708px, 492px)","zindex":0,"iscover":"false"},"id":"1ce4ce99-f3f8-4695-a9b0-f911919bc6da","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/Q9zQSPpATfs26Qx1"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"216px","height":"2px","transform":"translate(708px, 732px)","zindex":0,"iscover":"false"},"id":"7e3449b5-ef8f-4044-a5dc-5c49a2525a19","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### analyser, mettre \u00e0 plat, d\u00e9composer\n\nUn site qui se contenterait de reproduire[1] des faits pr\u00e9sents par ailleurs serait d\u00e9j\u00e0 utile en ce qu\u2019il les rapprocherait en son sein. Le rapprochement permet une appr\u00e9ciation d\u2019ensemble de faits \u00e9parpill\u00e9s, des op\u00e9rations de comparaisons et de distinctions. Un tel site est d\u00e9j\u00e0 g\u00e9n\u00e9reux, qui apporte la possibilit\u00e9 d\u2019une autre exp\u00e9rience des faits, d\u2019une exp\u00e9rience d\u2019ensemble. Une telle pr\u00e9sentation est g\u00e9n\u00e9reuse dans sa retenue, plus g\u00e9n\u00e9reuse qu\u2019une repr\u00e9sentation communicante en ce qu\u2019elle nous offre les faits presque nus et nous permet \u00e0 la fois de les penser ensemble et par nous-m\u00eames. Peut-on faire encore plus g\u00e9n\u00e9reux, enrichir encore les possibilit\u00e9s d\u2019exp\u00e9rience des faits sans pour autant en recouvrir la pr\u00e9sence \u2013 sans recourir \u00e0 la repr\u00e9sentation ?\n\nAnalyser signifie d\u00e9faire (_ana_) les liens (_luo_). Analyser une technique consiste \u00e0 d\u00e9faire la composition, \u00e0 d\u00e9composer. D\u00e9faire un fait n\u2019est pas le recouvrir : la d\u00e9composition d\u00e9couvre les composants techniques, formels et fonctionnels au c\u0153ur du fait. Elle est une mani\u00e8re de d\u00e9tailler \u2013 de d\u00e9-tailler, de d\u00e9-couper \u2013 une pr\u00e9sence. Il s\u2019agit pour nous de d\u00e9tailler la praticabilit\u00e9, c\u2019est-\u00e0-dire de la d\u00e9couper en morceaux de pr\u00e9sence. Par exemple, au sein de l\u2019[outil fait pour le **TMNlab**](https:\/\/praticable.fr\/productions\/etat-des-lieux-du-numerique-2021), le principe de (d\u00e9)compo- sabilit\u00e9 se traduit notamment par l\u2019interaction de collecte, celui de (d\u00e9)atachabilit\u00e9 par celle d\u2019export d\u2019\u00e9l\u00e9ments de l\u2019\u00e9tude. Qu\u2019est-ce que serait une pr\u00e9sentation des principes mis en \u0153uvre analogue \u00e0 la d\u00e9marche de Philippe Rahm ? Un tentative perfectible de r\u00e9ponse par la pr\u00e9sentation de la collecte comme pratique de (d\u00e9)composition :\n\nLa proximit\u00e9 spatio-temporel entre deux ph\u00e9nom\u00e8nes \u2013 la contigu\u00eft\u00e9[2] \u2013 produit dans l\u2019esprit qui les per\u00e7oit une \u00ab esp\u00e8ce d\u2019unit\u00e9 \u00bb constitutive de la rationalit\u00e9. Dans cette image, les traits rouges signifient des contigu\u00eft\u00e9s rompues et le trait vert une contigu\u00eft\u00e9 cr\u00e9\u00e9e par l\u2019op\u00e9ration de collecte. Tandis qu\u2019ils participent fondamentalement du processus de constitution de la connaissance, les effets de contigu\u00eft\u00e9 sont la plupart du temps inaper\u00e7us. Cette image n\u2019en repr\u00e9sente pas une autre, elle tente de permettre d\u2019apercevoir une part g\u00e9n\u00e9ralement in-aper\u00e7ue de l\u2019exp\u00e9rience[3]. Elle pr\u00e9sente dans le champs visuel une op\u00e9ration invisible et pourtant d\u00e9terminante du point de vue de la connaissance.","width":"588px","height":"1219px","transform":"translate(96px, 1020px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670506947718, 1670510252722, 1670510288140","isintro":"false","isedit":"false"},"id":"1670506826695","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/poS5k2MgjYAvexxn"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>La collecte comme pratique de (d\u00e9)composition<\/p>","link":"","ratio":"","crop":"false","width":"420px","height":"92px","transform":"translate(708px, 1308px)","zindex":0,"iscover":"true"},"id":"d9c6a48f-4aa3-466f-be2e-413b2cda7c30","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"[1] Une op\u00e9ration conceptuelle de l\u2019Art \u00e0 l\u2019\u00e9poque de sa reproduction m\u00e9canis\u00e9e de Walter Benjamin est de distinguer entre la reproduction et la repr\u00e9sentation.","width":"576px","height":"131.5px","transform":"translate(708px, 1116px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670506826695","isintro":"false","isedit":"false"},"id":"1670506947718","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"[2] Voire les travaux de David Hume dans son _Enqu\u00eate sur l\u2019entendement humain_, qui comprend la contigu\u00eft\u00e9 comme l\u2019un des 3 principes de connexion des id\u00e9es qui fondent toute connaissance.","width":"576px","height":"158.5px","transform":"translate(708px, 1884px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670506826695","isintro":"false","isedit":"false"},"id":"1670510252722","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"[3]  On doit encore \u00e0 Pierre-Damien cette diff\u00e9rence entre percevoir et apercevoir, mot qu\u2019il propose de comprendre \u00e0 peu pr\u00e8s comme percevoir consciemment.","width":"576px","height":"131.5px","transform":"translate(708px, 2100px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670506826695","isintro":"false","isedit":"false"},"id":"1670510288140","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### mettre \u00e0 jour l'envers\n\nL\u2019espace technique est couramment partag\u00e9 en deux. Dans le champ de la technique th\u00e9\u00e2trale, les parts s\u2019appellent coulisse et sc\u00e8ne. Dans le champs de l\u2019artisanat, atelier et boutique. Dans le champ de l\u2019industrie, usine et (super)march\u00e9. Dans le champ du web, _back_ et _front_. Du point de vue de la connaissance de la technique, ce partage n\u2019est pas \u00e9gal. D\u2019un c\u00f4t\u00e9 on en sait beaucoup, de l\u2019autre on en sait peu, juste ce qu\u2019il faut pour s\u2019en servir l\u2019\u00e2me en paix. L\u2019entretien de cette s\u00e9r\u00e9nit\u00e9 \u00e0 un revers, elle entretien en m\u00eame temps l\u2019ignorance des proc\u00e9dures techniques qui sous-tendent les services. On utiliserait peut-\u00eatre plus volontiers d\u2019autres techniques si leurs conditions de fonctionnement s\u2019effa\u00e7aient moins dans leurs mises en sc\u00e8ne.\n\nEn tant que studio de design, nous pouvons contribuer concr\u00e8tement \u00e0 am\u00e9liorer cette situation :\n\n- en pr\u00e9f\u00e9rant les expositions claires aux mises en sc\u00e8ne et mystifications\n- en montrant les coulisses, les codes et les backoffices\n- en amenant des capacit\u00e9s techniques de l\u2019arri\u00e8re (_back_) \u00e0 l\u2019avant (_front_), notamment des capacit\u00e9s d\u2019\u00e9criture puisqu\u2019elles sont g\u00e9n\u00e9ralement r\u00e9serv\u00e9es \u00e0 l\u2019arri\u00e8re, dans le secret de la remise\n- en r\u00e9duisant la diff\u00e9rence entre le _back_ et le _front_, voire en la supprimant\n\nTechniquement, rien n\u2019oblige cette s\u00e9paration qui maintient dans l\u2019ignorance et l\u2019incapacit\u00e9 cette partie de l\u2019humanit\u00e9 qu\u2019on appelle du pauvre nom d\u2019usagers. Les \u00e9diteurs dit [WYSIWYG](https:\/\/fr.wikipedia.org\/wiki\/What_you_see_is_what_you_get), dont les acc\u00e8s sont malheureusement en g\u00e9n\u00e9ral limit\u00e9s aux propri\u00e9taires des sites, montrent un rapproche- ment possible du _back_ et du _front_. Le projet de navigateur [Bluesky](https:\/\/blueskyweb.xyz\/) (anciennement initul\u00e9 Beaker) fait une proposition plus radical encore en fusionnant le navigateur et l\u2019\u00e9diteur de code.","width":"588px","height":"967.75px","transform":"translate(96px, 2292px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670509834487","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"iframe:https:\/\/blueskyweb.xyz\/","width":"552px","height":"418px","transform":"translate(708px, 2832px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1670509913112","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### donner forme \u00e0 l'\u00e9galit\u00e9\n\nCelui qu\u2019on appelle \u00ab le premier venu \u00bb n\u2019est personne en particulier. Le \u00ab num\u00e9ro un \u00bb, \u00e0 la t\u00eate d\u2019une course ou d\u2019une organisation, en revanche, n\u2019est pas n\u2019importe qui. En guerre, le premier venu, l\u2019anonyme, sera le plus souvent en premi\u00e8re ligne, vou\u00e9 \u00e0 mourir d\u2019abord, lui dont la vie est suppos\u00e9e de derni\u00e8re importance. \u00c0 la premi\u00e8re place, d\u2019o\u00f9 s\u2019exerce un certain commandement, en revanche, la vie est moins risqu\u00e9e. \u00ab Premier \u00bb se dit en plusieurs sens, de valeurs vari\u00e9es, de sorte qu\u2019il n\u2019est pas toujours bon d\u2019\u00eatre premier.\n\nL\u2019ambig\u00fcit\u00e9 contenu dans notre notion de primaut\u00e9 ne date pas d\u2019aujourd\u2019hui. Le latin _princeps_ pouvait d\u00e9signer aussi bien les soldats de premi\u00e8re ligne de la formation en phalange (appel\u00e9s les _principes_) que ceux qu\u2019on appelle encore les princes \u2013 mot que, dans le milieu latin, on entendait sans doute plus nettement comme d\u00e9signant une esp\u00e8ce particuli\u00e8re de premiers. Plus t\u00f4t dans l\u2019histoire, la signification du vieux grec _arkh\u00e8_ (anc\u00eatre de notre archi-, qui donne archi-tecture mais aussi an-archie et hi\u00e9r-archie) oscillait selon les d\u00e9clinaisons entre \u00ab commencement \u00bb (_arkh\u00e8g\u00e9teuo_) et \u00ab commandement \u00bb (_arkh\u00e8g\u00e9teo_). Encore dans le champs guerrier, le langage d\u2019aujourd\u2019hui semble \u00e0 peine plus clair, qui distingue m\u00e9taphoriquement le front (la premi\u00e8re ligne) de la t\u00eate (le chef) de l\u2019arm\u00e9e. Le front et la t\u00eate ne sont pas des parties du corps strictement s\u00e9par\u00e9es mais enfin elles ne se confondent pas : le front est \u00e0 l\u2019avant de la t\u00eate. \u00c0 cette place, il est expos\u00e9 aux coups dans la figure. Le front est dur, il prot\u00e8ge un cerveau qu\u2019on pense volontiers comme le commandant des op\u00e9rations du corps, premier cette fois dans l\u2019ordre des causes de ses faits et gestes.\n\nEn jeu dans tout ce vocabulaire \u2013 le n\u00f4tre et celui de nos anc\u00eatres qui ne sont pas si loin \u2013 un probl\u00e8me qui change de forme sans jamais se r\u00e9gler pour de bon. Aujourd\u2019hui, on dirait un probl\u00e8me d\u2019ordre. Ordonner signifie \u00e0 la fois agencer et commander. Un ordre d\u00e9signe \u00e0 la fois une disposition et une obligation. Ici encore l\u2019ambig\u00fcit\u00e9 demeure, avec davantage d\u2019\u00e9vidence. Les dictionnaires s\u00e9parent les sens. Mais sommes-nous capables de parler d\u2019ordre sans impliquer si peu que ce soit le sens du commandement ? Donner un ordre, agencer, obliger, est une mani\u00e8re de produire une modification qui ne se produit pas d\u2019elle-m\u00eame. Ce serait en  effet un prince bien inutile, bien comique m\u00eame, que celui qui donnerait aux choses l\u2019ordre d\u2019\u00eatre ce qu\u2019elles sont. Ordonner, c\u2019est imposer un ordre. Se r\u00e9gler soi-m\u00eame est une traduction litt\u00e9ral possible de l\u2019autonomie. R\u00e9gler quelqu\u2019un ou quelque chose d\u2019autre en est une de l\u2019h\u00e9t\u00e9ronomie.\n\nEn pratique, nous ne distinguons d\u2019ailleurs gu\u00e8re plus clairement qu\u2019en parole : nous peinons \u00e0 produire des organisations non hi\u00e9rarchiques[4]. Quiconque tient peu ou prou \u00e0 l\u2019\u00e9galit\u00e9 \u00e9prouve plus ou moins gravement ce probl\u00e8me commun, banal. D\u2019un champs \u00e0 l\u2019autre, ses manifestations varient mais il se pose en termes semblables sinon identiques de composition, c\u2019est-\u00e0-dire de positions, de taille, de couleur, de mouvement, de dur\u00e9e. Au football, pour r\u00e9tablir le jeu, on remet la balle au centre. En temps de campagne pr\u00e9sidentielle d\u00e9mocratique, on compte les temps de parole.","width":"588px","height":"1564px","transform":"translate(96px, 3312px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670510686422","isintro":"false","isedit":"false"},"id":"1670510482317","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"[4]  Mot lui-m\u00eame tributaire de l\u2019ambig\u00fcit\u00e9 de l\u2019_arkh\u00e8_ grec.","width":"324px","height":"104.5px","transform":"translate(708px, 4596px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"1670510482317","isintro":"false","isedit":"false"},"id":"1670510686422","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-12-07

----

Coverthumb: https://praticable.fr/media/pages/productions/site-praticable/intention/605d367e02-1670518762/collectable-collecte-intention-praticable.png

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: IOV4NTQfpVHIaXcy