Title: le projet

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# le projet","width":"504px","height":"125px","transform":"translate(72px, 96px)","zindex":3,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1677515807505","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"En 2018, les diff\u00e9rents campus de l'Universit\u00e9 de Lille sont fusionn\u00e9s. Elle doit alors s'organiser et composer avec les **7 sites** universitaires, soit environ **72 000 \u00e9tudiants** fran\u00e7ais et internationaux. C\u2019est dans ce contexte qu\u2019a \u00e9t\u00e9 formul\u00e9e la demande d\u2019am\u00e9liorer l\u2019accueil des \u00e9tudiant\u00b7es de l\u2019Universit\u00e9 de Lille par une **d\u00e9marche de design**.","width":"504px","height":"239px","transform":"translate(72px, 252px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678871254693","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## 01","width":"108px","height":"99.4px","transform":"translate(516px, 528px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678295482247","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 10\/2020\n### atelier de cadrage\n\n1. **R\u00e9flexion sur la notion d'accueil** avec un podcast d'ouverture r\u00e9alis\u00e9 par [Pa\u00efdeia](https:\/\/paideiaconseil.fr\/).\n2. **Analyse collective de l'existant** par l'\u00e9tude des objets d'accueil pr\u00e9sents sur les campus.\n3. **D\u00e9finition de 3 fonctions prioritaires** pour am\u00e9liorer l'accueil : s'orienter, s'informer et s'arr\u00eater.\n\n##### livrables\n- [**Podcast d'ouverture** - MP3 - 19,98 MB ](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-podcast.mp3)\n- [**Script du podcast** - PDF - 1,76 MB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-livret.pdf)\n- [**Outils de l'atelier** - PDF - 145,02 KB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-atelier-cadrage-outils.pdf)\n- [**Synth\u00e8se de l'atelier** - PDF - 270,63 KB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-synthese-cadrage.pdf)","width":"504px","height":"549.75px","transform":"translate(72px, 564px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678295247649","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/NU1ldG6lAHkNMRza"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"360px","height":"2px","transform":"translate(1032px, 840px)","zindex":0,"iscover":"false"},"id":"9ba0479c-813a-4a1c-a985-c601b5065839","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/54HWYtAtyklZQrpS"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"360px","height":"2px","transform":"translate(636px, 840px)","zindex":0,"iscover":"false"},"id":"368cbecc-e9ee-4998-afbd-fd2f07731823","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"##### Podcast d'ouverture\n\n<audio\n        controls\n        src=\"https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-podcast.mp3\">\n            <a href=\"https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-podcast.mp3\">\n                Download audio\n            <\/a>\n    <\/audio>","width":"360px","height":"136.5px","transform":"translate(636px, 672px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678885121625","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/ZGchCJrRWFtocAjd"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"168px","height":"2px","transform":"translate(1428px, 840px)","zindex":0,"iscover":"false"},"id":"73ae4fe2-0765-4a3b-8f9d-4d8d4cb516a3","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 02","width":"100px","height":"99.4px","transform":"translate(516px, 1164px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678290695946","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 10\/2020 -> 09\/2021\n## enqu\u00eate\n\nIniti\u00e9e en octobre 2020, puis interrompue \u00e0 cause de la pand\u00e9mie du Covid-19, reprise finalement en septembre 2021 avec le retour des \u00e9tudiants, les conditions d'enqu\u00eate, certes p\u00e9rilleuses mais diverses, nous ont permis d'assister \u00e0 **2 \u00e9v\u00e8nements de rentr\u00e9e**, de visiter les 3 campus, de recueillir de nombreux **t\u00e9moignages** des \u00e9tudiants et de mener **5 entretiens** aupr\u00e8s des repr\u00e9sentants de l'universit\u00e9.\n\n\u00c0 l'issue de cette phase, nous avons fait part de nos observations et recommandations au travers d'un **rapport d'enqu\u00eate illustr\u00e9** et d'une **mini-exposition sur place**. Enfin, nous avons pr\u00e9sent\u00e9 le **positionnement du projet**, celui de soutenir les pratiques d'accueil d\u00e9j\u00e0 pr\u00e9sentes sur les campus.\n\n##### livrables\n- [**Synth\u00e8se de l'enqu\u00eate** - PDF - 4,45 MB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-enquete.pdf), imprim\u00e9e, expos\u00e9e et en ligne, comprenant l'analyse terrain des 3 campus, les plans de site \u00e9tudi\u00e9s et mat\u00e9rialis\u00e9s, ainsi que le positionnement du projet.","width":"504px","height":"760.9px","transform":"translate(72px, 1200px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678290789626","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/K2rKgocds5Dz2aoq"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"432px","height":"2px","transform":"translate(1152px, 1512px)","zindex":0,"iscover":"false"},"id":"14758650-d25b-4555-8582-7144654d828a","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/Kj0nSwqi7s2VXb8I"],"src":"","isdesktoponly":"false","location":"kirby","alt":"cartographie campus","caption":"","link":"","ratio":"","crop":"false","width":"432px","height":"2px","transform":"translate(1152px, 1200px)","zindex":0,"iscover":"false"},"id":"5b0613ea-b2cd-4169-95e0-5a19064edbbe","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/8JNgxW7p097GbOmK"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"2px","transform":"translate(648px, 1764px)","zindex":0,"iscover":"false"},"id":"d1118e01-0fef-4d68-854c-56a3a4e81aeb","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/w4Ljel5huWGjuLZD"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"2px","transform":"translate(972px, 1764px)","zindex":0,"iscover":"false"},"id":"b8766973-05dd-4113-8b6a-41f7bb86790c","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/cibUzNpeP2xbfHVQ"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"2px","transform":"translate(1296px, 1764px)","zindex":0,"iscover":"false"},"id":"14f0062e-b5db-4cf6-ba78-9e81626e09ee","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/rQAq1XPN2qjLvtjA"],"src":"","isdesktoponly":"true","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"216px","height":"2px","transform":"translate(648px, 1200px)","zindex":0,"iscover":"false"},"id":"908d3630-f2ec-42ea-9600-0afba75b7e99","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/QnAfcyw2LrDW7QkV"],"src":"","isdesktoponly":"true","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"216px","height":"2px","transform":"translate(900px, 1200px)","zindex":0,"iscover":"false"},"id":"e328c1eb-1ffe-4a5e-abf8-dbd4bba64098","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 03","width":"100px","height":"99.4px","transform":"translate(504px, 2184px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678894483044","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 10\/2021 -> 02\/2022\n## id\u00e9ation\n\nGr\u00e2ce \u00e0 l'enqu\u00eate, nous avons r\u00e9dig\u00e9 un parti-pris clair et organis\u00e9 un **workshop** le 26 janvier 2022, pour imaginer des propositions avec les \u00e9tudiant\u00b7e\u00b7s et repr\u00e9sentant\u00b7e\u00b7s de l'universit\u00e9. Suite \u00e0 la formalisation d'un premier cahier d'id\u00e9es, nous avons esquiss\u00e9 un outil qui permet de partager et de co-produire des objets d'accueil.\n\n##### livrables\n- [**Cahier d'id\u00e9es** - PDF - 4,34 MB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-intention.pdf), comprenant l'ensemble des recherches et la pr\u00e9sentation de l'intention.\n- [**Proposition** - PDF - 9,72 MB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-proposition.pdf) au travers d'esquisses, de sch\u00e9mas et divers visuels.","width":"504px","height":"593.65px","transform":"translate(60px, 2232px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678894475641","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/v0DtjbgaqUs4Tvd5"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"2px","transform":"translate(648px, 2232px)","zindex":0,"iscover":"false"},"id":"e5d7fb52-986c-4f71-857a-73fcff4d955c","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/ywphX20B5IbQkGlc"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"612px","height":"2px","transform":"translate(972px, 2232px)","zindex":0,"iscover":"false"},"id":"0578f04f-69df-4189-941b-7034fe8224f4","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/ZgghKW5PPVWHfWbZ"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"240px","height":"2px","transform":"translate(1104px, 2616px)","zindex":0,"iscover":"false"},"id":"0e421f6a-21f8-4786-bc7c-4c00b0b50ecd","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/oPhTZK1E4WOXparz"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"432px","height":"2px","transform":"translate(648px, 2544px)","zindex":0,"iscover":"false"},"id":"e7d7fe86-9205-4d15-a1d9-d93fff533fb9","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 04","width":"100px","height":"99.4px","transform":"translate(504px, 2916px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678897654098","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 02\/2021 -> 05\/2022\n## maquette\n\nUne fois la proposition discut\u00e9e et valid\u00e9e, nous l'avons approfondi et finalis\u00e9 au travers d'une **maquette interactive** qui pr\u00e9sente l'ensemble des \u00e9crans ainsi que diff\u00e9rentes mises en situation (accessibles via plusieurs codes QR).\nCe prototype Figma permet de pr\u00e9senter et de tester la proposition aux diff\u00e9rents services ainsi qu'aux \u00e9tudiant\u00b7e\u00b7s de l'universit\u00e9.\n\n##### livrables\n- [**Arborescence** - PDF  - 2,99 MB](https:\/\/praticable.fr\/productions\/re-faire-accueil\/projet\/praticable-universite-lille-arborescence.pdf)\n- [**Prototype** - Figma](https:\/\/t.ly\/rZZy)","width":"504px","height":"546.4px","transform":"translate(72px, 2964px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1678897642823","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/Hrnd5aWUOu4G2aFW"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"432px","height":"2px","transform":"translate(648px, 2952px)","zindex":0,"iscover":"true"},"id":"2c0b096d-e07c-40c5-bd0d-39b7c4fabfff","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/usnKV7cv2J9VHQrD"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"612px","height":"2px","transform":"translate(1104px, 2952px)","zindex":0,"iscover":"false"},"id":"2cb204ea-e38b-4aa4-8cd4-df8da398eb8c","isHidden":false,"type":"image"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-02-27

----

Uuid: c7kYlo4zbONoAzNd

----

Coverthumb: https://praticable.fr/media/pages/productions/re-faire-accueil/projet/53b328b453-1678812554/praticable-universite-lille-accueil-etudiant-maquette-cover.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris