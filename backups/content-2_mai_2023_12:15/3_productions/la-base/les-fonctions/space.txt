Title: Les fonctions

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Les fonctions","width":"585px","height":"127px","transform":"translate(225px, 15px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1664812692753","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Fonctionnement g\u00e9n\u00e9ral","width":"585px","height":"92.5px","transform":"translate(225px, 165px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1673440764567","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/la-base\/les-fonctions\/la-base-fonctionnement-praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"1215px","height":"492.242px","transform":"translate(225px, 285px)","zindex":0,"iscover":"false"},"id":"77c7ac26-67a7-4899-9e1b-f3a919e53ce5","isHidden":false,"type":"image"},{"content":{"title":"Moteur de recherche","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/la-base\/les-fonctions\/moteur-de-recherche\/6f6c610304-1673443887\/la-base-icone-moteur-de-recherche-praticable.jpg","intro":"","width":"270px","height":"133px","transform":"translate(1170px, 810px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/la-base\/les-fonctions\/moteur-de-recherche","tags":"","linkedspaces":[]},"id":"productions\/la-base\/les-fonctions\/moteur-de-recherche","isHidden":false,"type":"representative"},{"content":{"title":"Bases","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/la-base\/les-fonctions\/bases\/ff6a591987-1673454038\/la-base-icone-bases-praticable.jpg","intro":"","width":"270px","height":"92.5px","transform":"translate(225px, 810px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/la-base\/les-fonctions\/bases","tags":"","linkedspaces":[]},"id":"productions\/la-base\/les-fonctions\/bases","isHidden":false,"type":"representative"},{"content":{"title":"Fiche ressource","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/la-base\/les-fonctions\/fiche-ressource\/0df0bdb501-1673454069\/la-base-icone-fiche-ressource-praticable.jpg","intro":"","width":"270px","height":"133px","transform":"translate(855px, 810px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/la-base\/les-fonctions\/fiche-ressource","tags":"","linkedspaces":[]},"id":"productions\/la-base\/les-fonctions\/fiche-ressource","isHidden":false,"type":"representative"},{"content":{"title":"Compte","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/la-base\/les-fonctions\/compte\/cf26931582-1673454458\/la-base-icone-profil-praticable.jpg","intro":"","width":"270px","height":"92.5px","transform":"translate(540px, 810px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/la-base\/les-fonctions\/compte","tags":"","linkedspaces":[]},"id":"productions\/la-base\/les-fonctions\/compte","isHidden":false,"type":"representative"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-10-03

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: W4TohEsmu6p3nWfH