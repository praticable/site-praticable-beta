Title: la démarche

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# la d\u00e9marche","width":"612px","height":"200px","transform":"translate(48px, 96px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679658992257","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## 01","width":"108px","height":"99.4px","transform":"translate(384px, 276px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679659016806","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 11\/2020 \u2192 01\/2021\n### diagnostic et pistes de design\n\nCe document r\u00e9sume la **strat\u00e9gie du projet** \u00e0 travers ses intentions. Il constitue une base de travail sur laquelle s'accorder pour le **design** du projet. Il est structur\u00e9 \u00e0 partir d'un mod\u00e8le d'organisation de projet con\u00e7u par nos soins, le **VRAP** (Vision, Relation, Action, Perception).\n\nCe diagnostic a pu b\u00e9n\u00e9ficier de [l'aide au Diagnostic Design de la BPI](https:\/\/www.bpifrance.fr\/catalogue-offres\/soutien-a-linnovation\/diagnostic-design).","width":"408px","height":"472.5px","transform":"translate(48px, 312px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679659082105","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/kzMsk1zvnLihfHeO"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"480px","height":"2px","transform":"translate(480px, 444px)","zindex":0,"iscover":"false"},"id":"6e2cc938-f584-458d-a018-42e0bea86771","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/Uy0GXkCzncQMEgDz"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"348px","height":"2px","transform":"translate(804px, 228px)","zindex":1,"iscover":"false"},"id":"0c2a6329-d3e6-4a26-b3ed-bdd280c74e69","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 02","width":"108px","height":"99.4px","transform":"translate(396px, 852px)","zindex":3,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679660002292","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 02\/2021 \u2192 03\/2021\n### [je passe au libre](https:\/\/osinum.fr\/jepasseaulibre\/)\n\n**Design et d\u00e9veloppement** d'un outil qui permet d'identifier ses pratiques de travail et les outils libres adapt\u00e9s tout en pr\u00e9sentant les avantages de l'offre Osinum.","width":"408px","height":"287.5px","transform":"translate(48px, 900px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679660019865","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/PstlmLw7Uwq5xTEr"],"src":"","isdesktoponly":"false","location":"kirby","alt":"je passe au libre","caption":"<p><a href=\"https:\/\/osinum.fr\/jepasseaulibre\/\">Je passe au libre<\/a>, un outil qui vous aide \u00e0 passer \u00e0 des outils libres.<\/p>","link":"https:\/\/osinum.fr\/jepasseaulibre\/","ratio":"","crop":"false","width":"408px","height":"92px","transform":"translate(48px, 1224px)","zindex":null,"iscover":"false"},"id":"3a325da5-d559-4bf0-ba17-25c8c7e74b2f","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 03","width":"108px","height":"99.4px","transform":"translate(552px, 1056px)","zindex":3,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679666852404","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 03\/2022 \u2192 04\/2022\n### enqu\u00eate\n\n- **Veille et analyse** de l'existant\n- [**Recensement des outils** d'acteurs](https:\/\/praticable.fr\/productions\/osinum-territoires\/projet\/praticable-osinum-recensement-outils-collectivites.pdf)\n- **Entretiens** aupr\u00e8s des communes, des communaut\u00e9s de communes, associations et syndicats de communes (mairie, \u00e9lu\u00b7e\u00b7s , \u00e9lu\u00b7e\u00b7s au num\u00e9rique, secr\u00e9taire de mairie, adjoint aux transitions, DGS, conseilliers num\u00e9rique, etc)","width":"408px","height":"414px","transform":"translate(636px, 1128px)","zindex":4,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679666856165","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"## 04","width":"108px","height":"99.4px","transform":"translate(384px, 1656px)","zindex":5,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679667574583","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 05\/2022 \u2192 09\/2022\n### conception\n\n1. [**Arborescence** - PDF - 5,42 MB](https:\/\/praticable.fr\/productions\/osinum-territoires\/projet\/praticable-osinum-arborescence.pdf)\n2. Wireframe (esquisses des \u00e9crans)\n3. Design graphique\n4. Design system\n3. [**Maquettes** - Figma](https:\/\/www.figma.com\/community\/file\/1215710352343118860)","width":"408px","height":"322.25px","transform":"translate(48px, 1704px)","zindex":4,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679667579254","isHidden":false,"type":"markdown"},{"content":{"image":["file:\/\/ZoZpG09y9YOIs8nU"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"300px","height":"2px","transform":"translate(540px, 1620px)","zindex":0,"iscover":"false"},"id":"f56d9603-2ea4-4d69-91ac-b02ef27c069d","isHidden":false,"type":"image"},{"content":{"image":["file:\/\/htzHtHxZKAQ9TbIN"],"src":"","isdesktoponly":"false","location":"kirby","alt":"maquettes osimum","caption":"","link":"","ratio":"","crop":"false","width":"288px","height":"2px","transform":"translate(48px, 2052px)","zindex":0,"iscover":"true"},"id":"316a4b61-da0b-48d6-8886-88cee5aa2c1e","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"## 05","width":"108px","height":"99.4px","transform":"translate(528px, 2052px)","zindex":5,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679674261405","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"##### 10\/2021 \u2192 01\/2023\n### d\u00e9veloppement\n\nD\u00e9veloppement sur Wordpress par [BSA Web](https:\/\/www.bsa-web.fr\/).\n\n- Suivi du d\u00e9veloppement\n- Documentation du projet \n- Tests et lancement en f\u00e9vrier 2023","width":"444px","height":"301.5px","transform":"translate(612px, 2124px)","zindex":6,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1679674268745","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2023-03-24

----

Uuid: nfbQoI4KYGPlIqAh

----

Coverthumb: https://praticable.fr/media/pages/productions/osinum-territoires/demarche/20d1e07f45-1679673201/praticable-osinum-maquettes.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris