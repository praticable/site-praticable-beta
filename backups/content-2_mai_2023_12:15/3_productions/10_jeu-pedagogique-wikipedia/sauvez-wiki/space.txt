Title: Sauvez Wiki : un jeu de rôle

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces:

- >
  productions/jeu-pedagogique-wikipedia/sous-le-masque

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Sauvez Wiki","width":"588px","height":"125px","transform":"translate(84px, 96px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672054175423","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"\ud83d\udc46 _Ce jeu est \u00e0 l'\u00e9tat d'\u00e9bauche. \nCertains aspects comme le graphisme, n'ont pas encore \u00e9t\u00e9 trait\u00e9s mais nous le documentons pour qu'il puisse vous servir. Si vous voulez y contribuer contactez-nous !_","width":"348px","height":"212px","transform":"translate(912px, 252px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672653078082","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"792px","height":"642.983px","transform":"translate(84px, 252px)","zindex":0,"iscover":"true"},"id":"6374f2cd-3229-4a56-9bd5-958881c88eb9","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"D\u2019une simple correction orthographique \u00e0 l\u2019\u00e9criture d\u2019un nouvel article, il existe bien des mani\u00e8res de contribuer \u00e0 Wikip\u00e9dia, bien des r\u00f4les contributifs qui enrichissent tous \u2013 quoi que diff\u00e9remment \u2013 la connaissance commune. \n\n_**Sauvez wiki**_ est un jeu coop\u00e9ratif, m\u00e9lange de [jeu de r\u00f4le](https:\/\/fr.wikipedia.org\/wiki\/Jeu_de_r%C3%B4le_sur_table) et de [deckbuilding](https:\/\/fr.wikipedia.org\/wiki\/Jeu_de_deck-building). \n\nIl nous plonge dans les al\u00e9as et p\u00e9rip\u00e9ties de l\u2019am\u00e9lioration d\u2019un article fictif.\n\n\nL\u2019objectif ? Am\u00e9liorer sa qualit\u00e9, menac\u00e9e par des trolls, des vandales, des complotistes et autres fripouilles qui maltraitent la connaissance. \n\nD\u2019une partie \u00e0 l\u2019autre, les changements de r\u00f4les fournissent aux joueurs et joueuses l\u2019occasion d\u2019\u00e9prouver l\u2019entreprise de connaissance comme d\u00e9marche coop\u00e9rative et \u00e9galitaire, qui n\u2019appartient \u00e0 personne en particulier, dont nul n\u2019est propri\u00e9taire. Il tiendra \u00e0 chacun de prendre soin du savoir gr\u00e2ce aux actions propres \u00e0 son r\u00f4le, plus ou moins exigeantes en mati\u00e8re de temps. Des \u00e9v\u00e9nements viendront al\u00e9atoirement perturber la partie et solliciter les capacit\u00e9s de r\u00e9partie et d\u2019improvisation des futurs contributeurs\u00b7rices.","width":"792px","height":"573px","transform":"translate(84px, 816px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672655635678","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-02_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>La qualit\u00e9 de l\u2019article est symbolis\u00e9e par une barre progressive, d\u00e9compos\u00e9e en coches. Plus le curseur (le trombone) est \u00e0 droite, plus l\u2019article est de bonne qualit\u00e9. Les joueurs\u00b7euses commencent \u00e0 gauche et devront se retrouver dans la partie claire \u00e0 droite en fin de jeu.<\/p>","link":"","ratio":"","crop":"false","width":"600px","height":"381.817px","transform":"translate(84px, 1332px)","zindex":0,"iscover":"false"},"id":"eab1a891-3a60-4f2b-924a-53d775d5163f","isHidden":false,"type":"image"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-03_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>L\u2019encha\u00eenement des tours est repr\u00e9sent\u00e9 \u00e9galement par un curseur.<\/p>","link":"","ratio":"","crop":"false","width":"600px","height":"337.75px","transform":"translate(732px, 1332px)","zindex":0,"iscover":"false"},"id":"be595136-e098-4155-8564-40dba52d89a1","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Les r\u00f4les\nPour faire bouger le curseur vers la droite et am\u00e9liorer ainsi l\u2019article, les joueurs\u00b7euses doivent choisir en d\u00e9but de partie de former une \u00e9quipe en incarnant diff\u00e9rents r\u00f4les. Les cartes r\u00f4les correspondent \u00e0 des formes de contribution sur Wikip\u00e9dia. Chaque r\u00f4le poss\u00e8de ses propres capacit\u00e9s, et peut ajouter une ou plusieurs am\u00e9liorations par tour, plus ou moins vite...\n- La Wikifi\u00e8re, traite la mise en page ;\n- L\u2019ajoutrice, ajoute des informations ;\n- L\u2019orthographiste, d\u00e9tecte les petites fautes ;\n- Le sourcier, trouve et ajoute les bonnes sources ;\n- Le reformulator, s\u2019attaque \u00e0 la syntaxe et \u00e9vite les copier\/coller.\n\nL'enjeu p\u00e9dagogique est d'introduire les diff\u00e9rentes formes de\ncontribution.","width":"600px","height":"509.75px","transform":"translate(84px, 1776px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672652653465","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-04_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>Les captures d\u2019\u00e9crans sur les cartes familiarisent avec les outils des diff\u00e9rentes contributions<\/p>","link":"","ratio":"","crop":"false","width":"540px","height":"92px","transform":"translate(732px, 1776px)","zindex":0,"iscover":"false"},"id":"99753971-c251-46ed-918e-3f32775b95ab","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Le temps comme ressource\nPour utiliser leurs capacit\u00e9s d\u2019am\u00e9lioration, les joueurs\u00b7euses doivent d\u00e9penser une ressource : le temps. \nLes unit\u00e9s de temps s\u2019acqui\u00e8rent en piochant dans la pioche temps.","width":"600px","height":"213.5px","transform":"translate(84px, 2424px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672653101799","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-05_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"540px","height":"472.033px","transform":"translate(732px, 2424px)","zindex":0,"iscover":"false"},"id":"ee85e3f8-4469-46bc-a417-06d36987e45f","isHidden":false,"type":"image"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-06_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>N&#8217;importe quel objet peut faire office d&#8217;unit\u00e9 de temps.<\/p>","link":"","ratio":"","crop":"false","width":"600px","height":"485.75px","transform":"translate(84px, 2640px)","zindex":0,"iscover":"false"},"id":"eb8f6677-6f55-4b91-a8ce-0c824b0ceb99","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Pouvoirs et Bots\nUn deck de pouvoirs est disponible. Il permet d'introduire les formes de protection et d\u2019aide des contributeurs\u00b7rices. \n- **Des cartes Bots** qui facilitent leur travail  en apportant un pouvoir passif (L\u2019orthograBot corrige l\u2019orthographe, le vandalBot supprime le vandalisme etc). \n- **D\u2019autres pouvoirs** (Blocage IP, etc) pour contrer les actions des \u00ab\u202ffripouilles\u202f\u00bb.","width":"600px","height":"298.25px","transform":"translate(84px, 3216px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672653693384","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-07_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"636px","height":"511.05px","transform":"translate(720px, 3216px)","zindex":0,"iscover":"false"},"id":"fff19bc0-3f1f-4585-a817-b6b6b23ce4e4","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Les fripouilles\nL\u2019am\u00e9lioration ne sera pas un long fleuve tranquille. Des cartes fripouilles sont automatiquement jou\u00e9es. Ces cartes font reculer le curseur qualit\u00e9 ou perdre des unit\u00e9s de temps \u00e0 l\u2019\u00e9quipe etc. Quelques exemples :\n- Le vandal \n- Le troll\n- Le complotiste\n- Le directeur (utilise un ton promotionnel)\n- Le copier-colleur (copie beaucoup de contenu non libre)\n- Le bot un peu trop rapide\n\nL'enjeu p\u00e9dagogique est de comprendre que certaines contributions volontaires (vandalisme) ou non (directeur) nuisent \u00e0 la qualit\u00e9 d\u2019un article et sont autant d\u2019axes d\u2019am\u00e9lioration. De mani\u00e8re d\u00e9tourn\u00e9e, on comprend les principes fondateurs de Wikip\u00e9dia (neutralit\u00e9, libert\u00e9 de contenu, encyclop\u00e9disme etc), et on entra\u00eene les participant\u00b7es \u00e0 d\u00e9tecter ce qui pourrait \u00eatre am\u00e9lior\u00e9 et de quelle mani\u00e8re contribuer.","width":"600px","height":"596px","transform":"translate(84px, 3780px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672654567551","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/jeu-wikipedia-1-08_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"<p>Les petites captures d\u2019\u00e9crans familiarisent avec les diff\u00e9rents bandeaux d&#8217;avertissement de la plateforme.<\/p>","link":"","ratio":"","crop":"false","width":"528px","height":"572.45px","transform":"translate(720px, 3780px)","zindex":0,"iscover":"false"},"id":"8b27a86e-fdc2-47a6-901c-31399cb29354","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Adaptabilit\u00e9 p\u00e9dagogique\nLe jeu est imagin\u00e9 comme base p\u00e9dagogique que le ou la professeur\u00b7e ou le ou la r\u00e9f\u00e9rent\u00b7e peut adapter, \u00e9tendre, sp\u00e9cialiser : \n- Changer les objectifs de qualit\u00e9 et le nombre de tours pour une s\u00e9ance plus courte ;\n- Cr\u00e9er de nouvelles qu\u00eates et extensions qui s'attardent sur certains types de contributions plus exigeantes (\u00ab\u202fCr\u00e9er son article\u202f\u00bb). Ou encore une extension sur la contribution de sources qui int\u00e9gre des quiz sur certaines cartes o\u00f9 le joueur\u00b7euse doit choisir la meilleure source parmi plusieurs, etc.","width":"600px","height":"399.5px","transform":"translate(84px, 4428px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672652631639","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Licence : [**CC BY 3.0 FR** ](https:\/\/creativecommons.org\/licenses\/by\/3.0\/fr\/)","width":"348px","height":"77.5px","transform":"translate(912px, 492px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1672662426129","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-12-26

----

Coverthumb: https://praticable.fr/media/pages/productions/jeu-pedagogique-wikipedia/sauvez-wiki/c2212c3b17-1672056255/jeu-wikipedia-1_praticable.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: nq0wXOIKaFLjNdef