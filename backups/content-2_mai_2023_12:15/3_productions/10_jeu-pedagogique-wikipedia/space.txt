Title: Jeu pédagogique pour Wikipédia

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# Jeux p\u00e9dagogiques autour de Wikip\u00e9dia","width":"792px","height":"200px","transform":"translate(72px, 96px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false","originalContent":"# Jeux p\u00e9dagogiques autour de Wikip\u00e9dia"},"id":"1672053795545","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Pour initier \u00e0 la contribution sur Wikip\u00e9dia avant de se lancer r\u00e9ellement sur la plateforme, nous avons imagin\u00e9 deux jeux coop\u00e9ratifs en papier \u00e0 fabriquer facilement chez soi.","width":"792px","height":"104px","transform":"translate(72px, 324px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false","originalContent":"Pour initier \u00e0 la contribution sur Wikip\u00e9dia avant de se lancer r\u00e9ellement sur la plateforme, nous avons imagin\u00e9 deux jeux coop\u00e9ratifs en papier \u00e0 fabriquer facilement chez soi."},"id":"1672053893362","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"\ud83d\udc46 _Ces jeux sont encore \u00e0 l'\u00e9tat d'\u00e9bauche, certains aspects graphiques ou de d\u00e9veloppement n'ont pas \u00e9t\u00e9 trait\u00e9s. Nous avions lanc\u00e9 cette r\u00e9flexion suite \u00e0 un appel de l'association [Wikimedia France](https:\/\/www.wikimedia.fr\/). Nous documentons ici les deux jeux n'ayant pas \u00e9t\u00e9 retenus pour qu'ils puissent \u00eatre utiles.\nSi vous voulez contribuer contactez-nous !_","width":"408px","height":"266.5px","transform":"translate(900px, 468px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false","originalContent":"\ud83d\udc46 _Ces jeux sont encore \u00e0 l'\u00e9tat d'\u00e9bauche, certains aspects graphiques ou de d\u00e9veloppement n'ont pas \u00e9t\u00e9 trait\u00e9s. Nous avions lanc\u00e9 cette r\u00e9flexion suite \u00e0 un appel de l'association [Wikimedia France](https:\/\/www.wikimedia.fr\/). Nous documentons ici les deux jeux n'ayant pas \u00e9t\u00e9 retenus pour qu'ils puissent \u00eatre utiles.\nSi vous voulez contribuer contactez-nous !_"},"id":"1672671156663","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Le jeu pour lever les freins \u00e0 la contribution\nDe par sa nature, le jeu permet de surmonter diff\u00e9rents obstacles \u00e0 la contribution. Dans un jeu, on ose davantage. Le jeu est fonci\u00e8rement d\u00e9sacralisant, d\u00e9sinhibant. En rendant l\u2019\u00e9chec fictif, en le virtualisant, il installe un espace-temps l\u00e9ger, pr\u00e9serv\u00e9 des cons\u00e9quences ordinaires et susceptible ici de lib\u00e9rer l\u2019action contributive des entraves courantes.","width":"792px","height":"281px","transform":"translate(72px, 468px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false","originalContent":"### Le jeu pour lever les freins \u00e0 la contribution\nDe par sa nature, le jeu permet de surmonter diff\u00e9rents obstacles \u00e0 la contribution. Dans un jeu, on ose davantage. Le jeu est fonci\u00e8rement d\u00e9sacralisant, d\u00e9sinhibant. En rendant l\u2019\u00e9chec fictif, en le virtualisant, il installe un espace-temps l\u00e9ger, pr\u00e9serv\u00e9 des cons\u00e9quences ordinaires et susceptible ici de lib\u00e9rer l\u2019action contributive des entraves courantes."},"id":"1672670893030","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Les enjeux des modes de jeu\nJeu de bac \u00e0 sable, de craft, de r\u00f4le, de r\u00e9flexion, de deck\u2026 Il existe une formidable diversit\u00e9 de types suivant lesquels varient les effets des jeux sur les joueurs. Car les jeux ne font pas seulement quelque chose pour nous, ils font quelque choses de nous. Certains d\u00e9veloppent notre cr\u00e9ativit\u00e9, d\u2019autres notre paresse, notre rigueur ou notre impatience, notre logique\u2026 Cela dit, il importe de choisir des modes coh\u00e9rents par rapport aux savoirs, savoir-faire et savoir-\u00eatre qu\u2019il s\u2019agit de d\u00e9velopper. \n\nCes deux jeux rel\u00e8vent de deux modes qui r\u00e9pondent \u00e0 deux dimensions au c\u0153ur du projet de Wikip\u00e9dia : **le r\u00f4le et l\u2019enqu\u00eate**. \n\nUn jeu de r\u00f4le, pour appr\u00e9hender le fonctionnement des r\u00f4les contributifs.\n\nUn jeu d\u2019enqu\u00eate pour adopter la conduite de recherche au c\u0153ur de toute entreprise de connaissance. \n\nDeux jeux coop\u00e9ratifs, pour d\u00e9velopper en tous les cas la discussion, la coordination, la diversit\u00e9 d\u2019approches et la camaraderie durant le jeu et apr\u00e8s, entre \u00e9l\u00e8ves et avec les \u00e9ventuels accompagnants, professeurs, documentalistes ou autres responsables p\u00e9dagogiques.","width":"585px","height":"654.5px","transform":"translate(504px, 792px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false","originalContent":"### Les enjeux des modes de jeu\nJeu de bac \u00e0 sable, de craft, de r\u00f4le, de r\u00e9flexion, de deck\u2026 Il existe une formidable diversit\u00e9 de types suivant lesquels varient les effets des jeux sur les joueurs. Car les jeux ne font pas seulement quelque chose pour nous, ils font quelque choses de nous. Certains d\u00e9veloppent notre cr\u00e9ativit\u00e9, d\u2019autres notre paresse, notre rigueur ou notre impatience, notre logique\u2026 Cela dit, il importe de choisir des modes coh\u00e9rents par rapport aux savoirs, savoir-faire et savoir-\u00eatre qu\u2019il s\u2019agit de d\u00e9velopper. \n\nCes deux jeux rel\u00e8vent de deux modes qui r\u00e9pondent \u00e0 deux dimensions au c\u0153ur du projet de Wikip\u00e9dia : **le r\u00f4le et l\u2019enqu\u00eate**. \n\nUn jeu de r\u00f4le, pour appr\u00e9hender le fonctionnement des r\u00f4les contributifs.\n\nUn jeu d\u2019enqu\u00eate pour adopter la conduite de recherche au c\u0153ur de toute entreprise de connaissance. \n\nDeux jeux coop\u00e9ratifs, pour d\u00e9velopper en tous les cas la discussion, la coordination, la diversit\u00e9 d\u2019approches et la camaraderie durant le jeu et apr\u00e8s, entre \u00e9l\u00e8ves et avec les \u00e9ventuels accompagnants, professeurs, documentalistes ou autres responsables p\u00e9dagogiques."},"id":"1672671350217","isHidden":false,"type":"markdown"},{"content":{"image":["jeu-wikipedia-vignette_praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"585px","height":"2px","transform":"translate(504px, 1488px)","zindex":0,"iscover":"true"},"id":"67bacc14-e96c-435f-a7d3-4cba430128c2","isHidden":false,"type":"image"},{"content":{"title":"Sauvez Wiki : un jeu de r\u00f4le","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/jeu-pedagogique-wikipedia\/sauvez-wiki\/c2212c3b17-1672056255\/jeu-wikipedia-1_praticable.jpg","intro":"","width":"384px","height":"131px","transform":"translate(72px, 792px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/jeu-pedagogique-wikipedia\/sauvez-wiki","tags":"","linkedspaces":[]},"id":"productions\/jeu-pedagogique-wikipedia\/sauvez-wiki","isHidden":false,"type":"representative"},{"content":{"title":"Sous le Masque : un jeu d'enqu\u00eate","cover":"https:\/\/praticable.fr\/media\/pages\/productions\/jeu-pedagogique-wikipedia\/sous-le-masque\/89d622ead5-1672056948\/jeu-wikipedia-2_praticable.jpg","intro":"","width":"384px","height":"131px","transform":"translate(72px, 1212px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/productions\/jeu-pedagogique-wikipedia\/sous-le-masque","tags":"","linkedspaces":[]},"id":"productions\/jeu-pedagogique-wikipedia\/sous-le-masque","isHidden":false,"type":"representative"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: Jeux pédagogiques à fabriquer soi-même pour initier à la contribution sur Wikipédia

----

Author: 

----

Published: 2022-12-26

----

Coverthumb: https://praticable.fr/media/pages/productions/jeu-pedagogique-wikipedia/72e10687fa-1672675299/jeu-wikipedia-vignette_praticable.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: vd2NvZIyVMB5FJc1