Title: Exco immersion

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"#### Bienvenus \u00e0 la Convention Annuelle 2037,\n\nLa direction nous a confi\u00e9 l\u2019organisation du moment. Nous l\u2019avons fait avec ceci \u00e0 l\u2019esprit que, malgr\u00e9 les am\u00e9liorations constat\u00e9es, nous pouvons et nous devons **faire mieux**. Il nous faut d\u2019abord ce courage, reconna\u00eetre que la transformation engag\u00e9e en 2022, assur\u00e9ment b\u00e9n\u00e9fique, n\u2019est pas \u00e0 la hauteur. \n\nC\u2019est \u00e0 nous que revient cette fois la responsabilit\u00e9 de l\u2019organisation de la convention annuelle et nous estimons que le mouvement doit \u00eatre radical, rapide et, par-dessus tout, doit provenir de nous toutes et tous, \u00e9maner de partout dans l\u2019entreprise et l\u2019irriguer en profondeur.\n\n**Soyons critique**, mais servons-nous aussi des le\u00e7ons des succ\u00e8s pass\u00e9s. Gr\u00e2ce \u00e0 l\u2019\u00e9coute de la direction et \u00e0 l\u2019implication d\u2019individus dispers\u00e9s dans le groupe, nous avons engag\u00e9 le d\u00e9bat, multipli\u00e9 les exp\u00e9rimentations, travaill\u00e9 avec d\u2019autres organisations qui tentaient, dans le m\u00eame sens que nous, d\u2019**assumer au mieux leur responsabilit\u00e9 vis-\u00e0-vis de la soci\u00e9t\u00e9 et de la plan\u00e8te**.\n\nIl a fallu pour cela quitter les rails. Frayer un chemin n\u2019est jamais chose facile et nous avons \u00e0 l\u2019occasion essuy\u00e9 quelques \u00e9checs mais \u2013 tout de m\u00eame \u2013 ne nous privons pas de dire les bonnes nouvelles, que l\u2019empreinte carbone nette d\u2019Exco est d\u00e9sormais n\u00e9gative, que nous sommes une entreprise \u00e0 mission reconnue comme telle, labellis\u00e9e Bcorp, pionni\u00e8re en mati\u00e8re de comptabilit\u00e9 \u00e0 triple capital, motrice au sein des blockchain corps, rompue aux analyses de cycles de vie, r\u00e9put\u00e9e et toujours plus appel\u00e9e pour ce type de savoir-faire qui red\u00e9finissent concr\u00e8tement le sens de nos m\u00e9tiers. Nos m\u00e9thodes et, peut-\u00eatre plus fondamentalement, notre vision en inspire d\u2019autres que nous. \u00c0 notre niveau, nous contribuons aux vastes changements que la situation exige. Voyez d\u2019ailleurs les jeunes comptables qui frappent \u00e0 notre porte pour se joindre \u00e0 nos efforts, pour fertiliser les possibles !\n\nPourtant, la situation globale exige de notre part des modifications plus profondes encore. Les effets du changement climatique (et les r\u00e9glementations qui les prennent en compte), les nouvelles technologies, les **nouvelles formes d\u2019entreprises et de travail** : tout change autour de nous et nous adopter ces changements, les faire n\u00f4tres. Pensons au bien qu\u2019ont d\u00e9j\u00e0 apport\u00e9 les changements r\u00e9alis\u00e9s et r\u00e9jouissons-nous de ceux que nous avons \u00e0 inventer, \u00e0 produire !\n\n2022 amor\u00e7ait une transformation\u00a0 ; 2037 vise une r\u00e9volution. Il est question des fondements de notre entreprise et nous sommes ici pour en poser les bases ensemble. Que voulons-nous \u00eatre et faire ? Comment fertiliser ces nouvelles formes d\u2019organisation qui poussent autour de nous ? Lesquels nous correspondent et comment les accompagner ? Quelle place tenir d\u00e9sormais parmi les technologies dites \u201cintelligentes\u201d ? Quand et comment veut-on s\u2019en saisir ? Quand estime-t-on pr\u00e9f\u00e9rable de se tenir \u00e0 distance, d\u2019\u00e9lire des techniques plus sobres ?\n\nDe quoi fertiliseur est-il le nom ? Quelles bonnes pousses, quels bons fruits, quelle culture peuvent accompagner nos pratiques d\u2019auditeurs, d\u2019experts comptables ? **Qu\u2019est-ce qui compte ?** Qu\u2019est-ce qui a de la valeur ? Qu\u2019est-ce qui est capital ? \u00c0 quoi faut-il accorder du cr\u00e9dit ? Est-on bien s\u00fbr de compter ce qui compte vraiment ? De notre compr\u00e9hension de ces notions fondamentales d\u00e9pendent notre activit\u00e9 et le syst\u00e8me \u00e9conomique qu\u2019elle soutient. Il est temps de questionner, de nous questionner. Quitte \u00e0 bousculer nos pratiques, nos outils, nos m\u00e9thodes. Quitte \u00e0 changer. Et naturellement, comme depuis 2022 : **\u201cCeux qui pensent que c'est impossible sont pri\u00e9s de ne pas d\u00e9ranger ceux qui essaient\u201d.**","width":"585px","height":"1676px","transform":"translate(150px, 300px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666184056185","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"couleur-fond:#000\ncouleur-texte:#FFF\n\n#### Lettre \u00e0 Exco de 2037","width":"585px","height":"79px","transform":"translate(150px, 195px)","zindex":0,"backgroundcolor":"#000","textcolor":"#FFF","refs":"","isintro":"false","isedit":"false"},"id":"1666184108884","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"couleur-fond:#000\ncouleur-texte:#FFF\n\n#### Questions immersives","width":"585px","height":"79px","transform":"translate(780px, 195px)","zindex":0,"backgroundcolor":"#000","textcolor":"#FFF","refs":"","isintro":"false","isedit":"false"},"id":"1666184166352","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Nous sommes en 2037, ...\n\n1. Comment \u00eates-vous venus \u00e0 Marseille ? \n2. Qu'est-ce-que vous voyez par votre fen\u00eatre ?\n3. Qu'avez-vous mang\u00e9 au petit-d\u00e9jeuner ?\n4. Dans quel type de logement vivez-vous ?\n5. Quel est le titre du journal ce matin ?\n6. De quoi \u00eates-vous nostalgique ?\n7. Qu'est-ce-qui vous ravit aujourd'hui ?\n8. Qu'est-ce-que vous pratiquez comme loisir ?\n9. Quel temps fait-il aujourd'hui ?\n10. Qu'est-ce-qui a \u00e9t\u00e9 invent\u00e9 cette ann\u00e9e ?","width":"585px","height":"347px","transform":"translate(780px, 300px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666184195336","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"couleur-fond:#000\ncouleur-texte:#FFF\n\n#### Des r\u00f4les pour animer l'atelier","width":"585px","height":"79px","transform":"translate(780px, 690px)","zindex":0,"backgroundcolor":"#000","textcolor":"#FFF","refs":"","isintro":"false","isedit":"false"},"id":"1666184595500","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Les 400 participants ont \u00e9t\u00e9 r\u00e9parti en groupes de dix personnes. Chacune avait un r\u00f4le \u00e0 tenir durant l'atelier.","width":"585px","height":"104px","transform":"translate(780px, 795px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666184620599","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"**1. Le demandeur senior**\nVous \u00eates les auteurs de la demande dans la troisi\u00e8me partie de l\u2019atelier. Avec votre acolyte appropriez-vous la demande, accentuez les points qui vous semblent importants incarnez-la pour pouvoir travailler avec les collaborateurs Exco. Imaginez ce que nous avons oubli\u00e9 dans le texte. \n\n**2. Le demandeur junior**\nVous \u00eates les auteurs de la demande dans la troisi\u00e8me partie de l\u2019atelier. Avec votre acolyte appropriez-vous la demande, accentuez les points qui vous semblent importants incarnez-la pour pouvoir travailler avec les collaborateurs Exco. Imaginez ce que nous avons oubli\u00e9 dans le texte. \n\n**3. Le m\u00e9tier myst\u00e8re**\nVous \u00eates le nouvel atout d\u2019Exco en 2037. Votre m\u00e9tier n\u2019existe pas encore, mais il aura \u00e9t\u00e9 cr\u00e9\u00e9 en fonction des enjeux et des transformations d\u2019Exco au fil des 15 prochaines ann\u00e9es. D\u00e9finissez avec le groupe les caract\u00e9ristiques de ce nouveau m\u00e9tier durant la troisi\u00e8me partie de l\u2019atelier. \n\n**4. L\u2019optimiste**\nPour vous tout est possible, vous n\u2019h\u00e9sitez pas \u00e0 pousser toutes les bonnes id\u00e9es, vous y croyez avant tout le monde. \n\n**5. Le terre \u00e0 terre**\nVous \u00eates celui qui rationalise et p\u00e8se les risques. Sans bloquer le groupe, vous rebondissez sur les propositions en ramenant au concret et \u00e0 la r\u00e9alit\u00e9. \n\n**6. L\u2019empathique**\nConnect\u00e9 \u00e0 vos ressentis, vous \u00eates en empathie avec les autres. Votre principal souci est le bien-\u00eatre des personnes qui vous entourent, mais aussi du vivant.\n\n**7. Le ponctuel**\nVous voulez avancer, \u00eatre \u00e0 l\u2019heure - pour le d\u00e9jeuner, vous \u00eates le ma\u00eetre du temps. Assurez-vous de la bonne tenue du timing tout au long de l\u2019atelier. N\u2019h\u00e9sitez \u00e0 utiliser un minuteur.\n\n**8. Le scribe**\nC\u2019est une bonne situation \u00e7a, scribe ! Vous avez besoin de fixer les mots, de rendre compte par l\u2019\u00e9crit des \u00e9changes entres les collaborateurs. Vous \u00eates le scribe du groupe, mais tous les collaborateurs sont invit\u00e9s \u00e0 annoter, surligner, etc.  \n\n**9. Le cr\u00e9atif**\nVous faites des propositions audacieuses et valorisez les id\u00e9es de tout le groupe. M\u00eame les plus farfelues et surprenantes. Vous y voyez les pr\u00e9mices des plus belles possibilit\u00e9s. \n\n**10. Le curieux**\nVous ne savez jamais pourquoi. Alors vous demandez. Vous avez \u00e0 c\u0153ur de bien comprendre les id\u00e9es de chacun\u00b7e ; pour ceci vous osez poser beaucoup de questions. Allez savoir pourquoi...","width":"585px","height":"1329px","transform":"translate(780px, 930px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666186287710","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Ces \u00e9l\u00e9ments ont favoris\u00e9 la projection des participant en 2037, et les a aid\u00e9 \u00e0 formuler des interrogations et des propositions innovantes pour l'atelier.","width":"555px","height":"130px","transform":"translate(150px, 45px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667556466790","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### [Retour](\/productions\/atelier-prospective\/)","width":"120px","height":"79px","transform":"translate(1395px, 300px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1667578764688","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: true

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-10-19

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: Xw6NlJWBIRrNbHvd