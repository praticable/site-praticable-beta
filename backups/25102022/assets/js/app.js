import Blocks from './components/blocks/blocks.js'
import Store from './store.js'
import apparatus from './components/apparatus/apparatus.js';
import contextMenuMixin from './components/apparatus/mixins/context-menu-mixin.js';
import Colors from './components/buttons/colors.js';

window.EventBus = new Vue();

if (document.querySelector('#app')) {
  const app = new Vue({
    el: '#app',
    mixins: [contextMenuMixin],
    components: {
      'apparatus': apparatus,
      'blocks': Blocks,
      'colors': Colors
    },
    data: {
      rootUrl: rootUrl,
      store: Store,
      index: index,
      tags: tags,
      page: pageUrl,
      apiUrl: apiUrl,
      updateSignal: 0,
      maxZIndex: null,
      minZIndex: null,
      isEditMode: true,
      isLog: isLog,
      debug: true,
      palette: palette,
      pageColorsLabel: pageColorsLabel,
      breadcrumb: breadcrumb
    },
    computed: {
      new: function () {
        return this.blocks.find(block => block.isNew) || false
      },
      highestBlock: function () {
        let highestBlock = undefined
        this.blocks.forEach(block => {
          if (highestBlock === undefined) {
            highestBlock = block
          } else {
            if (block.content.zindex > highestBlock.content.zindex) {
              highestBlock = block
            }
          }
        })
        if (highestBlock == undefined) return false
        return highestBlock || false
      },
      lowestBlock: function () {
        let lowestBlock = undefined
        this.blocks.forEach(block => {
          if (lowestBlock === undefined) {
            lowestBlock = block
          } else {
            if (block.content.zindex < lowestBlock.content.zindex) {
              lowestBlock = block
            }
          }
        })
        return lowestBlock || false
      },
      isParentPage: function () {
        if (this.blocks.some(block => block.type === 'page')) {
          return true
        } else {
          return false
        }
      },
      nbrEditedBlocks: function () {
        let nbrEditedBlocks = []
        nbrEditedBlocks = this.blocks.filter(block => block.isEdited)
        return nbrEditedBlocks.length
      },
      coverImageBlock: function () {
        return this.blocks.find(block => block.content.iscover == true) || false
      },
      blocks: function () {
        if (this.store.state.filters.length === 0) {
          return this.store.state.blocks
        } else {
          return this.store.state.blocks.filter(block => {
            if (block.type !== 'representative') return true
            const tags = block.content.tags.split('/')
            return tags.some(tag => {
              return this.store.state.filters.includes(tag)
            })
          })
        }
      },
      pageColors: function () {
        return this.palette.find(color => color.label === this.pageColorsLabel)
      }
    },
    watch: {
      highestBlock: {
        handler: function (highestBlock) {
          if (highestBlock == undefined || highestBlock.content === undefined) return
          this.maxZIndex = parseInt(highestBlock.content.zindex)
        },
        immediate: true
      },
      lowestBlock: {
        handler: function (lowestBlock) {
          if (lowestBlock == undefined || lowestBlock.content === undefined) return
          this.minZIndex = parseInt(lowestBlock.content.zindex)
        },
        immediate: true
      },
      isEditMode: function(isEditMode) {
        if (isEditMode) {
          document.querySelector('html').classList.add('grid')
        } else {
          document.querySelector('html').classList.remove('grid')
        }
      },
      pageColors: function(pageColors) {
        document.querySelector('html').style.color = pageColors.text
        document.querySelector('html').style.backgroundColor = pageColors.background      
      }
    },
    methods: {
      initiaOffset: function (selector) {
        const initialOffset = {
          x: document.querySelector(selector).getBoundingClientRect().x,
          y: document.querySelector(selector).getBoundingClientRect().y
        }
  
        return initialOffset
      },
      setInteract: function () {
        let position
        const snapStep = 15
        const initialOffset = this.initiaOffset('.blocks')
        interact('.block:not(.edit, .lock)')
          .resizable({
            edges: {
              top: false,
              left: false,
              bottom: false,
              right: true
            },
            listeners: {
              move: (event) => {
                this.store.state.isUpToDate = false
                let {
                  x,
                  y
                } = event.target.dataset
  
                x = (parseFloat(x) || 0) + event.deltaRect.left
                y = (parseFloat(y) || 0) + event.deltaRect.top
  
                Object.assign(event.target.style, {
                  width: `${event.rect.width}px`,
                  height: `${event.rect.height}px`
                })
  
                Object.assign(event.target.dataset, {
                  x,
                  y
                });
              }
            },
            modifiers: [
              interact.modifiers.snapSize({
                targets: [{
                    width: 100
                  },
                  interact.snappers.grid({
                    width: snapStep,
                    height: snapStep
                  }),
                ],
              })
            ],
          })
        interact('.block--image.selected')
          .draggable({
            autoScroll: true,
            listeners: {
              start(event) {
                position = {
                  x: event.target.getBoundingClientRect().x - initialOffset.x,
                  y: event.target.getBoundingClientRect().y - initialOffset.y
                }
              },
              move(event) {
                position.x += event.dx
                position.y += event.dy
  
                event.target.style.transform =
                  `translate(${position.x}px, ${position.y}px)`
              }
            },
  
            modifiers: [
              interact.modifiers.snap({
                targets: [
                  interact.snappers.grid({
                    x: snapStep,
                    y: snapStep
                  })
                ]
              })
            ]
          })
          .resizable({
            edges: {
              top: false,
              left: false,
              bottom: true,
              right: true
            },
            listeners: {
              move: (event) => {
                let {
                  x,
                  y
                } = event.target.dataset
  
                x = (parseFloat(x) || 0) + event.deltaRect.left
                y = (parseFloat(y) || 0) + event.deltaRect.top
  
                Object.assign(event.target.style, {
                  width: `${event.rect.width}px`,
                  height: `${event.rect.height}px`
                })
  
                Object.assign(event.target.dataset, {
                  x,
                  y
                });
              }
            },
            modifiers: [
              interact.modifiers.snapSize({
                targets: [{
                    width: 100
                  },
                  interact.snappers.grid({
                    width: snapStep,
                    height: snapStep
                  }),
                ],
              }),
              interact.modifiers.aspectRatio({
                // make sure the width is always double the height
                ratio: 'preserve'
              })
            ],
          })
        interact('.free .block, #selection')
          .draggable({
            autoScroll: {
              container: window,
              distance: 100,
              interval: snapStep,
              speed: 10000
            },
            listeners: {
              start: (event) => {
                position = {
                  x: event.target.getBoundingClientRect().x - (initialOffset.x - window.scrollX),
                  y: event.target.getBoundingClientRect().y - (initialOffset.y - window.scrollY)
                }
              },
              move: (event) => {
                this.store.state.isUpToDate = false
                position.x += event.dx
                position.y += event.dy
  
                position.x = Math.round(position.x / snapStep) * snapStep
                position.y = Math.round(position.y / snapStep) * snapStep
  
                event.target.style.transform =
                  `translate(${position.x}px, ${position.y}px)`
              },
              end(event) {
                document.querySelector('#app').style.width = ''
                document.querySelector('#app').style.height = ''
              }
            },
            modifiers: [
              interact.modifiers.snap({
                targets: [
                  interact.snappers.grid({
                    x: snapStep,
                    y: snapStep
                  })
                ]
              })
            ]
          })
  
      },
      addState: function () {
        this.blocks.forEach(block => {
          block.isEdit = false
          block.isEdited = false
          block.isNew = false
        })
      },
      add: function (block) {
        if (this.$root.debug) {
          console.log(this.$options.el + ' add: ', block)
        }
        this.blocks.push(block)
      },
      getEditedBlocks: function () {
        const editedBlocks = this.blocks.filter(block => !block.isNew)
        if (editedBlocks.length > 0) {
          return editedBlocks
        } else {
          return false
        }
        if (this.$root.debug) {
          console.log(this.$options._componentTag + ' editedBlocks: ', editedBlocks)
        }
      },
      removeNewBlock: function () {
        if (!this.store.state.currentEditBlock) {
          this.store.removeNewBlock()
  
          this.$emit('reset-options')
          if (this.debug) {
            console.log(this.$options.el + ' removeNewBlock')
          }
        }
      },
      keysShortcut: function () {
        document.addEventListener('keyup', event => {
          if (event.key === 'Escape') {
            this.removeNewBlock()
          }
  
          if (event.key === 'Backspace') {
            // this.remove()
          }

          if (this.store.state.layout === 'full') {
            if (event.key === 'ArrowLeft') {
              document.querySelector('#slide-nav__prev').classList.add('active')
              setTimeout(() => {
                document.querySelector('#slide-nav__prev').classList.remove('active')
              }, 100)
              if (this.store.state.slideIndex === 0) {
                this.store.state.slideIndex = this.store.state.blocks.length - 1
              } else {
                this.store.state.slideIndex--
              }
            } else if (event.key === 'ArrowRight') {
              document.querySelector('#slide-nav__next').classList.add('active')
              setTimeout(() => {
                document.querySelector('#slide-nav__next').classList.remove('active')
              }, 100)
              if (this.store.state.slideIndex === this.store.state.blocks.length - 1) {
                this.store.state.slideIndex = 0
              } else {
                this.store.state.slideIndex++
              }
            }
          }
        })
      },
      setPageColors: function() {
        document.querySelector('html').style.backgroundColor = this.pageColors.background
        document.querySelector('html').style.color = this.pageColors.text
      },
      hideContextMenu: function() {
        document.querySelector('#context-menu').classList.add('hidden')
      },
      resetScroll: function() {
        window.scrollTo(0, 0)
      }
    },
    mounted: function () {
      this.resetScroll()
      this.store.setBlocks(blocks)
      this.setInteract()
      this.keysShortcut()
      if (!this.debug) {
        window.addEventListener('beforeunload', (event) => {
          if (this.store.state.isUpToDate) return
          event.returnValue = "Certaines modifications ne sont pas enregistrées. Êtes-vous sûr·e de vouloir quitter la page ?"
          confirm("Certaines modifications ne sont pas enregistrées. Êtes-vous sûr·e de vouloir quitter la page ?")
        })
      }
    }
  })
}


