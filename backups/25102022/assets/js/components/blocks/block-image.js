import blockMixin from "./block-mixin.js"

const BlockImage = {
    mixins: [blockMixin],
    template: `
        <input 
          v-if="!url" 
          
          type="file" 
          ref="myFile" 
          accept="image/png, image/jpeg"
          
          @change="upload" 
        >
        <figure class="flex" v-else>
            <img :src="block.content.thumb" ref="block" alt="" />
        </figure>
    `,
    computed: {
        url: function() {
            if (this.block.content.image !== undefined) {
              const imageName = this.block.content.image[0].substr(this.block.content.image[0].lastIndexOf('/') + 1);
              const isHome = this.page === 'Cultures visuelles'
              if (isHome) {
                return window.location.pathname + '/home/' + imageName || false
              } else {
                return window.location.pathname + '/' + imageName || false
              }
            } else {
                return false
            }
        }
    },
    methods: {
        toggleCover: function() {
          const bool = this.block.content.iscover
          this.store.removeCover()
          if (bool == 'true') {
            this.block.content.iscover = 'false'
          } else {
            this.block.content.iscover = 'true'
          }
          this.store.state.isUpToDate = false
        },
        upload: function() {
            const file = this.$refs.myFile.files[0]
            this.setBase64(file)
            this.setName(file)
            if (!this.block.content.iscover) {
              this.block.content.iscover = false
            }
            this.$root.updateSignal++
            setTimeout(() => {
                this.send()
            }, 500);
        },
        setBase64: function(file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                const base64String = reader.result.split(',')[1]
                
                this.block.b64 = base64String
            };
            reader.readAsDataURL(file);
        },
        setName: function(file) {
            this.block.content.image = [{
                filename: file.name
            }]
        },
        send: function() {
            const data = {
                page: this.$root.page,
                blocks: this.$root.blocks,
                colors: this.$root.pageColorsLabel
            }
            const init = {
                method: 'POST',
                body: JSON.stringify(data)
            }
            if (this.$root.debug) {
                console.log(this.$options._componentTag + ' send: ', data)
            }
            
            fetch(`./save.json`, init)
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    if (this.$root.debug) {
                        console.log(this.$options.el + ' save.json return: ', data)
                    }
                    this.store.state.isUpToDate = true
                    EventBus.$emit('stop-save-state')
                    location.reload()
                    // const subpagesBlocks = this.$root.blocks.filter(block => block.type === 'page' || block.type === 'subpage-cover')
                    // this.store.setBlocks(subpagesBlocks.concat(data.blocks))
                    // this.store.state.isUpToDate = true
                })
        }
    }
}

export default BlockImage