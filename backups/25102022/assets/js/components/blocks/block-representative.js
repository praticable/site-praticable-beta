const BlockRepresentative = {
  props: {
    'block': Object
  },
  computed: {
    markedIntro: function() {
      if (this.block.content.intro.length === 0) return false
      marked.setOptions({
          breaks: true,
          smartypants: true
      })
      const markedIntro = marked.parse(this.block.content.intro)
      return markedIntro
    }
  },
  template: `
    <div ref="block">
      <div v-if="block.content.cover.length > 0" class="block--representative__image">
        <a :href="block.content.url" :title="'Aller à la page ' + block.content.title"></a>
        <figure :style="'z-index: ' + (block.content.zindex - 1)">
          <img :src="block.content.cover" alt="">
        </figure>
      </div>
      <div class="block--representative__sum">
        <h2><a :href="block.content.url" :title="'Aller à la page ' + block.content.title">{{ block.content.title }} ➞</a></h2>
        <p v-if="markedIntro" class="text-3" v-html="markedIntro"></p>
      </div>
    </div>
  `
}

export default BlockRepresentative