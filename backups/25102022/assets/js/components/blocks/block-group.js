import Store from "../../store.js"
import blockMixin from "./block-mixin.js"
import SubBlock from "./subBlock.js"

const blockGroup = {
  mixins: [blockMixin],
  components: {
    'sub-block': SubBlock
  },
  data: function() {
    return {
      store: Store
    }
  },
  template: `
    <div class="group">
      <h4>Blocs dans le groupe : </h4>
      <ul>
        <li v-for="block in block.content.blocks"><button>- {{ block.id }}</button></li>
      </ul>
      <h4>Blocs disponibles :</h4>
      <ul>
        <li v-for="block in store.state.blocks" @click="add(block)" v-if="block.type !== 'group'"><button>- {{ block.id }}</button></li>
      </ul>
      <sub-block
        v-for="block in block.content.blocks" 
        :block="block" 
        :update-signal="updateSignal"
        :page="page"
        :key="block.id"

        @select-block="selectBlock"
      >

      </sub-block>
    </div>
  `,
  methods: {
    add: function(block) {
      const groupId = this.block.id
      this.store.addToGroup(block.id, groupId)
    }
  }
}

export default blockGroup