import Store from "../../store.js"
import noteMixin from "./note-mixin.js"

const BlockText = {
    props: {
        block: Object,
        isEdit: Boolean
    },
    mixins: [noteMixin],
    data: function() {
      return {
        store: Store
      }
    },
    computed: {
        parsedText: function() {          
          const iframedText = this.block.content.text.replaceAll('::', '<').replaceAll('&&', '>')

          marked.setOptions({
              breaks: true,
              smartypants: true
          })
          const markedText = marked.parse(iframedText)

          const reiframedText = markedText.replace(/&amp;&amp;/gm, '>')
          
          let noteIndex = 0
          const notedMdText = reiframedText.replaceAll(/\[[0-9]+\]/g, match => {
            const html = `<a href="#${this.splitedRefs[noteIndex]}" class="note" id="${this.block.id}">${match}</a>`
            noteIndex++
            return html
          })

          return notedMdText
        },
        splitedRefs: function() {
          if (!this.block.content.refs.length) return []
          return this.block.content.refs.split(', ')
        }
    },
    template: `
        <div>
            <div v-if="block.content.text.length > 0" v-html="parsedText" ref="content"></div>
            <div v-else ref="content"><p>Clic droit pour éditer.</p></div>
        </div>
    `,
    methods: {
      enableNoteHighlight: function() {
        const notes = this.$refs.content.querySelectorAll('.note')
        notes.forEach((note, index) => {
          note.removeEventListener('mouseenter', this.highlight)
          note.removeEventListener('mouseleave', this.unhighlight)
          note.addEventListener('mouseenter', this.highlight)
          note.addEventListener('mouseleave', this.unhighlight)
        })
      },
      highlight: function(e) {
        const textContent = e.target.textContent
        const regex = /\[([0-9]+)\]/
        const noteIndex = parseInt(textContent.match(regex).pop())
        const refIndex = (noteIndex - 1) - (this.noteFirstIndex - 1)
        const refId = this.splitedRefs[refIndex]
        this.store.highlightBlockById(this.block.id)
        this.store.highlightBlockById(refId)
      },
      unhighlight: function(e) {
        const textContent = e.target.textContent
        const regex = /\[([0-9]+)\]/
        const noteIndex = parseInt(textContent.match(regex).pop())
        const refIndex = (noteIndex - 1) - (this.noteFirstIndex - 1)
        const refId = this.splitedRefs[refIndex]
        this.store.unhighlightBlockById(this.block.id)
        this.store.unhighlightBlockById(refId)
      }
    },
    mounted: function() {
      this.enableNoteHighlight()
    }
}

export default BlockText