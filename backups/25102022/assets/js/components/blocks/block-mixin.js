import Store from "../../store.js";
import contextMenuMixin from "../apparatus/mixins/context-menu-mixin.js";

const blockMixin = {
  props: {
    block: Object,
    updateSignal: Number,
    page: String
  },
  mixins: [contextMenuMixin],
  data: function () {
    return {
      store: Store
    }
  },
  watch: {
    isEdit: function (newIsEdit) {
      setTimeout(() => {
        if (newIsEdit === true) {
          this.focusEditNode()
          this.store.state.currentEditBlock = this.block
        } else {
          this.store.state.currentEditBlock = false
        }
      }, 100)
    }
  },
  methods: {
    focusEditNode: function() {
      if (document.querySelector('.edit')) {
        document.querySelector('.edit').firstChild.focus()
      }
    },
    slugify: function (str) {
      str = str.replace(/^\s+|\s+$/g, '');

      // Make the string lowercase
      str = str.toLowerCase();

      // Remove invalid chars except last dot (.extension)
      const last = str.lastIndexOf('.')
      const butLast = str.substring(0, last).replace(/\./g, '')
      str = butLast + str.substring(last)

      // Remove accents, swap ñ for n, etc
      const from = "áäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;"
      const to = "aaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------"
      for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
      }

      // Collapse whitespace and replace by -
      str = str.replace(/\s+/g, '-')

        // Collapse dashes
        .replace(/-+/g, '-');

      return str;
    },
    select: function (e) {
      console.log(e)
      if (e.crtlKey || e.metaKey) {
        this.store.toggleSelectBlockById(this.block.id)
        document.querySelector('.blocks').removeChild(this.$refs.block)
        document.querySelector('#selection').appendChild(this.$refs.block)
      }
    },
    unselect: function () {
      document.addEventListener('click', event => {
        const isNotException = 
          !event.target.closest('.block') 
          && !event.target.closest('#z-index') 
          && !event.target.closest('#colors') 
          && !event.target.closest('textarea') 
          && !event.target.closest('#md-buttons') 
          && event.target.closest('.apparatus__btn button') !== null
          
          if (event.target.nodeName === 'HTML' || event.target.id === 'app' || event.target.classList.contains('blocks') || isNotException) {
            if (this.$root.debug) {
                console.log(this.$options._componentTag + ' unselect: ', )
            }
            this.block.content.isedit = false
          } else {
            this.store.state.currentEditBlock = false
            this.store.state.isUpToDate = false
          }
      })
      document.addEventListener('keyup', event => {
        if (event.key === 'Escape') {
          this.block.content.isedit = false
        }
      })
    },
    saveStyle: function () {
      if (this.$root.isEditMode) {
        const actualStyle = this.$refs.block.style
        this.block.content.width = actualStyle.width
        this.block.content.height = actualStyle.height
        this.block.content.transform = actualStyle.transform

        if (this.isHover === false) {
          this.block.content.zindex = parseInt(actualStyle.zIndex) || 0
        }
      }
    }
  },
  created: function () {
    this.unselect()
  },
  mounted: function () {
    if (!this.$refs.block) return
    
    this.block.content.height = window.getComputedStyle(this.$refs.block).getPropertyValue('height')
  }
}

export default blockMixin
