import Store from "../../store.js"
import Block from "./block.js"

const Blocks = {
    props: [
        'blocks', 'children', 'updateSignal', 'page'
    ],
    components: {
        'block': Block,
    },
    data: function() {
      return {
        store: Store
      }
    },
    computed: {
      isSelectionEmpty: function() {
        return this.store.state.selectedBlocks.length === 0
      }
    },
    template: `
        <section class="blocks">
            <div id="selection" :class="{ hidden: isSelectionEmpty }"></div>
            <block 
                v-for="(block, index) in blocks" 
                v-if="store.state.layout !== 'full' || index === store.state.slideIndex"
                :block="block" 
                :update-signal="updateSignal"
                :page="page"
                :key="block.id"

                @select-block="selectBlock"
            ></block>
        </section>
    `,
    methods: {
        selectBlock: function(block) {
            this.$emit('select-block', block)
            if (this.$root.debug) {
                console.log(this.$options._componentTag + ' selectBlock: ', block)
            }
        }
    }
}

export default Blocks