import blockMixin from "./block-mixin.js"
import BlockText from "./block-text.js"
import BlockImage from "./block-image.js"
import Store from "../../store.js"
import BtnsMd from "../buttons/btns-md.js"
import blockGroup from "./block-group.js"
import BlockRelated from "./block-related.js"
import BlockRepresentative from "./block-representative.js"


const Block = {
  mixins: [blockMixin],
  props: {
    block: Object,
    page: String
  },
  components: {
    'block-text': BlockText,
    'block-image': BlockImage,
    'block-related': BlockRelated,
    'block-representative': BlockRepresentative,
    'block-group': blockGroup,
    'btns-md': BtnsMd
  },
  data: function () {
    return {
      store: Store,
      heightTextarea: '',
      initialZIndex: this.block.content.zindex,
      isHover: false
    }
  },
  computed: {
    isHighlight: function() {
      return this.block.isHighlight || false
    },
    isSelected: function() {
      return this.store.state.selectedBlocks.indexOf(this.block) > -1
    }
  },
  template: `
    <div 
    @contextmenu="toggleContext"
    v-if="block.type === 'text' && (block.content.isedit == 'true' || block.content.isedit == true)">
      <btns-md
        :block="block"
        :style="
            'transform: ' + block.content.transform + ' scale(0.666); ' +
            'z-index: 999'
        "
        class="| border"
      ></btns-md>

        <textarea 
            v-if="block.type === 'text' && (block.content.isedit == 'true' || block.content.isedit == true)"
            v-model="block.content.text"
            
            ref="textarea"
            :style="
                'width: ' + block.content.width + '; ' +
                'transform: ' + block.content.transform + '; ' +
                'z-index: 999'
            " 
            class="block--text | autoExpand border" 
            placeholder="Entrez votre texte"
        />
    </div>
    <div 
      v-else
      class="block white" 
      :class="[
          'block--' + block.type,
          { 'block--text': block.type === 'subpage' },
          { edit: block.content.isEdit },
          { flex: block.type === 'options' },
          { highlight: block.isHighlight },
          { 'block--selected': isSelected }
      ]" 
      :data-type="block.type" 
      :style="
          'width: ' + block.content.width + '; ' +
          'height: ' + block.content.height + '; ' +
          'transform: ' + block.content.transform + '; ' +
          'z-index: ' + block.content.zindex + ';' +
          'background-color: ' + block.content.backgroundcolor + ';' +
          'color :' + block.content.textcolor + ';'
      "
      
      @mouseup="saveStyle"
      @mouseenter="mouseEnter"
      @mouseleave="mouseLeave"
      @contextmenu="toggleContext"
      @click="select"
      
      ref="block">
      <component 
        :is="'block-' + block.type" 
        :block="block" 
        :page="page"
        :update-signal="updateSignal"
      ></component>
    </div>
    `,
  methods: {
    getScrollHeight: function (el) {
      var savedValue = el.value
      el.value = ''
      el._baseScrollHeight = el.scrollHeight
      el.value = savedValue
    },
    mouseEnter: function() {
      this.toFirstPlan()
      if ((this.block.type === 'subpage' || this.block.type === 'subpage-cover') && this.block.content.ref.length > 0) { 
        this.store.highlightBlockByTitle(this.block.content.ref) 
        this.store.highlightBlockById(this.block.id) 
      }
    },
    mouseLeave: function() {
      this.toInitialPlan()
      if ((this.block.type === 'subpage' || this.block.type === 'subpage-cover') && this.block.content.ref.length > 0) { 
        this.store.unhighlightBlockByTitle(this.block.content.ref) 
        this.store.unhighlightBlockById(this.block.id) 
      }
    },
    toFirstPlan: function () {
      this.isHover = true
      this.$refs.block.style.zIndex = this.$root.maxZIndex + 1
    },
    toInitialPlan: function () {
      this.isHover = false
      this.$refs.block.style.zIndex = this.block.content.zindex
    }
  }
}

export default Block
