import Store from "../../store.js"
import noteMixin from "../blocks/note-mixin.js"

const BtnsMd = {
  props: {
    block: Object
  },
  mixins: [noteMixin],
  data: function () {
    return {
      store: Store
    }
  },
  template: `
    <div id="md-buttons" class="flex">
        <button 
            id="bold" 
            class="btn btn--icon"
            title="Bold" 
            @click="insert('bold')"
        >   
            <svg class="icon"><use xlink:href="#icon-bold"></use></svg>
            <span class="sr-only">Bold</span>
        </button>
        <button 
            id="italic" 
            class="btn btn--icon"
            title="Italic" 
            @click="insert('italic')"
        >   
            <svg class="icon"><use xlink:href="#icon-italic"></use></svg>
            <span class="sr-only">Italic</span>
        </button>
        <button 
            id="code" 
            class="btn btn--icon"
            title="Lien" 
            @click="insert('code')"
        >   
            <svg class="icon"><use xlink:href="#icon-code"></use></svg>
            <span class="sr-only">Lien</span>
        </button>
        <button 
            id="link" 
            class="btn btn--icon"
            title="Lien" 
            @click="insert('link')"
        >   
            <svg class="icon"><use xlink:href="#icon-link"></use></svg>
            <span class="sr-only">Lien</span>
        </button>
        <button 
            id="quote" 
            class="btn btn--icon"
            title="Citation" 
            @click="insert('quote')"
        >   
            <svg class="icon"><use xlink:href="#icon-quote"></use></svg>
            <span class="sr-only">Citation</span>
        </button>
        <button 
            id="note" 
            class="btn btn--icon"
            title="Note" 
            @click="insert('note')"
        >   
            <svg class="icon"><use xlink:href="#icon-note"></use></svg>
            <span class="sr-only">Note</span>
        </button>
        <a 
            id="markdown" 
            class="btn btn--icon"
            title="Aide markdown"
            href="https://duckduckgo.com/?t=ffab&q=markdown+cheatsheet&atb=v243-1&ia=answer&iax=1"
            target="_blank"
        >   
            <svg class="icon"><use xlink:href="#icon-markdown"></use></svg>
            <span class="sr-only">Aide markdown</span>
        </a>
    </div>
  `,
  methods: {
    preventDeselect: function () {
      document.querySelectorAll('#md-buttons .btn').forEach(btn => {
        btn.addEventListener('mousedown', e => {
          e = e || window.event
          e.preventDefault()
        })
      })
    },
    insert: function (btn) {
      const selectedText = this.getSelectedText().length > 0 ? this.getSelectedText() : 'écrivez ici'

      let field = document.querySelector('textarea')
      let value

      switch (btn) {
        case 'bold':
          value = `**${selectedText}**`
          break;
        case 'italic':
          value = `_${selectedText}_`
          break;
        case 'link':
          value = `[${selectedText}](https://)`
          break;
        case 'code':
          value = `\`${selectedText}\``
          break;
        case 'quote':
          value = `> ${selectedText}`
          break;
        case 'note':
          const container = document.querySelector('.blocks')
          const fieldWidth = parseInt(window.getComputedStyle(field).getPropertyValue('width'))
          const fieldX = this.getOffset(field).left
          const fieldY = this.getOffset(field).top
          const containerX = this.getOffset(container).left
          const containerY = this.getOffset(container).top
          const gridGap = 10
          const noteTranslateX = (fieldX - containerX) + (fieldWidth + gridGap)
          const noteTranslateY = (fieldY - containerY)
          const noteId = Date.now().toString()

          value = `[${this.noteIndex}]`
          const newBlock = {
            type: 'text',
            ref: noteId,
            id: this.block.id,
            left: noteTranslateX,
            top: noteTranslateY,
            text: `[${this.noteIndex}]`
          }
          this.store.addBlock(newBlock.type, newBlock.ref, newBlock.id, newBlock.left, newBlock.top, newBlock.text)
          this.addRef(newBlock.ref)
          break;
      }

      // Update block.content.text
      setTimeout(() => {
        this.block.content.text = document.querySelector('textarea').value
      }, 50)

      //IE support
      if (document.selection) {
        field.focus();
        sel = document.selection.createRange();
        sel.text = value;
      }
      //MOZILLA and others
      else if (field.selectionStart || field.selectionStart == '0') {
        let startPos = field.selectionStart;
        let endPos = field.selectionEnd;
        field.value = field.value.substring(0, startPos) +
          value +
          field.value.substring(endPos, field.value.length);
      } else {
        field.value += value;
      }
    },
    addRef: function (id) {
      this.block.content.refs += this.block.content.refs.length === 0 ? id : `, ${id}`
    },
    getSelectedText: function () {
      const textField = document.querySelector('textarea')
      const selectedText = textField.value.substring(textField.selectionStart, textField.selectionEnd)
      return selectedText
    },
    getOffset: function (el) {
      const rect = el.getBoundingClientRect();
      return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
      }
    }
  },
  mounted: function () {
    this.preventDeselect()
  }
}

export default BtnsMd
