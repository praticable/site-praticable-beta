import Store from "../../store.js"

const Register = {
  props: {
    'page': String,
    'page-colors-label': String
  },
  data: function() {
    return {
      store: Store
    }
  },
  template: `
    <button 
      class="| text-2 border" 
      @click="save"
      ref="register-btn"
    >enregistrer</button>
  `,
  methods: {
    save: function () {
      this.$refs['register-btn'].textContent = "enregistrement…"
      const uniframedBlocks = this.uniframeBlocks(this.store.state.blocks)
      setTimeout(() => {
        const blocks = {
          page: this.page,
          colors: this.pageColorsLabel,
          blocks: uniframedBlocks
        }
        if (this.$root.debug) {
          console.log(this.$options.el + ' save: ', blocks)
        }
        this.send(blocks)
      }, 500)
    },
    uniframeBlocks: function (blocks) {
      const uniframedBlocks = blocks.map(block => {
        if (block.type === 'text') {
          block.content.text = block.content.text.replaceAll('<', '::').replaceAll('>', '&&')
        }
        return block
      })
      return uniframedBlocks
    },
    send: function (blocks) {
      const init = {
        method: 'POST',
        body: JSON.stringify(blocks)
      }
      fetch(`./save.json`, init)
        .then(res => {
          return res.json()
        })
        .then(data => {
          if (this.$root.debug) {
            console.log(this.$options._componentTag + ' save.json return: ', data)
          }
          this.$refs['register-btn'].textContent = "enregistré"
          this.$refs['register-btn'].classList.add('active')
          setTimeout(() => {
            this.store.state.isUpToDate = true
          }, 1200)
        })
    },
    listenSaveShortcut: function() {
      if (this.$root.debug) {
          console.log(this.$options._componentTag + ' : ', )
      }
      document.addEventListener("keydown", e => {
          if ((window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)  && e.keyCode == 83) {
            e.preventDefault()
            this.save()
          }
        }, false)
    }
  },
  mounted: function() {
    this.listenSaveShortcut()
  }
}

export default Register