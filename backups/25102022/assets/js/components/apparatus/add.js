import Store from "../../store.js"

const Add = {
  data: function() {
    return {
      store: Store,
      isOpen: false,
      types: [
        {
          label: 'texte',
          type: 'text'
        },
        {
          label: 'image',
          type: 'image'
        }
      ]
    }
  },
  template: `
    <div class="| flex flex-end" @mouseleave="close">
      <ul v-if="isOpen">
        <li v-for="item in types">
          <button class="| border" @click="create(item.type)">{{ item.label }}</button>
        </li>
      </ul>
      <button id="add" class="| border lock-active" @click="toggle">
        <p>ajouter</p>
      </button>
    </div>
  `,
  methods: {
    toggle: function() {
      this.isOpen = !this.isOpen
    },
    close: function() {
      this.isOpen = false
    },
    create: function (type) {
      const top = document.querySelector('#search').offsetHeight + 45

      this.store.state.isUpToDate = false

      const scrollY = window.scrollY
      const halfViewHeight = window.innerHeight / 2 - (77 / 2)
      
      const scrollX = window.scrollX
      const halfViewWidth = (window.innerWidth / 2) - (375 / 2)
      
      const pos = {
        x: (scrollX + halfViewWidth),
        y: (scrollY + halfViewHeight)
      }
      this.store.addBlock(type, undefined, undefined, pos.x, pos.y, undefined)
      this.toggle()
    }
  }
}

export default Add