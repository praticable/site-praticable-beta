import Store from "../../../store.js"

const PrevNext = {
  data: function() {
    return {
      store: Store
    }
  },
  template: `
    <div id="slide-nav" class="apparatus__btn">
      <button id="slide-nav__prev" class="border prevent-apparatus-layer" @click="prev">←</button>
      <button id="slide-nav__next" class="border prevent-apparatus-layer" @click="next">→</button>
    </div>
  `,
  methods: {
    prev: function() {
      if (this.store.state.slideIndex === 0) {
        this.store.state.slideIndex = this.store.state.blocks.length - 1
      } else {
        this.store.state.slideIndex--
      }
    },
    next: function() {
      if (this.store.state.slideIndex === this.store.state.blocks.length - 1) {
        this.store.state.slideIndex = 0
      } else {
        this.store.state.slideIndex++
      }
    }
  }
}

export default PrevNext