import Store from "../../../store.js"
import toggleMixin from "../mixins/toggle-mixin.js"

const Present = {
  mixins: [toggleMixin],
  data: function() {
    return {
      store: Store,
      isOpen: false,
      layouts: [
        {
          label: 'librement',
          value: 'free'
        },
        {
          label: 'à la verticale',
          value: 'vertical'
        },
        {
          label: 'à l\'horizontale',
          value: 'horizontal'
        },
        {
          label: 'en plein écran',
          value: 'full'
        }
      ]
    } 
  },
  template: `
    <div 
      id="present"
    >
      <!-- <button class="| text-2 border lock-active" @click="toggle" ref="shortcut">P</button> -->
      <button class="| text-2 border lock-active" @click="toggle" ref="btn">présenter</button>
      <form id="present__options" action="">
          <div v-if="isOpen && store.state.apparatus.isActive" v-for="layout in layouts" 
            class="inputWrapper border" 
            :class="store.state.layout === layout.value ? 'active': false"
          >
            <label class="| text-2" :for="layout.value">{{ layout.label }}</label>
            <input 
              type="radio" 
              v-model="store.state.layout"
              name="present" 
              :id="layout.value" 
              :value="layout.value" 
            >
          </div>
      </form>
    </div>
  `,
  methods: {
    changeLayout: function(layout) {
      this.store.changeLayout(layout)
    },
    switchLayout: function(direction) {
      const activeValue = document.querySelector('.inputWrapper.active input').value
      this.layouts.forEach(layout => {
        layout.active = false
        if (layout.value === activeValue) {
          const layoutIndex = this.layouts.indexOf(layout)
          const indexBoundary = direction === 'next' ? this.layouts.length - 1 : 0
          if (layoutIndex === indexBoundary) {
            const resetValue = direction === 'next' ? this.layouts[0].value : this.layouts[this.layouts.length - 1].value
            this.store.state.layout = resetValue
          } else {
            const neighborValue = direction === 'next' ? this.layouts[layoutIndex + 1].value : this.layouts[layoutIndex - 1].value
            this.store.state.layout = neighborValue
          }
        }
      })
    },
    enableShortcut: function() {
      document.addEventListener('keyup', event => {
        if (this.store.state.currentEditBlock) return
        switch (event.key) {
          case 'p':
            this.toggle()
            break
          case 'ArrowDown':
            if (!this.isOpen) return            
            this.switchLayout('next')
            break
          case 'ArrowRight':
            if (!this.isOpen) return
            this.switchLayout('next')
            break
          case 'ArrowUp':
            if (!this.isOpen) return
            this.switchLayout('previous')
            break
          case 'ArrowLeft':
            if (!this.isOpen) return
            this.switchLayout('previous')
            break
          case 'Enter':
            if (!this.isOpen) return
            this.close()
            break
        }
      })
    }
  },
  mounted: function() {
    this.enableShortcut()
  }
}

export default Present