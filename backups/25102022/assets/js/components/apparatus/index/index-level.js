import Store from "../../../store.js"

const IndexLevel = {
  name: 'index-level',
  props: {
    'index-level': Array,
    'parent-url': String,
    'breadcrumb': Array,
    'page': String,
    'parent-name': String,
    'is-open': Boolean
  },
  data: function() {
    return {
      isNextLevelDisplayed: false,
      store: Store
    }
  },
  computed: {
    isCurrent: function() {
      return this.parentName === this.page
    },
    containsActiveCategory: function() {
      const containsActiveCategory = this.indexLevel.some(category => {
        return this.breadcrumb.includes(category.title.toLowerCase())
      })
      return containsActiveCategory
    }
  },
  template: `
    <ul 
      v-if="isOpen"
      class="index__level" 
      :class="{
        hidden: !containsActiveCategory,
        'lock-visible': containsActiveCategory && store.state.apparatus.isActive && isOpen
      }"
      @focus="showNextBtn"
    >
      <button 
        v-if="parentUrl.length > 0"
        class="index__category | text-2 border"
        :class="{'lock-active': isCurrent}"
      >
        <a :href="parentUrl">tout</a>
      </button>
     
      <li
        v-for="(category, childIndex) in indexLevel"
      >
        <ul 
          class="thread | flex" 
          @mouseenter="showNextBtn" 
          @mouseleave="hideNextBtn" 
          tabindex="-1"
        >
          <li tabindex="-1">
            <button 
              class="index__category | text-2 border"
              :class="isInBreadcrumb(category.title)"
              tabindex="-1"
            >
              <a :href="category.url" @focus="showNextBtn" @focusout="hideNextBtn">{{ category.title }}</a>
            </button>
          </li>
          
          <li>
            <button 
              v-if="category.children"
              class="index__arrow | text-2 border"
              tabindex="-1"
            >→</button>
          </li>
          <index-level
            v-if="category.children"
            :index-level="category.children"
            :parent-url="category.url"
            :parent-name="category.title"
            :breadcrumb="breadcrumb"
            :page="page"
            :is-open="isOpen"
          ></index-level>
        </ul>
      </li>
    </ul>
  `,
  methods: {
    isInBreadcrumb: function(category) {
      if (category.toLowerCase() === 'accueil') return false
      if (this.breadcrumb.includes(category.toLowerCase())) {
        return 'lock-active'
      }
    },
    showNextBtn: function(e) {
      const target = e.target.tagName === 'A' ? e.target.parentNode.parentNode.parentNode.querySelector('.index__level') : e.target.querySelector('.index__level')
      if (!target) return
      target.classList.remove('hidden')
    },
    hideNextBtn: function(e) {
      const target = e.target.tagName === 'A' ? e.target.parentNode.parentNode.parentNode.querySelector('.index__level') : e.target.querySelector('.index__level')
      if (!target) return
      target.classList.add('hidden')
    }
  }
}

export default IndexLevel