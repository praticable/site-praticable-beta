import Store from "../../../store.js"
import toggleMixin from "../mixins/toggle-mixin.js"
import IndexLevel from "./index-level.js"

const Index = {
  props: {
    'index': Array,
    'breadcrumb': Array,
    'page': String
  },
  mixins: [toggleMixin],
  data: function() {
    return {
      isOpen: false,
      store: Store
    }
  },
  components: {
    'index-level': IndexLevel
  },
  template: `
    <nav 
      id="index" 
      aria-label="Navigation principale" 
      role="Recherche" 
    
      @focusin="open"
    >
      <ul class="thread | flex">
        <li>
          <button 
            id="search"
            class="| text-2 border lock-active"
            @click="showNextBtn"
          >chercher</button>
        </li>

        <li>
          <button 
            class="index__arrow | text-2 border lock-active"
            @mouseenter="showNextBtn"
            @focus="showNextBtn"
            @click="showNextBtn"
            tabindex="0"
          >→</button>
        </li>
          
        <li>
          <index-level
            class="hidden"
            :index-level="index"
            :parent-url="''"
            :breadcrumb="breadcrumb"
            :page="page"
            :is-open="isOpen"
          ></index-level>
        </li>
      </ul>
    </nav>
  `
}

export default Index