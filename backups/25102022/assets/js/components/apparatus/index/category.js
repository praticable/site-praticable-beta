import IndexLevel from "./index-level.js";

const Category = {
  name: 'category',
  props: {
    'category': Object
  },
  components: {
    'index-level': IndexLevel
  },
  template: `
    <ul class="index__category">
      <li>
        <button 
          class="| text-2 border"
          @mouseenter="toggleArrow"
          @mouseleave="toggleArrow"
        >
          {{ category.title }}
        </button>
      </li>

      <li>
        <button 
          class="| text-2 border hidden"
          @mouseenter="showNextLevel"
        >→</button>
      </li>
      <index-level v-if="isNextLevelDisplayed"
        @mouseenter="showNextLevel"
        :index-level="category.children"
      ></index-level>
    </ul>
  `,
  methods: {
    showNextLevel: function() {
      this.isNextLevel = true
    },
    hideNextLevel: function() {
      setTimeout(() => {
        this.isNextLevel = false
      }, 1000);
    }
  }
}

export default Category