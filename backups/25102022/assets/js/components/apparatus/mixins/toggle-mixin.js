const toggleMixin = {
  watch: {
    store: {
      handler: function(newVal, oldVal) {
        if (!newVal.state.apparatus.isActive && this.isOpen) {
          this.close()
        }
      },
      deep: true
    }
  },
  methods: {
    open: function() {
      this.isOpen = true
      this.store.state.apparatus.isActive = true
    },
    close: function() {
      console.log('close')
      this.isOpen = false
      this.store.state.apparatus.isActive = false
    },
    toggle: function() {
      this.isOpen = true
      this.store.state.apparatus.isActive = !this.store.state.apparatus.isActive
    },
    showNextBtn: function(e) {
      const target = e.target
      const isArrow = target.classList.contains('tag__arrow') ? true : false

      this.activeBtn(target)

      if (isArrow) {
        if (!target.parentNode.parentNode.querySelector('.tag__level')) return
        target.parentNode.parentNode.querySelector('.tag__level').classList.remove('hidden')
      } else {
        if (!target.parentNode.parentNode.querySelector('.tag__level')) return
        target.parentNode.parentNode.querySelector('.tag__arrow').classList.remove('hidden')
      }
    },
    activeBtn: function(target) {
      target.classList.add('active')
    }
  }
}

export default toggleMixin