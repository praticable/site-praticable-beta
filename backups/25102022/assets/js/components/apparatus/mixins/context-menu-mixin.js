const contextMenuMixin = {
  methods: {
    toggleContext: function(event) {
      console.log(event.button)
      if (event.ctrlKey || event.metaKey) {
        event.preventDefault()
        event.stopPropagation()
        const { clientX: mouseX, clientY: mouseY } = event
        const contextMenu = document.querySelector('#context-menu')
        contextMenu.style.left = `${mouseX}px`
        contextMenu.style.top = `${mouseY}px`
        contextMenu.classList.toggle('hidden')

        if (event.target.id === 'app' || event.target.classList.contains('blocks')) {
          this.store.state.contextMenu.target = {
            type: 'apparatus'
          }
        } else if (this.block) {
          this.store.state.contextMenu.target = this.block
        }
      }
    }
  }
}

export default contextMenuMixin