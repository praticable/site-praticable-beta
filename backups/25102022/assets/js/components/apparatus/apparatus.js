import Add from "./add.js"
import Present from "./present/present.js"
import Index from "./index/index.js"
import PrevNext from "./present/prev-next.js"
import Store from "../../store.js"
import Register from "./register.js"
import Tune from "./tune/tune.js"

const apparatus = {
  props: {
    'index': Array,
    'tags': Array,
    'page': String,
    'page-colors-label': String,
    'breadcrumb': Array
  },
  components: {
    'index': Index,
    'tune': Tune,
    'register': Register,
    'add': Add,
    'present': Present,
    'prev-next': PrevNext,
  },
  data: function() {
    return {
      store: Store
    }
  },
  template: `
    <div id="apparatus" ref="apparatus" :class="{ active: store.state.apparatus.isActive }">
      <ul id="context-menu" class="| hidden">
        <li v-for="option in store.state.contextMenu.options">
          <button 
            v-if="
            option.concernedBlocks.includes(store.state.contextMenu.target.type) 
            && option.condition(store.state.contextMenu.target)
            " 
            class="context-menu__button| border" 
            @click="store.handleContextMenuFunction(option.function)">{{ option.label }}</button>
        </li>
      </ul>
      <div id="apparatus__top">
        <index
          class="apparatus__btn"
          
          :index="index"
          :page="page"
          :breadcrumb="breadcrumb"
        ></index>
        <present
          class="apparatus__btn"
        ></present>
      </div>

      <div id="apparatus__middle">
        <prev-next v-if="store.state.layout === 'full'"></prev-next>
      </div>
     
      <div id="apparatus__bottom" class="| flex-end">
        <tune
          class="apparatus__btn"
          :tags="tags"
        ></tune>
        <register v-if="!store.state.isUpToDate"
          class="apparatus__btn"
          :page="page"
          :page-colors-label="pageColorsLabel"
        ></register>
        <add
          class="apparatus__btn"
        ></add>
      </div>
    </div>
  `,
  methods: {
    off: function(e) {
      if (this.$root.debug) {
          console.log(this.$options._componentTag + ' off: ', )
      }
      this.store.state.apparatus.isActive = false
      if (!e.crtlKey && !e.metaKey) {
        this.store.clearSelection()
      }
    },
    enableMultiDeleteBlocks: function(e) {
      document.addEventListener('keyup', (e) => {
        if (this.store.state.selectedBlocks.length > 0 && e.key === 'Backspace') {
          this.store.deleteSelectedBlocks()
        }
      })
    }
  },
  mounted: function() {
    document.querySelector('#app').addEventListener('click', e => {
      if (e.target.tagName !== 'BUTTON' && e.target.tagName !== 'A') {
        this.off(e)
      }
    })
    this.enableMultiDeleteBlocks()
  }
}

export default apparatus