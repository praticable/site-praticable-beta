import TagLevel from "./tag-level.js";

const Tag = {
  name: 'category',
  props: {
    'tag': Object
  },
  components: {
    'tag-level': TagLevel
  },
  template: `
    <ul class="tags__tag">
      <li>
        <button 
          class="| text-2 border"
          @mouseenter="toggleArrow"
          @mouseleave="toggleArrow"
        >
          {{ tag.title }}
        </button>
      </li>

      <li>
        <button 
          class="| text-2 border hidden"
          @mouseenter="showNextLevel"
        >→</button>
      </li>
      <tag-level v-if="isNextLevelDisplayed"
        @mouseenter="showNextLevel"
        :tag-level="tag.children"
      ></tag-level>
    </ul>
  `,
  methods: {
    showNextLevel: function() {
      this.isNextLevel = true
    },
    hideNextLevel: function() {
      setTimeout(() => {
        this.isNextLevel = false
      }, 1000);
    }
  }
}

export default Tag