import toggleMixin from "../mixins/toggle-mixin.js"
import Tags from "./tags.js"

const Tune = {
  props: {
    'tags': Array
  },
  components: {
    'tags': Tags
  },
  mixins: [toggleMixin],
  data: function() {
    return {
      isOpen: false
    }
  },
  template: `
    <div id="tune">
      <ul class="thread | flex">
        <li>
          <button 
            id="tune" 
            class="border lock-active"
          >
            régler
          </button>
        </li>
       
        <li>
          <button 
            class="index__arrow | text-2 border lock-active"
            @mouseenter="showNextBtn"
            @focus="showNextBtn"
            @click="showNextBtn"
            tabindex="0"
          >→</button>
        </li>

        <li>
          <tags
            class="hidden"
            :tags="tags"
            :is-open="isOpen"
          ></tags>
        </li>
      </ul>
    </div>
  `,
  methods: {
    toggle: function() {
      this.isOpen = !this.isOpen
    }
  }
}

export default Tune