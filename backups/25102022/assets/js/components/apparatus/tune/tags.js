import TagLevel from "./tag-level.js"

const Tags = {
  props: {
    'tags': Array
  },
  components: {
    'tag-level': TagLevel
  },
  template: `
    <div id="tags">
      <ul class="thread | flex">
        <li>
          <tag-level
            class="hidden"
            :tag-level="tags"
          ></tag-level>
        </li>
      </ul>
    </div>
  `
}

export default Tags