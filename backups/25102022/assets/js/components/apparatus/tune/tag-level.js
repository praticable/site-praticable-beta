import Store from "../../../store.js"

const TagLevel = {
  name: 'tag-level',
  props: {
    'tag-level': Array
  },
  data: function() {
    return {
      isNextLevelDisplayed: false,
      store: Store
    }
  },
  template: `
    <ul class="tag__level hidden">
      <li
        v-for="(tag, childIndex) in tagLevel"
      >
        <ul class="thread | flex">
          <li>
            <button 
              class="tag__category | text-2 border"
              :class="{ active: isActive(tag.title) }"
              @click="filter(tag.title)"
              @mouseenter="showNextBtn"
            >
              {{ tag.title }}
            </button>
          </li>
          
          <li>
            <button 
              v-if="tag.children"
              class="tag__arrow | text-2 border hidden"
              @mouseenter="showNextBtn"
            >→</button>
          </li>
          <tag-level
            v-if="tag.children"
            class="hidden"
            :tag-level="tag.children"
          ></tag-level>
        </ul>
      </li>
    </ul>
  `,
  methods: {
    isActive: function(tag) {
      return this.store.state.filters.includes(tag)
    },
    showNextBtn: function(e) {
      const target = e.target
      const isArrow = target.classList.contains('tag__arrow') ? true : false

      this.activeBtn(target)

      if (isArrow) {
        if (!target.parentNode.parentNode.parentNode.querySelector('.tag__level')) return
        target.parentNode.parentNode.parentNode.querySelector('.tag__level').classList.remove('hidden')
      } else {
        this.disableSiblingBtn(target)
        this.activeBtn(target)
        if (!target.parentNode.parentNode.querySelector('.tag__level')) return
        target.parentNode.parentNode.querySelector('.tag__arrow').classList.remove('hidden')
      }
    },
    activeBtn: function(target) {
      target.closest('ul').classList.add('active')
      target.classList.add('active')
    },
    disableSiblingBtn: function(target) {
      target.closest('li ul').parentNode.parentNode.querySelectorAll('button').forEach(btn => {
        if (btn !== target) {
          btn.classList.remove('active')
          if (btn.parentNode.parentNode.querySelector('.tag__arrow')) {
            btn.parentNode.parentNode.querySelector('.tag__arrow').classList.add('hidden')
            btn.parentNode.parentNode.querySelector('.tag__level').classList.add('hidden')
          }
        }
      });
    },
    filter: function(tag) {
      if (this.store.state.filters.includes(tag)) {
        this.store.state.filters = this.store.state.filters.filter(filter => filter !== tag)
      } else {
        this.store.state.filters.push(tag)
      }
    }
  }
}

export default TagLevel