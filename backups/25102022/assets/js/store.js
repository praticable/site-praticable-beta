const Store = {
  state: {
    blocks: [],
    selectedBlocks: [],
    layout: 'free',
    filters: [],
    slideIndex: 0,
    currentEditBlock: false,
    isUpToDate: true,
    saving: false,
    apparatus: {
      isActive: false
    },
    contextMenu: {
      target: {},
      options: [
        {
          label: 'supprimer',
          function: 'removeBlockById',
          concernedBlocks: ['text', 'image'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'modifier',
          function: 'editBlockById',
          concernedBlocks: ['text'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'placer devant',
          function: 'upper',
          concernedBlocks: ['image', 'text', 'representative'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'placer au premier plan',
          function: 'maxUpper',
          concernedBlocks: ['image', 'text', 'representative'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'placer derrière',
          function: 'lower',
          concernedBlocks: ['image', 'text', 'representative'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'placer au dernier plan',
          function: 'maxLower',
          concernedBlocks: ['image', 'text', 'representative'],
          condition: function(target) {
            return true
          }
        },
        {
          label: 'définir comme couverture',
          function: 'setAsCoverById',
          concernedBlocks: ['image'],
          condition: function(target) {
            return target.content.iscover == 'false' || target.content.iscover == false
          }
        },
        {
          label: 'révoquer comme couverture',
          function: 'unsetAsCoverById',
          concernedBlocks: ['image'],
          condition: function(target) {
            return target.content.iscover == 'true' || target.content.iscover == true
          }
        },
        {
          label: 'définir comme introduction',
          function: 'setAsIntroById',
          concernedBlocks: ['text'],
          condition: function(target) {
            return target.content.isintro == 'false' || target.content.isintro == false
          }
        },
        {
          label: 'révoquer comme introduction',
          function: 'unsetAsIntroById',
          concernedBlocks: ['text'],
          condition: function(target) {
            return target.content.isintro == 'true' || target.content.isintro == true
          }
        },
        {
          label: 'supprimer tous les blocs',
          function: 'removeAllBlocks',
          concernedBlocks: ['apparatus'],
          condition: function(target) {
            return true
          }
        }
      ]
    }
  },
  handleContextMenuFunction: function(name) {
    this[name](this.state.contextMenu.target.id)
  },
  setAsCoverById: function(id) {
    let oldCover = this.state.blocks.find(block => block.content.iscover == 'true' || block.content.iscover == true)
    if (oldCover) {
      oldCover.content.iscover = false
      this.removeBlockById(oldCover.id)
      this.state.blocks.push(oldCover)
    }

    let newCover = this.state.blocks.find(block => block.id === id)
    newCover.content.iscover = true

    this.removeBlockById(id)
    this.state.blocks.push(newCover)
    
    this.state.isUpToDate = false
  },
  unsetAsCoverById: function(id) {
    let target = this.state.blocks.find(block => block.content.iscover == 'true' || block.content.iscover == true)
    target.content.iscover = false

    this.removeBlockById(target.id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  setAsIntroById: function(id) {
    let oldIntro = this.state.blocks.find(block => block.content.isintro == 'true' || block.content.isintro == true)
    if (oldIntro) {
      oldIntro.content.isintro = false
      this.removeBlockById(oldIntro.id)
      this.state.blocks.push(oldIntro)
    }
    
    let newIntro = this.state.blocks.find(block => block.id === id)
    newIntro.content.isintro = true
    
    this.removeBlockById(id)
    
    this.state.blocks.push(newIntro)

    
    this.state.isUpToDate = false
  },
  unsetAsIntroById: function(id) {
    let target = this.state.blocks.find(block => block.content.isintro == 'true' || block.content.isintro == true)
    target.content.isintro = false

    this.removeBlockById(target.id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  editBlockById: function(id) {
    this.state.blocks = this.state.blocks.map(block => {
      if (block.id === id) {
        block.content.isedit = true
        this.state.currentEditBlock = block
      }
      return block
    })
    setTimeout(() => {
      if (document.querySelector('textarea')) {document.querySelector('textarea').focus()}
    }, 10)
  },
  toggleSelectBlockById: function(id) { // toggle selection of a block by id (select or unselect)
    let target = this.state.blocks.find(block => block.id === id)
    if (target) {
      if (this.state.selectedBlocks.find(block => block.id === id)) {
        this.state.selectedBlocks = this.state.selectedBlocks.filter(block => block.id !== id)
      } else {
        this.state.selectedBlocks.push(target)
      }
    }
  },
  clearSelection: function() {
    const selectedBlocks = this.state.blocks.filter(block => this.state.selectedBlocks.includes(block))
    const selectionPosition = [...document.querySelector('#selection').style.transform.matchAll(/[0-9]+/g)]
    const selectionX = parseInt(selectionPosition[0])
    const selectionY = parseInt(selectionPosition[1])
    
    selectedBlocks.forEach(selectedBlock => {
      const originalPosition = [...selectedBlock.content.transform.matchAll(/[0-9]+/g)]
      const originalX = parseInt(originalPosition[0])
      const originalY = parseInt(originalPosition[1])
      const newX = originalX + selectionX
      const newY = originalY + selectionY
      selectedBlock.content.transform = `translate(${newX}px, ${newY}px)`
    })
    document.querySelectorAll('.block--selected').forEach(selectedBlock => {
      document.querySelector('#selection').removeChild(selectedBlock)
      document.querySelector('.blocks').appendChild(selectedBlock)
    })
    this.state.selectedBlocks = []
    document.querySelector('#selection').style.transform = ''
  },
  deleteSelectedBlocks: function() { // delete all selected blocks
    this.state.selectedBlocks.forEach(block => {
      this.removeBlockById(block.id)
    })
    this.state.selectedBlocks = []
  },
  upper: function(id) {
    let target = this.state.blocks.find(block => block.id === id)
    target.content.zindex++

    this.removeBlockById(id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  maxUpper: function(id) {
    let highestBlock = undefined
    this.state.blocks.forEach(block => {
      if (highestBlock === undefined) {
        highestBlock = block
      } else {
        if (block.content.zindex > highestBlock.content.zindex) {
          highestBlock = block
        }
      }
    })

    let target = this.state.blocks.find(block => block.id === id)
    target.content.zindex = (highestBlock.content.zindex + 1)

    this.removeBlockById(id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  lower: function(id) {
    let target = this.state.blocks.find(block => block.id === id)
    if (target.content.zindex > 0) {
      target.content.zindex--
    }

    this.removeBlockById(id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  maxLower: function(id) {
    let lowestBlock = undefined
    this.state.blocks.forEach(block => {
      if (lowestBlock === undefined) {
        lowestBlock = block
      } else {
        if (block.content.zindex < lowestBlock.content.zindex) {
          lowestBlock = block
        }
      }
    })

    let target = this.state.blocks.find(block => block.id === id)
    if (lowestBlock.zindex > 0) {
      target.content.zindex = (lowestBlock.content.zindex - 1)
    } else {
      target.content.zindex = 0
    }

    this.removeBlockById(id)
    this.state.blocks.push(target)
    
    this.state.isUpToDate = false
  },
  changeLayout: function(layout) {
    this.state.layout = layout
  },
  setBlocks: function(blocks) {
    blocks.forEach(block => {
      block.isHighlight = false
      block.isVisible = true
    })
    
    this.state.blocks = blocks
  },
  addToGroup: function(blockId, groupId) {
    const blockToAdd = this.getBlockById(blockId)
    this.removeBlockById(blockId)
    this.state.blocks.forEach(block => {
      if (block.id === groupId) {
        block.content.blocks.push(blockToAdd)
      }
    })

  },
  getBlockByTitle: function(title) {
    return this.state.blocks.find(block => block.content.title === title)
  },
  getBlockById: function(id) {
    return this.state.blocks.find(block => block.id === id)
  },
  highlightBlockByTitle: function(title) {
    this.state.blocks.forEach(block => {
      if (block.content.title === title) {
        block.isHighlight = true
      }
    })
  },
  unhighlightBlockByTitle: function(title) {
    this.state.blocks.forEach(block => {
      if (block.content.title === title) {
        block.isHighlight = false
      }
    })
  },
  highlightBlockById: function(id) {
    this.state.blocks.forEach(block => {
      if (block.id == id) {
        block.isHighlight = true
      }
    })
  },
  unhighlightBlockById: function(id) {
    this.state.blocks.forEach(block => {
      if (block.id ==  id) {
        block.isHighlight = false
      }
    })
  },
  removeAllBlocks: function() {
    this.state.blocks = {}
    this.state.isUpToDate = false
  },
  removeBlockById: function(id) {
    this.state.blocks = this.state.blocks.filter(block => block.id != id)
    this.state.isUpToDate = false
  },
  removeNewBlock: function() {
    this.state.blocks = this.state.blocks.filter(block => !block.isNew)
  },
  removeCover: function() {
    this.state.blocks.forEach(block => {
      if (block.type === 'image') {
        block.content.iscover = 'false'
      }
    })
  },
  addBlock: function(type, id = Date.now(), refs = '', left = '0', top = '0', text = '') {
    this.state.isUpToDate = false
    let newBlock = {
      type: type,
      isNew: true,
      isEdited: false,
      isHighlight: false,
      id: id,
      content: {
        width: '375px',
        height: '',
        transform: `translate(${left}px, ${top}px)`,
        zindex: 0,
        color: 'white',
        backgroundcolor: "#FFF",
        textcolor: "#000",
        refs: refs,
        isedit: 'true'
      }
    }

    if (type === 'text') {
      newBlock.content.text = text
      setTimeout(() => {
        document.querySelector('textarea').focus()
      }, 10);
    }

    this.state.blocks.push(newBlock)
  }
}

export default Store