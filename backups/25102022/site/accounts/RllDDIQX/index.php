<?php

return [
    'email' => 'thomas@praticable.fr',
    'language' => 'fr',
    'name' => 'Thomas',
    'role' => 'admin'
];