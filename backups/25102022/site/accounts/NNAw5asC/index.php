<?php

return [
    'email' => 'alix@praticable.fr',
    'language' => 'fr',
    'name' => 'Alix',
    'role' => 'admin'
];