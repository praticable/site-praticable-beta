<?php

return [
    'email' => 'anthony@praticable.fr',
    'language' => 'fr',
    'name' => 'Anthony',
    'role' => 'admin'
];