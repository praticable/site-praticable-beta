<?php

return [
    'email' => 'laurene@praticable.fr',
    'language' => 'fr',
    'name' => 'Laurène',
    'role' => 'admin'
];