<?php

return [
    'email' => 'lea@praticable.fr',
    'language' => 'fr',
    'name' => 'Léa',
    'role' => 'admin'
];