<?php

return [
    'email' => 'morgane@praticable.fr',
    'language' => 'fr',
    'name' => 'Morgane',
    'role' => 'admin'
];