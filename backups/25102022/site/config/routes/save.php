<?php

return [
  'pattern' => ['(:all)save.json'],
  'method' => 'POST',
  'action' => function() {
      // GET BLOCK DATA FROM BODY REQUEST
      $json = file_get_contents("php://input");
      $data = json_decode($json);

      // FIND PAGE
      $site = site();
      $page = $site->index()->findBy('title', $data->page) ? $site->index()->findBy('title', $data->page) : $site->index()->drafts()->findBy('title', $data->page);

      // ACTIVE SUPERUSER FOR DATA MANIPULATIONS
      $kirby = kirby();
      $kirby->impersonate('kirby');

      // CLEAR COMPOSITION IF NO BLOCKS
      if (count($data->blocks) === 0) {
          $page->update([
              'composition' => json_encode([]),
              'colors' => $data->colors
          ]);
          return json_encode("Composition cleared");
      } else {
        $previousBlocks = $page->composition()->toBlocks();
          $newBlocks = [];

          $coverThumb = '';
          $coverThumbTransform = $page->coverThumbTransform();
          $pageThumbTransform = $page->pageThumbTransform();

          foreach ($data->blocks as $incomingBlock) {
              
            // PROCESS PAGE GENERATED BLOCKS
            if ($incomingBlock->type === 'subpage') {
              $correspondingPage = $page->children()->findBy('title', $incomingBlock->content->title);
              $correspondingPage->update([
                'pageThumbTransform' => $incomingBlock->content->transform,
                'pageThumbZindex' => $incomingBlock->content->zindex,
                'pageThumbWidth' => $incomingBlock->content->width
              ]);
            } else if ($incomingBlock->type === 'subpage-cover') {                        
              $correspondingPage = $page->children()->findBy('title', $incomingBlock->content->ref);
              
              $correspondingPage->update([
                'coverThumbTransform' => $incomingBlock->content->transform,
                'coverThumbZindex' => $incomingBlock->content->zindex,
                'coverThumbWidth' => $incomingBlock->content->width
              ]);
            } 
            // PROCESS NEW BLOCK
            else if (property_exists($incomingBlock, 'isNew') && $incomingBlock->isNew == 'true') {
                //================================================================ FILE
                if ($incomingBlock->type === 'image') {
                    $fileName = $incomingBlock->content->image[0]->filename;
                    // CREATE TEMP FILE IN ASSETS FOLDER
                    $path = './assets/files/' . $fileName;
                    $file = fopen($path, "wb");
                    fwrite($file, base64_decode($incomingBlock->b64));
                    fclose($file);

                    if ($page->files()->has(strtolower($data->page). '/' . $fileName)) {
                        unlink($path);
                        return json_encode('Files already exists');
                        die();
                    }

                    // CREATE FILE IN PAGE FOLDER
                    $kirbyFile = null;
                    $kirbyFile = $page->createFile([
                        'source' => $path,
                        'filename' => $fileName
                    ]);

                    // CREATE BLOCK 
                    $preparedBlock = new Kirby\Cms\Block([
                        'content' => [
                            'location' => 'kirby',
                            'image' => $kirbyFile->fileName(),
                            'width' => $incomingBlock->content->width,
                            'height' => $incomingBlock->content->height,
                            'transform' => $incomingBlock->content->transform,
                            'zindex' => (int)$incomingBlock->content->zindex,
                            'color' => $incomingBlock->content->color,
                            'isCover' => $incomingBlock->content->iscover
                        ],
                        'type' => 'image'
                    ]);
                    if ($incomingBlock->content->iscover == 'true') {                                
                      $coverThumb = (string)$kirbyFile->mediaUrl();
                    }

                    $newBlocks[] = $preparedBlock->toArray();
                    unlink($path);
                }
                //================================================================ TEXT
                elseif ($incomingBlock->type === 'text') {
                    // CREATE BLOCK 
                    $preparedBlock = new Kirby\Cms\Block([
                        'content' => [
                            'text' => $incomingBlock->content->text,
                            'width' => $incomingBlock->content->width,
                            'height' => $incomingBlock->content->height,
                            'transform' => $incomingBlock->content->transform,
                            'zindex' => (int)$incomingBlock->content->zindex,
                            'backgroundcolor' => $incomingBlock->content->backgroundcolor,
                            'textcolor' => $incomingBlock->content->textcolor,
                            'refs' => $incomingBlock->content->refs
                        ],
                        'id' => $incomingBlock->id,
                        'type' => 'text'
                    ]);
                    
                    $newBlocks[] = $preparedBlock->toArray();
                }

            } 
            // PROCESS EDITED BLOCK
            else {
                foreach ($previousBlocks as $previousBlock) {
                    // UPDATE EXISTING BLOCKS
                    if ($previousBlock->id() === $incomingBlock->id) {
                      $newBlocks[] = $incomingBlock;
                      if ($incomingBlock->type === 'image' && $incomingBlock->content->iscover == 'true') {
                        $coverThumb = (string)$previousBlock->image()->toFile()->mediaUrl();
                      }
                    }
                }
            }
          }

          $flatNewBlocks = [];
          $flatNewBlocks = array_values($newBlocks);

          $pageThumbTransform = $page->pageThumbTransform();
          $newPage = $page->update([
              'composition' => json_encode($flatNewBlocks),
              'coverThumb' => $coverThumb,
              'coverThumbTransform' => $coverThumbTransform,
              'pageThumbTransform' => $pageThumbTransform,
              'colors' => $data->colors
          ]);

          return json_encode([
              'new' => isset($preparedBlock) ? $preparedBlock->toArray(): false,
              'blocks' => $newPage->composition()->toBlocks()->toArray()
          ]);
      }
  }
];