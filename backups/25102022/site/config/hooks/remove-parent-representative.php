<?php

$parent = $page->parent();

$representative = $parent->composition()->toBlocks()->find($page->id());
if ($representative != null && $page->title() != "json") {
  $newParentComposition = $parent->composition()->toBlocks()->remove($representative->id());

  $parent->update([
    'composition' => json_encode($newParentComposition->toArray())
  ]);
}