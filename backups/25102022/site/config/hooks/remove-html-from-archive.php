<?php

$kirby = kirby();
$page = $file->page();
$target = $kirby->root('assets') . '/static-data/' . $page->slug() . '/' . $file->name();

if (is_dir($target)) {
  deleteAll($target);
}