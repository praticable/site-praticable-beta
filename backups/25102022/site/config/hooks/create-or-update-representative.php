<?php

if ($newPage->isPublished() != 'true') return;

$parent = $newPage->parent();

//================================================================ CREATE REPRESENTATIVE IN PARENT
if ($parent !== null) {
  
  $linkedSpaces = [];
  foreach ($newPage->linkedSpaces()->toPages() as $linkedSpace) {
    $linkedSpaces[] = $linkedSpace->uri();
  }
  foreach ($newPage->children() as $child) {
    $linkedSpaces[] = $child->uri();
  }
  
  $representative = $parent->composition()->toBlocks()->find($newPage->id());
  $preparedBlock = new Kirby\Cms\Block([
    'content' => [
        'title' => $newPage->title()->value(),
        'cover' => $newPage->composition()->toBlocks()->findBy('iscover', 'true') == null ? false : $newPage->composition()->toBlocks()->findBy('iscover', 'true')->image()->toFile()->url(),
        'intro' => $newPage->composition()->toBlocks()->findBy('isintro', 'true') == null ? false : $newPage->composition()->toBlocks()->findBy('isintro', 'true')->text()->value(),
        'width' => $representative !== null ? $representative->width()->value() : '400px',
        'height' => $representative !== null ? $representative->height()->value(): '',
        'transform' => $representative !== null ? $representative->transform()->value() : '',
        'zindex' => $representative !== null ? $representative->zindex()->value() : 0,
        'index' => $newPage->index()->value(),
        'url' => $newPage->url(), 
        'tags' => $newPage->tags()->split(),
        'linkedSpaces' => $linkedSpaces
    ],
    'id' => $newPage->id(),
    'type' => 'representative'
  ]);

  $newParentComposition = $parent->composition()->toBlocks();
  if ($representative != null) { 
    $newParentComposition = $newParentComposition->remove($preparedBlock->id());
    $newParentComposition = $newParentComposition->add($preparedBlock);    
  } else {
    $newParentComposition = $newParentComposition->add($preparedBlock);    
  }

  $parent->update([
    'composition' => json_encode($newParentComposition->toArray())
  ]);
}

//================================================================ CREATE REPRESENTATIVES FOR CHILD AND LINKED PAGES
// if ($newPage->linkedSpaces()->isNotEmpty()) {
//   $linkedSpaces = $newPage->linkedSpaces();

//   $newPageComposition = $newPage->composition()->toBlocks();
  
//   foreach ($linkedSpaces->toPages() as $linkedSpace) {    
//     $preparedBlock = new Kirby\Cms\Block([
//       'content' => [
//           'title' => $linkedSpace->title()->value(),
//           'cover' => $linkedSpace->composition()->toBlocks()->findBy('iscover', 'true') == null ? false : $linkedPage->composition()->toBlocks()->findBy('iscover', 'true')->image()->toFile()->url(),
//           'intro' => $linkedSpace->composition()->toBlocks()->findBy('isintro', 'true') == null ? false : $linkedPage->composition()->toBlocks()->findBy('isintro', 'true')->text()->value(),
//           'width' => $representative !== null ? $representative->width()->value() : '400px',
//           'height' => $representative !== null ? $representative->height()->value(): '',
//           'transform' => $representative !== null ? $representative->transform()->value() : '',
//           'zindex' => $representative !== null ? $representative->zindex()->value() : 0,
//           'index' => $linkedSpace->index()->value(),
//           'url' => $linkedSpace->url(),
//           'tags' => $linkedSpace->tags()->split()
//       ],
//       'id' => $linkedSpace->id(),
//       'type' => 'representative'
//     ]);

//     $representative = $newPage->composition()->toBlocks()->find($linkedSpace->id());
//     if ($representative != null) { 
//       $newPageComposition = $newPageComposition->remove($preparedBlock->id());
//       $newPageComposition = $newPageComposition->add($preparedBlock);    
//     } else {
//       $newPageComposition = $newPageComposition->add($preparedBlock);    
//     }
//   }

//   $newPage->update([
//     'composition' => json_encode($newPageComposition->toArray())
//   ]);
// }