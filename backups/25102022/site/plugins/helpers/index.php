<?php
function getItemByKeyValue($key, $value, $blocks) {
  foreach ($blocks as $block) {
    $isKeyExists = array_key_exists($key, $block['content']);
    
    if ($isKeyExists && $block['content'][$key] === $value) {
      return $block;
      break;
    }
  }
}

function getItemIndexByKeyValue($key, $value, $blocks) {
  foreach ($blocks as $index => $block) {
    $isKeyExists = array_key_exists('title', $block['content']);
    $isTarget = ((int)$isKeyExists === 1) && ($block['content'][$key] == $value);
    if ($isTarget) {      
      return $index;
      break;
    }
  }
}

function addHTMLTagsToNotionStrPart($strPart) {
  $string = $strPart->plain_text;
  
  if ($strPart->annotations->underline) {
    $string = '<u>' . $strPart->plain_text . '<\/u>';
  }
  if ($strPart->annotations->strikethrough) {
    $string = '<s>' . $strPart->plain_text . '<\/s>';
  }
  if ($strPart->annotations->italic) {
    $string = '<em>' . $strPart->plain_text . '<\/em>';
  }
  if ($strPart->annotations->bold) {
    $string = '<strong>' . $strPart->plain_text . '<\/strong>';
  }
  if ($strPart->annotations->code) {
    $string = '<code>' . $strPart->plain_text . '<\/code>';
  }
  
  return $string;
}

function cleanHTML($html) {
  $html_tag = '/<html>|<\/html>/';
  $head_tag = '/<head>(.+?)<\/head>/ms';
  $body_tag = '/<body>|<\/body>/';

  $html = preg_replace($html_tag, '', $html);
  $html = preg_replace($head_tag, '', $html);
  $html = preg_replace($body_tag, '', $html);

  return $html;
}

function deleteAll($dir) {
  foreach(glob($dir . '/*') as $file) {
    if(is_dir($file))
      deleteAll($file);
    else
      unlink($file);
  }
  rmdir($dir);
}

function getNotionBlocks($curl, $url, $headers) {
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => $headers,
  ));
  $json = curl_exec($curl);
  return $json;
}

function prepareBlocksRecursively($incomingBlocks, $api, $page, $target) {
  $preparedBlocks = [];
  foreach ($incomingBlocks as $incomingBlock) {
    $preparedBlock = prepareKBlockFromNotionBlock($incomingBlock, $api, $page, $target);
    
    if ($preparedBlock) {
      $preparedBlocks[] = $preparedBlock->toArray();
    }

    // GET COLUMN CHILDREN BLOCKS
    if ($incomingBlock->type === 'column_list') {                  
        $url = 'https://api.notion.com/v1/blocks/' . $incomingBlock->id . '/children?page_size=100';
  
        $headers = [
          'Notion-Version: ' . (string)$api->version(),
          'Authorization: Bearer ' . (string)$api->key()
        ];
  
        //QUERY API
        $curl = curl_init();
        $json = getNotionBlocks($curl, $url, $headers);
        curl_close($curl);
  
        // createJSONFile($page, $incomingBlock->id, $json);
  
        $response = json_decode($json);
        $incomingBlocks = $response->results;

        foreach ($incomingBlocks as $incomingBlock) {
          $url = 'https://api.notion.com/v1/blocks/' . $incomingBlock->id . '/children?page_size=100';
  
          $headers = [
            'Notion-Version: ' . (string)$api->version(),
            'Authorization: Bearer ' . (string)$api->key()
          ];
    
          //QUERY API
          $curl = curl_init();
          $json = getNotionBlocks($curl, $url, $headers);
          curl_close($curl);
    
          createJSONFile($page, $incomingBlock->id, $json);
    
          $response = json_decode($json);
          $incomingBlocks = $response->results;

          foreach ($incomingBlocks as $incomingBlock) {
            $preparedBlock = prepareKBlockFromNotionBlock($incomingBlock, $api, $page, $target);
    
            if ($preparedBlock) {
              $preparedBlocks[] = $preparedBlock->toArray();
            }
          }
        }
    }
  }
  return $preparedBlocks;
}

function prepareKBlockFromNotionBlock($notionBlock, $api, $page, $target) {
  $preparedBlock = null;
  switch ($notionBlock->type) {
    case 'heading_1':
      $strParts = $notionBlock->heading_1->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h1',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'heading_2':
      $strParts = $notionBlock->heading_2->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h2',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'heading_3':
      $strParts = $notionBlock->heading_3->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h3',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
  case 'heading_4':
      $strParts = $notionBlock->heading_4->rich_text;
      if (count($strParts) === 0) break;
      
      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h4',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'paragraph':
      $strParts = $notionBlock->paragraph->rich_text; 
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'text'
      ]);
      break;
    
    case 'divider':
      $preparedBlock = new Kirby\Cms\Block([
        'id' => $notionBlock->id,
        'type' => 'line'
      ]);
      break;
    
    case 'child_page':
      if ($target === 'subpages') {
        //CREATE CHILD PAGE
        $childPage = $page->createChild([
          'id' => $notionBlock->id,
          'slug' => Str::slug($notionBlock->child_page->title),
          'template' => 'composition',
          'content' => [
            'title' => $notionBlock->child_page->title
          ]
        ]);
        
        // CREATE BLOCK LINK
        $preparedBlock = new Kirby\Cms\Block([
          'id' => $notionBlock->id,
          'type' => 'text',
          'content' => [
            'text' => '<a href="' . $childPage->url() . '">' . $notionBlock->child_page->title . '</a>'
          ]
        ]);

        $url = 'https://api.notion.com/v1/blocks/' . $notionBlock->id . '/children?page_size=100';

        $headers = [
          'Notion-Version: ' . (string)$api->version(),
          'Authorization: Bearer ' . (string)$api->key()
        ];

        //QUERY API
        $curl = curl_init();
        $json = getNotionBlocks($curl, $url, $headers);
        curl_close($curl);

        createJSONFile($childPage, $notionBlock->id, $json);

        $response = json_decode($json);
        $incomingBlocks = $response->results;

        $preparedBlocks = prepareBlocksRecursively($incomingBlocks, $api, $childPage, $target);

        $childPage->update([
          'mode' => 'composition',
          'apiPageId' => $notionBlock->id,
          'composition' => json_encode($preparedBlocks)
        ]);

        $childPage->publish();
      }
      break;
  }
  return $preparedBlock;
}

function createJSONFile($page, $name, $json) {
  // $dataFile = fopen($page->contentFileDirectory() . '/json/' . $name . '.json', 'x+');
  // fwrite($dataFile, $json);
  // fclose($dataFile);
}

function getRecursiveTitleAndUrl($page, $template) {  
  if ($page->title()->value() != 'json' && (string)$page->template() === $template) {
    $children = null;
    if ($page->hasChildren() == 'true') {      
      foreach ($page->children()->listed() as $child) {
        if ($child->title()->value() != 'json') {
          $children[] = getRecursiveTitleAndUrl($child, $template);
        }
      }
    }
    $titleAndUrl = [
      'title' => $page->title()->value(),
      'url' => $page->url(),
      'children' => $children
    ];
    return $titleAndUrl;
  }
}

function getRecursiveTitleAndParent($page, $template) {
  if ($page->title()->value() != 'json' && (string)$page->template() === $template) {
    $children = null;
    if ($page->hasChildren() == 'true') {      
      foreach ($page->children() as $child) {
        if ($child->title()->value() != 'json') {
          $children[] = getRecursiveTitleAndParent($child, $template);
        }
      }
    }
    $breadcrumb = Str::split($page->uri(), '/');
    
    $titleAndUrl = [
      'title' => $page->title()->value(),
      'children' => $children,
      'isActive' => false,
      'breadcrumb' => $breadcrumb
    ];
    return $titleAndUrl;
  }
}