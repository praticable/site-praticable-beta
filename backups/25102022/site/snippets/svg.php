<svg display="none" xmlns="http://www.w3.org/2000/svg">
    <symbol id="icon-menu" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M1 4H23"/>
        <path d="M1 12H23"/>
        <path d="M1 20H23"/>
    </symbol>
    <symbol id="icon-close" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M3.51472 20.4853L20.4853 3.51472"/>
        <path d="M3.51472 3.51472L20.4853 20.4853"/>
    </symbol>
    <symbol id="icon-add" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M3 2H21C21.5523 2 22 2.44772 22 3V21C22 21.5523 21.5523 22 21 22H3C2.44772 22 2 21.5523 2 21V3C2 2.44772 2.44772 2 3 2Z"/>
        <path d="M7 12L17 12"/>
        <path d="M12 17L12 7"/>
    </symbol>
    <symbol id="icon-remove" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M7 12L17 12"/>
        <circle cx="12" cy="12" r="9"/>
    </symbol>
    <symbol id="icon-check" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M16 9L11 14L8 11"/>
        <circle cx="12" cy="12" r="9"/>
    </symbol>
    <symbol id="icon-edit" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M16 5.00001L19 2.00001L22 5.00001L19 8.00001M16 5.00001L12 9L11 13L15 12L19 8.00001M16 5.00001L19 8.00001" stroke-linejoin="round"/>
        <path d="M14 2H3C2.44772 2 2 2.44772 2 3V21C2 21.5523 2.44772 22 3 22H21C21.5523 22 22 21.5523 22 21V10"/>
    </symbol>
    <symbol id="icon-chevron-up" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M18 15L12 9L6 15"/>
    </symbol>
    <symbol id="icon-chevron-double-up" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M18 19L12 13L6 19"/>
        <path d="M18 12L12 6L6 12"/>
    </symbol>
    <symbol id="icon-text" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
        <path d="M5 3V2C4.44772 2 4 2.44772 4 3H5ZM19 3H20C20 2.44772 19.5523 2 19 2V3ZM6 6V3H4V6H6ZM18 3V6H20V3H18ZM5 4H12V2H5V4ZM12 4H19V2H12V4ZM11 3V21H13V3H11ZM8 22H16V20H8V22Z"/>
    </symbol>
    <symbol id="icon-image" width="24" height="24" viewBox="0 0 24 24" fill="currentColor" stroke="currentColor">
        <path d="M20 6L16 2V6H20Z"/>
        <path d="M16 2H5C4.44772 2 4 2.44772 4 3V21C4 21.5523 4.44772 22 5 22H19C19.5523 22 20 21.5523 20 21V6M16 2L20 6M16 2V6H20" fill="none" stroke-width="2"/>
        <path d="M13.1 10.6L10.5 14.9L9.4 13L7.5 16H16.5L13.1 10.6Z"/>
    </symbol>
    <symbol id="icon-user" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
        <path d="M12 12C9.8 12 8 10.2 8 8C8 5.8 9.8 4 12 4C14.2 4 16 5.8 16 8C16 10.2 14.2 12 12 12ZM12 6C10.9 6 10 6.9 10 8C10 9.1 10.9 10 12 10C13.1 10 14 9.1 14 8C14 6.9 13.1 6 12 6Z"/>
        <path d="M20 20H4V19C4 15.5 7.3 13 12 13C16.7 13 20 15.5 20 19V20ZM6.2 18H17.9C17.3 16.2 15.1 15 12.1 15C9 15 6.8 16.2 6.2 18Z"/>
    </symbol>
    <symbol id="icon-logout" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M17.5 11L21.5 15L17.5 19"/>
        <path d="M13 15L21.5 15"/>
        <path d="M4 2L10 7V22L4 17V2ZM4 2H14V12" stroke-linejoin="round"/>
    </symbol>
    <symbol id="icon-view" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M12 6.5C7.2 6.5 3.33333 10.1667 2 12C3.33333 13.8333 7.2 17.5 12 17.5C16.8 17.5 20.6667 13.8333 22 12C20.6667 10.1667 16.8 6.5 12 6.5Z" stroke="currentColor" stroke-width="2"/>
        <circle cx="12" cy="12" r="3" fill="currentColor"/>
    </symbol>
    <symbol id="icon-star" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
        <path d="M18 22C17.8 22 17.6 21.9 17.4 21.8L12 18.2L6.60002 21.8C6.30002 22 5.80002 22 5.50002 21.8C5.20002 21.6 5.00002 21.1 5.10002 20.7L6.90002 14.3L2.30002 9.70001C2.00002 9.40001 1.90002 9.00001 2.10002 8.60001C2.20002 8.20001 2.60002 8.00001 3.00002 8.00001H8.40002L11.1 2.60001C11.4 1.90001 12.6 1.90001 12.9 2.60001L15.6 8.00001H21C21.4 8.00001 21.8 8.20001 21.9 8.60001C22.1 9.00001 22 9.40001 21.7 9.70001L17.1 14.3L18.9 20.7C19 21.1 18.9 21.5 18.5 21.8C18.4 21.9 18.2 22 18 22ZM12 16C12.2 16 12.4 16.1 12.6 16.2L16.3 18.7L15 14.3C14.9 14 15 13.6 15.3 13.3L18.6 10H15C14.6 10 14.3 9.80001 14.1 9.40001L12 5.20001L9.90002 9.40001C9.70002 9.80001 9.40002 10 9.00002 10H5.40002L8.70002 13.3C9.00002 13.6 9.10002 13.9 9.00002 14.3L7.70002 18.7L11.4 16.2C11.6 16.1 11.8 16 12 16Z" />
    </symbol>
    <symbol id="icon-star-fill" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
        <path d="M17.4 21.8C17.6 21.9 17.8 22 18 22C18.2 22 18.4 21.9 18.5 21.8C18.9 21.5 19 21.1 18.9 20.7L17.1 14.3L21.7 9.70001C22 9.40001 22.1 9.00001 21.9 8.60001C21.8 8.20001 21.4 8.00001 21 8.00001H15.6L12.9 2.60001C12.6 1.90001 11.4 1.90001 11.1 2.60001L8.40002 8.00001H3.00002C2.60002 8.00001 2.20002 8.20001 2.10002 8.60001C1.90002 9.00001 2.00002 9.40001 2.30002 9.70001L6.90002 14.3L5.10002 20.7C5.00002 21.1 5.20002 21.6 5.50002 21.8C5.80002 22 6.30002 22 6.60002 21.8L12 18.2L17.4 21.8Z" />
  </symbol>
    <symbol id="icon-bold" width="24" height="24" viewBox="0 0 24 24" fill="none">
      <path d="M5.89113 8.736C5.89113 8.288 5.80313 7.93867 5.62713 7.688C5.45646 7.43733 5.21913 7.26133 4.91513 7.16C4.61113 7.05333 4.26446 7 3.87513 7H2.51513V10.36H3.77913C4.04046 10.36 4.29646 10.3387 4.54713 10.296C4.79779 10.2533 5.02446 10.176 5.22713 10.064C5.43513 9.94667 5.59779 9.78133 5.71513 9.568C5.83246 9.35467 5.89113 9.07733 5.89113 8.736ZM5.46713 3.944C5.46713 3.44267 5.30979 3.08267 4.99513 2.864C4.68579 2.64533 4.22713 2.536 3.61913 2.536H2.51513V5.456H3.71513C4.31246 5.456 4.75246 5.33867 5.03513 5.104C5.32313 4.864 5.46713 4.47733 5.46713 3.944ZM8.16313 8.792C8.16313 9.416 8.04046 9.936 7.79513 10.352C7.55513 10.768 7.22713 11.096 6.81113 11.336C6.39513 11.576 5.92313 11.7467 5.39513 11.848C4.87246 11.9493 4.32579 12 3.75513 12H0.323125V0.936H3.55513C4.09913 0.936 4.61646 0.981333 5.10713 1.072C5.59779 1.16267 6.03513 1.31733 6.41913 1.536C6.80846 1.74933 7.11513 2.03733 7.33913 2.4C7.56846 2.76267 7.68313 3.21333 7.68313 3.752C7.68313 4.20533 7.58713 4.59467 7.39513 4.92C7.20846 5.24533 6.95779 5.50933 6.64313 5.712C6.33379 5.90933 6.00046 6.05333 5.64313 6.144C6.04313 6.208 6.43513 6.33333 6.81913 6.52C7.20846 6.70133 7.52846 6.976 7.77913 7.344C8.03513 7.70667 8.16313 8.18933 8.16313 8.792Z" fill="black"/>
    </symbol>
    <symbol id="icon-italic" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M21.4 3.70001V1.70001H11.4V3.70001H14.9L7.80002 19.7H3.40002V21.7H13.4V19.7H9.90002L17 3.70001H21.4Z" fill="currentColor"/>
    </symbol>
    <symbol id="icon-code" width="24" height="24" fill="none">
        <g clip-path="url(#a)" fill="currentColor">
            <path d="m8 22 5.75-20H16l-5.75 20H8ZM6.7 5 0 11.7l6.7 6.7L8.1 17l-5.3-5.3 5.3-5.3L6.7 5ZM16 6.4l5.3 5.3L16 17l1.4 1.4 6.7-6.7L17.4 5 16 6.4Z"/>
        </g>
        <defs><clipPath id="a"><path fill="#fff" d="M0 0h24v24H0z"/></clipPath></defs>
    </symbol>
    <symbol id="icon-quote" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M18 13L18 12L15 12C13.9 12 13 11.1 13 10L13 6C13 4.9 13.9 4 15 4L19 4C19.6 4 20 4.4 20 5L20 13C20 16.866 16.866 20 13 20L13 18C15.7614 18 18 15.7614 18 13Z" fill="currentColor"/>
        <path d="M9 13L9 12L6 12C4.9 12 4 11.1 4 10L4 6C4 4.9 4.9 4 6 4L10 4C10.6 4 11 4.4 11 5L11 13C11 16.866 7.86599 20 4 20L4 18C6.76142 18 9 15.7614 9 13Z" fill="currentColor"/>
    </symbol>
    <symbol id="icon-link" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M20 4L14 10" stroke-linecap="square" stroke-linejoin="round"/>
        <path d="M15 4L14.4142 3.41421C13.6332 2.63316 12.3668 2.63316 11.5858 3.41421L3.41421 11.5858C2.63317 12.3668 2.63317 13.6332 3.41421 14.4142L4 15"/>
        <path d="M9 20L9.58579 20.5858C10.3668 21.3668 11.6332 21.3668 12.4142 20.5858L20.5858 12.4142C21.3668 11.6332 21.3668 10.3668 20.5858 9.58579L20 9"/>
        <path d="M10 14L4 20" stroke-linecap="square" stroke-linejoin="round"/>
    </symbol>
    <symbol id="icon-note" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2">
        <path d="M7 2H3C2.44772 2 2 2.44772 2 3V21C2 21.5523 2.44772 22 3 22H7"/>
        <path d="M17 22H21C21.5523 22 22 21.5523 22 21V3C22 2.44772 21.5523 2 21 2H17"/>
        <path d="M13 17V7V7C13 9.20914 11.2091 11 9 11V11"/>
    </symbol>
    <symbol id="icon-markdown" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M3 6H21C21.5523 6 22 6.44772 22 7V17C22 17.5523 21.5523 18 21 18H3C2.44772 18 2 17.5523 2 17V7C2 6.44772 2.44772 6 3 6Z" stroke="currentColor" stroke-width="2"/>
        <path d="M14 12L17 15L20 12H14Z" fill="currentColor"/>
        <path d="M6 15V10H6.5L9 12.5L11.5 10H12V15" stroke="currentColor" stroke-width="2"/>
        <path d="M17 9V12" stroke="currentColor" stroke-width="2"/>
    </symbol>
</svg>
