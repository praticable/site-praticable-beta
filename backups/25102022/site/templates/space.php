<?= snippet('header') ?>
<?php if ($page->mode() == 'iframe'): ?>
  <?= $page->iframe() ?>
<?php elseif ($page->mode() == 'pdf'): ?>
  <iframe src="<?= $page->pdf()->toFile()->url() ?>"></object>
<?php elseif ($page->mode() == 'htmlFiles' && $page->htmlFiles()->isNotEmpty()): ?>
  <?php 
    $dirname = $page->htmlFiles()->toFile()->name();
    $indexPath = $kirby->root('assets') . '/static-data/' . $page->slug() . '/' . $dirname . '/index.html';
    include_once($indexPath); 
  ?>
<?php elseif ($page->mode() == 'composition'): ?>
    <div id="mobile-disclaimer">
      <h1>
        Collectif Bam<br />
        devient Praticable
      </h1>
      <h4>
        La version mobile arrive bientôt mais vous pouvez pratiquer la version ordinateur.
      </h4>
    </div>
    <!--========== APP ==========-->
    <div 
        id="app"
        v-if="store.state.isDesktop"
        class="dragscroll grid"
        :class="store.state.layout"
        @click="hideContextMenu"
        @contextmenu="toggleContext"
    >
      <apparatus
        :index="index"
        :tags="tags"
        :page="page"
        :page-colors-label="pageColors.label"
        :breadcrumb="breadcrumb"
        :is-log="isLog"
      ></apparatus>
      <main class="main" role="main">
          <h1 class="hidden"><?= $page->title() ?></h1>
          <ul id="context-menu" class="| hidden">
            <li v-for="option in store.state.contextMenu.options">
              <button 
                v-if="
                option.concernedBlocks.includes(store.state.contextMenu.target.type) 
                && option.condition(store.state.contextMenu.target)
                " 
                class="context-menu__button| border prevent-unselect" 
                @click="store.handleContextMenuFunction(option.function)">{{ option.label }}</button>
            </li>
          </ul>
          <blocks v-if="blocks.length"
            :blocks="blocks"
            :page="page"
            :update-signal="updateSignal"
          ></blocks>
      </main>
    </div>
<?php endif ?>

    <?= snippet('variables') ?>
</body>
</html>