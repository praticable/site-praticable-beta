<?php

return function($page, $site, $kirby) {
  session_start();
  $_SESSION["redirect_url"] = (string)$page->url();

  $authPages = null;
  
  if (!$kirby->user()) {
    $authPages = json_encode([]); 
  } else {
    if ($kirby->user()->role() === 'admin'){
      $authPages = 'all';
    } else {
      $authPages = json_encode($kirby->user()->authPages()->split());
    }
  }

  //================================================================ BLOCKS
  $blocks = [];
  foreach ($page->composition()->toBlocks() as $rawBlock) {
    
    $block = $rawBlock->toArray();
    if ($rawBlock->type() === 'image' && $rawBlock->image()->toFile()) {      
      $block['content']['thumb'] = $rawBlock->image()->toFile()->extension() == 'gif' ? $rawBlock->image()->toFile()->url() : $rawBlock->image()->toFile()->url();
      $block['content']['caption'] = $rawBlock->caption()->kt()->value();
    }
    
    if ($rawBlock->type() === 'representative' && $rawBlock->linkedSpaces()->isNotEmpty()) {
      $linkedSpaces = [];
      foreach ($rawBlock->linkedSpaces()->toPages() as $linkedSpace) {
        $linkedSpace = [
          'title' => $linkedSpace->title()->value(),
          'url' => $linkedSpace->url()
        ];
        $linkedSpaces[] = $linkedSpace;
      }
      $block['content']['linkedspaces'] = $linkedSpaces;
    }
    $blocks[] = $block;
  }

  //================================================================ RELATED PAGES
  $callablePages = $site->index()->filterBy('template', 'callable');
  $relatedPages = [];
  foreach ($callablePages as $callablePage) {
    if (in_array($page->title(), $callablePage->categories()->split())) {
      $relatedPages[] = [
        'title' => $callablePage->title()->value(),
        'categories' => $callablePage->categories()->split(),
        'type' => 'related',
        'content' => [
          'text' => '',
          'width' => '',
          'height' => '',
          'transform' => '',
          'zindex' => 0,
          'backgroundcolor' => '#FFF',
          'textcolor' => '#000',
          'refs' => ''
        ]
      ];
    }
  }

  return [
    "blocks" => array_merge($blocks, $relatedPages),
    "authPages" => $authPages
  ];
};