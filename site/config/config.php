<?php

return [
    'debug' => true,
    'thumbs' => [
      'presets' => [
        'default' => [
          'width' => 1920
        ]
      ]
    ],
    'smartypants' => true,
    'routes' => [
        require_once 'routes/sitemap-xml.php',
        require_once 'routes/sitemap.php'
    ]
];