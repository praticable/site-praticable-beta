<?php

return [
  'pattern' => 'sitemap',
  'action'  => function() {
    $site = site();
    return go($site->url() . '/sitemap.xml', 301);
  }
];