<?php
return [
  'pattern' => 'sitemap.xml',
  'action'  => function() {
      $pages = site()->pages()->index()->filterBy('status', 'listed')->filterBy('template', 'space');
      // throw new Exception(json_encode($pages), 1);
      

      // fetch the pages to ignore from the config settings,
      // if nothing is set, we ignore the error page
      $ignore = kirby()->option('sitemap.ignore', [
        'error'
      ]);

      $content = snippet('sitemap', compact('pages', 'ignore'), true);

      // return response with correct header type
      return new Kirby\Cms\Response($content, 'application/xml');
  }
];