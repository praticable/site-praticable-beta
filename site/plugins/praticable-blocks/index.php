<?php

require_once __DIR__ . '/src/helpers/index.php';

Kirby::plugin('adrienpayet/praticable-blocks', [
  'routes' => [
    require_once __DIR__ . '/src/routes/save.php',
    require_once __DIR__ . '/src/routes/fetch.php',
    require_once __DIR__ . '/src/routes/external-request.php'
  ],
  'hooks' => [
    'page.create:after' => function ($page) {
      require_once __DIR__ . '/src/hooks/create-static-dir.php';
      // mkdir($page->root() . '/json');
    },
    'page.update:after' => function ($oldPage, $newPage) {
      require_once __DIR__ . '/src/hooks/create-or-update-representatives.php';
    },
    'page.changeStatus:after' => function (Kirby\Cms\Page $newPage, Kirby\Cms\Page $oldPage) {
      require_once __DIR__ . '/src/hooks/update-blocks-a-tags.php';
      if ($newPage->isPublished()) {
        require_once __DIR__ . '/src/hooks/create-or-update-representatives.php';
      } else if ($newPage->isDraft()) {
        require_once __DIR__ . '/src/hooks/remove-parent-representative.php';
      }
    },
    'page.delete:before' => function ($page) {
      require_once __DIR__ . '/src/hooks/remove-static-dir.php';
      require_once __DIR__ . '/src/hooks/remove-parent-representative.php';
    },
    'file.create:after' => function ($file) {
      // GET GLOBAL OBJECTS
      $kirby = kirby();
      $site = site();
      $page = $file->page();

      if ($file->type() == 'archive') {
        require_once __DIR__ . '/src/hooks/create-html-from-archive.php';
      }
    },
    'file.delete:before' => function ($file) {
      if ($file->type() == 'archive') {
        require_once __DIR__ . '/src/hooks/remove-html-from-archive.php';
      }
    }
  ],
  'blueprints' => [
    'blocks/markdown' => __DIR__ . '/src/blueprints/blocks/markdown.yml',
    'blocks/images' => __DIR__ . '/src/blueprints/blocks/images.yml',
    'blocks/file' => __DIR__ . '/src/blueprints/blocks/file.yml',
    'blocks/representative' => __DIR__ . '/src/blueprints/blocks/representative.yml',
    'blocks/code' => __DIR__ . '/src/blueprints/blocks/code.yml'
  ]
]);