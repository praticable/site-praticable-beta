# Praticable blocks

This plugin mainly allows several blocks processes at the heart of the praticable.fr website.

## Installation

### Download

Download and copy this repository to `/site/plugins/praticable-blocks`.

### Composer

```
composer require adrienpayet/praticable-blocks
```

## Features

_Documentation in progress_

### Store JSON data as Kirby blocks

One of the main features consist in storing JSON data sent from the front-end through a POST request to [Kirby blocks](https://getkirby.com/docs/reference/panel/fields/blocks). POST request can fetch any route ending by save.php, eg. `www.your-website.com/save.json` or `www.your-website.com/page/save.json`. Data should be stored in the body request, following that structure :

```typescript
type BodyRequest = {
  pageUri: string
  blocks: Array<MarkdownBlock | ImageBlock>
}

type MarkdownBlock = {
  type: "markdown"
  content: {
    text: string
    width?: string = "400px"
    height?: string = "200px"
    transform?: string = "translate(912px, 240px)"
    zindex?: number = 0
    refs?: string = ""
  }
}

type ImageBlock = {
  type: "image"
  content: {
    image: Array<string> // eg. ['example-image.jpeg']
    b64: string // A b64 string of the image
    width?: string = "400px"
    height?: string = "400px"
    transform?: string = "translate(0px, 0px)"
    zindex?: number = 0
    iscover?: boolean = false
    caption?: string = ""
  }
}
```

### Representatives

#### Concept

![representative structure](./doc/representative-structure.png)

Representatives are blocks that represent spaces (pages) and allow to navigate to it. At least, they contain the title of the represented space, linked to it (wrapped in a `<a>` tag). Optionnaly, they can contain a cover image and a introduction text.

#### Usage

<!-- ## Development

_Add instructions on how to help working on the plugin (e.g. npm setup, Composer dev dependencies, etc.)_ -->

## License

MIT

## Credits

- [Adrien Payet](https://framagit.org/isUnknown) for [Praticable](https://framagit.org/praticable)
