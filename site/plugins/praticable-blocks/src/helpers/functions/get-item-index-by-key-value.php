<?php

function getItemIndexByKeyValue($key, $value, $blocks) {
  foreach ($blocks as $index => $block) {
    $isKeyExists = array_key_exists('title', $block['content']);
    $isTarget = ((int)$isKeyExists === 1) && ($block['content'][$key] == $value);
    if ($isTarget) {      
      return $index;
      break;
    }
  }
}