<?php

function addHTMLTagsToNotionStrPart($strPart) {
  $string = $strPart->plain_text;
  
  if ($strPart->annotations->underline) {
    $string = '<u>' . $strPart->plain_text . '<\/u>';
  }
  if ($strPart->annotations->strikethrough) {
    $string = '<s>' . $strPart->plain_text . '<\/s>';
  }
  if ($strPart->annotations->italic) {
    $string = '<em>' . $strPart->plain_text . '<\/em>';
  }
  if ($strPart->annotations->bold) {
    $string = '<strong>' . $strPart->plain_text . '<\/strong>';
  }
  if ($strPart->annotations->code) {
    $string = '<code>' . $strPart->plain_text . '<\/code>';
  }
  
  return $string;
}