<?php
function getRecursiveTitleAndParent($page, $template) {
  if ($page->title()->value() != 'json' && (string)$page->template() === $template) {
    $children = null;
    if ($page->hasChildren() == 'true') {      
      foreach ($page->children() as $child) {
        if ($child->title()->value() != 'json') {
          $children[] = getRecursiveTitleAndParent($child, $template);
        }
      }
    }
    $breadcrumb = Str::split($page->uri(), '/');
    
    $titleAndUrl = [
      'title' => $page->title()->value(),
      'children' => $children,
      'isActive' => false,
      'breadcrumb' => $breadcrumb
    ];
    return $titleAndUrl;
  }
}