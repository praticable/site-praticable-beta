<?php
$functionsDir = __DIR__ . '/functions';
$functions = glob($functionsDir . '/*.php');

foreach ($functions as $function) {
    require($function);
}