<?php

$target = isset($newPage) ? $newPage : $page;

$parent = $target->parent();

if (!$parent) {
    return;
}

$representative = $parent->composition()->toBlocks()->find($target->id());
if ($representative != null && $target->title() != "json") {
  $newParentComposition = $parent->composition()->toBlocks()->remove($representative->id());

  $parent->update([
    'composition' => json_encode($newParentComposition->toArray())
  ]);
}