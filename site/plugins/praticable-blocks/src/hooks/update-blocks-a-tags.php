<?php

$site = site();
$originalBlocks = $newPage->composition()->toBlocks()->toArray();
$newBlocks = [];

// Fonction de remplacement
function replace_urls($match, $newPage) {
  $url = $match[0];
  $basename = basename($url);
  $newFilePath = $newPage->files()->find($basename);
  if ($newFilePath) {
    return $newFilePath;
  } else {
    return $url;
  }
}

foreach ($originalBlocks as $originalBlock) {  
  if ($originalBlock['type'] === 'markdown' || $originalBlock['type'] === 'text') {
    if (Str::contains($originalBlock['content']['text'], $site->url())) {
      // Expression régulière pour capturer les URLs
      $regex = '/\bhttps?:\/\/\S+\b/';

      // Remplacement des URLs
      $updatedText = preg_replace_callback(
          $regex, 
          function ($match) use ($newPage) {
            return replace_urls($match, $newPage);
          }, 
          $originalBlock['content']['text']
        );
      $originalBlock['content']['text'] = $updatedText; 
    }
  }
  $newBlocks[] = $originalBlock;
}

$newPage->update([
  'composition' => $newBlocks
]);