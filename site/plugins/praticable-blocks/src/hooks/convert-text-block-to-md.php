<?php

$site = site();
$index = $site->index(true);

foreach ($index as $targetPage) {
  $blocks = $targetPage->composition()->toBlocks();
  
  $newBlocks = [];
  
  foreach ($blocks as $block) {
    $newBlock = $block->toArray();
    if ($newBlock['type'] === 'text') {
      $newBlock['type'] = 'markdown';
    }
    $newBlocks[] = $newBlock;
  }
  
  $flatNewBlocks = array_values($newBlocks);
  
  $targetPage->update([
    'composition' => json_encode($flatNewBlocks)
  ]);
}