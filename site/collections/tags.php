<?php

return function ($site) {
  $tags = [];
  
  if ($site->hasChildren()) {
    foreach ($site->children()->listed()->filterBy('template', 'tag') as $child) {
      if (!$child->isErrorPage()) {
        $tags[] = getRecursiveTitleAndParent($child, 'tag');
      }
    }
  }

  return json_encode($tags);
};