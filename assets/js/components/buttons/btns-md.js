import Store from "../../store.js";
import noteMixin from "../blocks/note-mixin.js";
import { BlockFactory } from "../../classes/BlockFactory.js";

const BtnsMd = {
  props: {
    block: Object,
  },
  mixins: [noteMixin],
  data: function () {
    return {
      store: Store,
    };
  },
  template: `
    <div id="md-buttons" class="| flex prevent-unselect">
        <button 
            id="bold" 
            class="btn btn--icon border border--blue"
            title="Mette le texte en gras." 
            @click="insert('bold')"
        >   
            B
            <span class="sr-only">Mettre le texte en gras.</span>
        </button>
        <button 
            id="italic" 
            class="btn btn--icon border border--blue"
            title="Mettre le texte en italique." 
            @click="insert('italic')"
        >   
            I
            <span class="sr-only">Mettre le texte en italique.</span>
        </button>
        <button 
            id="code" 
            class="btn btn--icon border border--blue"
            title="Mettre le texte sous forme de code." 
            @click="insert('code')"
        >   
          &lt;/&gt;
            <span class="sr-only">Mettre le texte sous forme de code.</span>
        </button>
        <button 
            id="link" 
            class="btn btn--icon border border--blue"
            title="Insérer un lien." 
            @click="insert('link')"
        >   
            <svg class="icon"><use xlink:href="#icon-link"></use></svg>
            <span class="sr-only">Insérer un lien.</span>
        </button>
        <button 
            id="quote" 
            class="btn btn--icon border border--blue"
            title="Insérer une citation." 
            @click="insert('quote')"
        >   
            «»
            <span class="sr-only">Insérer une citation.</span>
        </button>
        <button 
            id="note" 
            class="btn btn--icon border border--blue"
            title="Insérer une note en créant un nouveau bloc texte." 
            @click="insert('note')"
        >   
            [1]
            <span class="sr-only">Insérer une note en créant un nouveau bloc texte.</span>
        </button>
        <a 
            id="markdown" 
            class="btn btn--icon border border--blue"
            title="Ouvrir l'aide Markdown."
            href="https://duckduckgo.com/?t=ffab&q=markdown+cheatsheet&atb=v243-1&ia=answer&iax=1"
            target="_blank"
        >   
            <svg class="icon"><use xlink:href="#icon-markdown"></use></svg>
            <span class="sr-only">Ouvrir l'aide Markdown.</span>
        </a>
    </div>
  `,
  methods: {
    preventDeselect: function () {
      document.querySelectorAll("#md-buttons .btn").forEach((btn) => {
        btn.addEventListener("mousedown", (e) => {
          e = e || window.event;
          e.preventDefault();
        });
      });
    },
    insert: function (btn) {
      let field = document.querySelector("textarea");
      let syntax;
      let selectionStart;

      switch (btn) {
        case "bold":
          syntax = `****`;
          selectionStart = 2;
          break;
        case "italic":
          syntax = `__`;
          selectionStart = 1;
          break;
        case "link":
          syntax = `[](https://)`;
          selectionStart = 1;
          break;
        case "code":
          syntax = `\`\``;
          selectionStart = 1;
          break;
        case "quote":
          syntax = `> `;
          selectionStart = 2;
          break;
        case "note":
          const container = document.querySelector(".blocks");
          const fieldWidth = parseInt(
            window.getComputedStyle(field).getPropertyValue("width")
          );
          const fieldX = this.getOffset(field).left;
          const fieldY = this.getOffset(field).top;
          const containerX = this.getOffset(container).left;
          const containerY = this.getOffset(container).top;
          const gridGap = 10;
          const noteTranslateX = fieldX - containerX + (fieldWidth + gridGap);
          const noteTranslateY = fieldY - containerY;
          const noteId = Date.now().toString();

          syntax = "[]";
          selectionStart = 1;

          const params = {
            refs: noteId,
            id: this.block.id,
            left: noteTranslateX,
            top: noteTranslateY,
            text: `[${this.noteIndex}]`,
          };
          const factory = new BlockFactory();
          const newBlock = factory.createBlock("markdown", params);

          this.store.state.localBlocks.push(newBlock);
          this.store.focusField();
          this.addRef(newBlock.ref);
          break;
      }

      // Current Selection
      const textarea = document.querySelector("textarea");
      const currentSelectionStart = textarea.selectionStart;
      const currentSelectionEnd = textarea.selectionEnd;
      const currentText = textarea.value;
      const placeholder = btn === "note" ? this.noteIndex : "écrivez ici";
      const selectionEnd =
        btn === "note"
          ? selectionStart + this.noteIndex.toString.length
          : selectionStart + placeholder.length;

      if (currentSelectionStart === currentSelectionEnd) {
        const textWithSyntax = (textarea.value =
          currentText.substring(0, currentSelectionStart) +
          syntax +
          currentText.substring(currentSelectionEnd));
        textarea.value =
          textWithSyntax.substring(0, currentSelectionStart + selectionStart) +
          placeholder +
          textWithSyntax.substring(currentSelectionStart + selectionStart);

        textarea.focus();
        textarea.setSelectionRange(
          currentSelectionStart + selectionStart,
          currentSelectionEnd + selectionEnd
        );
      } else {
        const selectedText = currentText.substring(
          currentSelectionStart,
          currentSelectionEnd
        );
        const withoutSelection =
          currentText.substring(0, currentSelectionStart) +
          currentText.substring(currentSelectionEnd);
        const textWithSyntax =
          withoutSelection.substring(0, currentSelectionStart) +
          syntax +
          withoutSelection.substring(currentSelectionStart);

        // Surround selected text
        textarea.value =
          textWithSyntax.substring(0, currentSelectionStart + selectionStart) +
          selectedText +
          textWithSyntax.substring(currentSelectionStart + selectionStart);

        textarea.focus();
        textarea.selectionEnd =
          currentSelectionEnd + selectionStart + selectedText.length;
      }
      // Update block.content.text
      setTimeout(() => {
        console.log(textarea.value);
        this.block.content.text = textarea.value;
      }, 100);
    },
    addRef: function (id) {
      this.block.content.refs +=
        this.block.content.refs.length === 0 ? id : `, ${id}`;
    },
    getSelectedText: function () {
      const textField = document.querySelector("textarea");
      const selectedText = textField.value.substring(
        textField.selectionStart,
        textField.selectionEnd
      );
      return selectedText;
    },
    getOffset: function (el) {
      const rect = el.getBoundingClientRect();
      return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY,
      };
    },
  },
  mounted: function () {
    this.preventDeselect();
  },
};

export default BtnsMd;
