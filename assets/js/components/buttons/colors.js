import Store from "../../store.js"

const Colors = {
    props: {
      palette: Array
    },
    data: function() {
      return {
        colors: {},
        store: Store
      }
    },
    template: `
        <div id="colors" class="fixed flex" aria-label="Couleurs">
            <div class="btn btn--radio" v-for="color in palette">
                <input 
                  type="radio" 
                  :id="'color-'+color.id" 
                  name="color" 
                  :value="{
                    background: color.background,
                    text: color.text,
                    label: color.label
                  }" 
                  :style="
                    '--color-bg: ' + color.background + ';' +
                    '--color-text: ' + color.text + ';' 
                  " 
                  :title="color.label" 
                  @click="store.state.isUpToDate = false"
                  v-model="colors"
                />
                <label :for="'color-' + color.id">{{ color.label }}</label>
            </div>
        </div>
    `
}

export default Colors