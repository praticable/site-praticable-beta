import Store from "../../store.js";

const DesktopView = {
  props: {
    index: Array,
    page: String,
    pageUri: String,
    "page-colors-label": String,
    breadcrumb: Array,
  },
  data: function () {
    return {
      store: Store,
    };
  },
  template: `
    <div 
        id="viewfinder__top"
        class="viewfinder__section"
      >
        <index
          class="viewfinder__btn"
          :index="index"
          :page="page"
          :breadcrumb="breadcrumb"
          @switchOffViewfinder="off"
        ></index>
      </div>

      <div id="viewfinder__middle">
        <prev-next v-if="store.state.layout === 'full'"></prev-next>
      </div>
     
      <div
        id="viewfinder__bottom"
        class="viewfinder__section"
      >
        <tune
          class="viewfinder__btn"
          :tags="tags"
          @switchOffViewfinder="off"
        ></tune>
        <register v-if="!store.state.isUpToDate && (store.state.isLog || store.state.isOpenSpace)"
          class="viewfinder__btn"
          :pageUri="pageUri"
          :page-colors-label="pageColorsLabel"
        ></register>
        <modify
          @switchOffViewfinder="off"
          class="viewfinder__btn"
        ></modify>
      </div>
  `,
};

export default DesktopView;
