import Store from "../../../store.js";

const viewfinderBtnMixin = {
  data: function () {
    return {
      store: Store,
    };
  },
  computed: {
    isviewfinderActive: function () {
      return this.store.state.viewfinder.isActive;
    },
  },
  watch: {
    isviewfinderActive: function (newValue) {
      if (newValue === false) {
        this.isOpen = false;
      }
    },
  },
  methods: {
    toggleIsOpen: function (menuName) {
      if (this.store.state.viewfinder.isActive) {
        this.$emit("switchOffViewfinder");
      } else {
        this.isOpen = !this.isOpen;
        this.store.state.viewfinder.isActive = this.isOpen;
        if (this.isOpen) {
          setTimeout(() => {
            const optionsMaxHeight = this.calculateOptionsMaxHeight();
            if (document.querySelector(".lock-visible") > optionsMaxHeight) {
              document.querySelector(".lock-visible").classList.add("scroll-y");
            }
          }, 100);
        }
      }
    },
    calculateOptionsMaxHeight: function () {
      const btnHeight = document.querySelector(
        "#viewfinder--mobile__current-section"
      ).offsetHeight;
      const optionsMaxHeight = window.innerHeight - btnHeight;

      return optionsMaxHeight;
    },
    setHoverColor: function (e) {
      if (!e.target.closest(".wrapper")) return;
      e.target.closest(".wrapper").classList.add("hover");
    },
    unsetHoverColor: function (e) {
      if (!e.target.closest(".wrapper")) return;
      e.target.closest(".wrapper").classList.remove("hover");
    },
  },
};

export default viewfinderBtnMixin;
