const navigateMobileMixin = {
  methods: {
    activeParents: function (activeSection) {
      const parent =
        activeSection.tagName === "UL"
          ? activeSection.closest("li")
          : activeSection.closest("ul");
      if (!parent.classList.contains("active-section"))
        parent.classList.add("active-section");

      if (parent.classList.contains("lock-visible")) return;
      this.activeParents(parent);
    },
    navigateTo: function (section, event) {
      if (this.store.state.isDesktop) return;
      event.preventDefault();

      this.store.state.viewfinder.currentSection = section;

      const newActiveSection = event.currentTarget.closest("li");
      newActiveSection.classList.add("active-section");

      this.activeParents(newActiveSection);
      this.sizeDisplayedUl();
    },
    sizeDisplayedUl: function () {
      const activeSections = document.querySelectorAll(".active-section");

      this.removeCurrentOptionsClass();

      const target = activeSections[activeSections.length - 1]
        ? activeSections[activeSections.length - 1].querySelector("ul")
        : document.querySelector(".lock-visible");

      const optionsMaxHeight = this.calculateOptionsMaxHeight();
      if (target.offsetHeight > optionsMaxHeight) {
        console.log("add scroll-y class");
        target.classList.add("scroll-y");
      }
    },
    removeCurrentOptionsClass: function () {
      if (document.querySelector(".scroll-y")) {
        document.querySelector(".scroll-y").classList.remove("scroll-y");
      }
    },
    calculateOptionsMaxHeight: function () {
      if (!document.querySelector("#viewfinder--mobile__current-section"))
        return;
      const btnHeight = document.querySelector(
        "#viewfinder--mobile__current-section"
      ).offsetHeight;
      const optionsMaxHeight = window.innerHeight - btnHeight;

      return optionsMaxHeight;
    },
  },
};

export default navigateMobileMixin;
