import Store from "../../../../store.js";
import viewfinderBtnMixin from "../../mixins/viewfinder-btn-mixin.js";
import Tags from "./tags.js";

const Refine = {
  props: {
    tags: Array,
  },
  mixins: [viewfinderBtnMixin],
  components: {
    tags: Tags,
  },
  data: function () {
    return {
      isOpen: false,
      store: Store,
    };
  },
  computed: {
    operatingFilters: function () {
      const operatingFilters = [];
      this.store.state.blocks.localVersion.forEach((block) => {
        if (!block.content.tags || block.content.tags.length === 0) return;
        const tags = block.content.tags.split(/,|\//).map((tag) => tag.trim());

        tags.forEach((tag) => {
          if (!operatingFilters.includes(tag)) {
            operatingFilters.push(tag);
          }
        });
      });
      return operatingFilters;
    },
  },
  template: `
  <li v-if="operatingFilters.length > 0">
    <span>
      <button 
        class="| text-2 border"
        :class="{ 'lock-active': store.state.filters.length > 0 }"
      >filtre(s)</button>
      <span
        class="menu__arrow | text-2 border"
        tabindex="0"
      >-></span>
    </span>
    <tags
      :operatingFilters="operatingFilters"
      :tags="tags"
    ></tags>
  </li>
  `,
};

export default Refine;
