import Index from "./index/index.js";
import PrevNext from "./tune/composition/prev-next.js";
import Store from "../../store.js";
import Register from "./register.js";
import Tune from "./tune/tune.js";
import Modify from "./modify/modify.js";
import CurrentSection from "./current-section.js";
import IndexMobile from "./index/index-mobile.js";

const viewfinder = {
  props: {
    index: Array,
    flatIndex: Array,
    tags: Array,
    page: String,
    pageUri: String,
    "page-colors-label": String,
    breadcrumb: Array,
  },
  components: {
    index: Index,
    tune: Tune,
    register: Register,
    modify: Modify,
    "index-mobile": IndexMobile,
    "prev-next": PrevNext,
    "current-section": CurrentSection,
  },
  data: function () {
    return {
      store: Store,
      offscreenBlocks: {
        top: null,
        right: null,
        bottom: null,
        left: null,
      },
    };
  },
  template: `
    <div 
      id="viewfinder" 
      ref="viewfinder" 
      :class="{ active: store.state.viewfinder.isActive }"
    >
      <template v-if="store.state.isDesktop">
        <div 
          id="viewfinder__top"
          class="viewfinder__section"
        >
          <index
            class="viewfinder__btn"
            :index="index"
            :page="page"
            :breadcrumb="breadcrumb"
            @switchOffViewfinder="off"
          ></index>
        </div>

        <div id="viewfinder__middle">
          <prev-next v-if="store.state.layout === 'full'"></prev-next>
        </div>
      
        <div
          id="viewfinder__bottom"
          class="viewfinder__section"
        >
          <tune
            class="viewfinder__btn"
            :tags="tags"
            @switchOffViewfinder="off"
          ></tune>
          <register v-if="!store.state.isUpToDate && (store.state.isLog || store.state.isOpenSpace)"
            class="viewfinder__btn"
            :pageUri="pageUri"
            :page-colors-label="pageColorsLabel"
          ></register>
          <modify
            @switchOffViewfinder="off"
            class="viewfinder__btn"
          ></modify>
        </div>
      </template>
    </div>
  `,
  methods: {
    off: function (e) {
      this.clearActiveItems();
      this.store.state.viewfinder.isActive = false;
      this.store.state.viewfinder.isIndexMobileOpen = false;
      const lastBlock = this.store.getLastBlock();
      if (lastBlock.type === "markdown" || lastBlock.type === "text") {
        if (
          lastBlock.content.text.length === 0 &&
          !e.target.classList.contains("keep-new-block")
        ) {
          this.store.removeBlockById(lastBlock.id);
        }
      }
    },
    clearActiveItems: function () {
      const activeItems = document.querySelectorAll(".active-section");
      activeItems.forEach((activeItem) => {
        activeItem.classList.remove("active-section");
      });
    },
    enableEscHotkey: function () {
      document.addEventListener("keyup", (e) => {
        if (e.keyCode === 27) {
          this.off(e);
        }
      });
    },
    enableDetectOffscreenBlocks: function () {
      setTimeout(() => {
        this.setOffscreenBlocks();
      }, 500);
    },
    setOffscreenBlocks: function () {
      this.offscreenBlocks.right =
        this.isOffscreenY() === undefined ? false : true;
      this.offscreenBlocks.bottom =
        this.isOffscreenX() === undefined ? false : true;
    },
    getXPosFromTransform: function (transform) {
      const x = transform.match(/\((.*)\)/)[1].split(",")[0];
      return parseInt(x);
    },
    isOffscreenY: function () {
      return this.store.state.blocks.localVersion.find((block) => {
        const y = block.content.transform.match(/\((.*)\)/)[1].split(",")[1];
        return parseInt(y) > window.scrollX;
      });
    },
    isOffscreenX: function () {
      return this.store.state.blocks.localVersion.find((block) => {
        const x = block.content.transform.match(/\((.*)\)/)[1].split(",")[0];
        return this.getXPosFromTransform(parseInt(x)) > window.scrollX;
      });
    },
  },
  mounted: function () {
    document.querySelector("#app").addEventListener("click", (e) => {
      if (!e) return;
      const tagName = e.target.tagName.toLowerCase();
      const isNotException =
        tagName !== "label" &&
        tagName !== "input" &&
        tagName !== "button" &&
        tagName !== "textarea" &&
        !e.target.classList.contains("menu__arrow") &&
        tagName !== "a" &&
        e.target.closest("button") === null;

      if (isNotException) {
        this.off(e);
      }
    });
    this.enableEscHotkey();
    this.setOffscreenBlocks();
  },
};

export default viewfinder;
