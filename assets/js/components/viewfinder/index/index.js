import Store from "../../../store.js";
import viewfinderBtnMixin from "../mixins/viewfinder-btn-mixin.js";
import IndexLevel from "./index-level.js";

const Index = {
  props: {
    index: Array,
    breadcrumb: Array,
    page: String,
  },
  mixins: [viewfinderBtnMixin],
  data: function () {
    return {
      isOpen: false,
      store: Store,
      name: "naviguer",
    };
  },
  components: {
    "index-level": IndexLevel,
  },
  template: `
    <ul class="menu top left">
      <li>
        <span @click="toggleIsOpen(name)" class="wrapper" @mouseover="setHoverColor" @mouseleave="unsetHoverColor">
          <button 
            id="search"
            class="| text-2 border lock-active"
            title="Chercher une page du site."
            :class="{'no-hover': store.state.isMobile }"
          >{{ this.name }}</button>
          <span
            class="menu__arrow | text-2 border lock-active"
            tabindex="0"
          >-></span>
        </span>
        <index-level
          v-if="isOpen"
          :index-level="index"
          :parent-url="''"
          :breadcrumb="breadcrumb"
          :page="page"
        ></index-level>
      </li>
    </ul>
  `,
};

export default Index;
