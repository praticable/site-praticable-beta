import Store from "../../../store.js";

const IndexMobile = {
  props: {
    flatIndex: Array,
    pageTitle: String,
    pageUri: String,
  },
  data: function () {
    return {
      currentSpaceUri: this.pageUri,
      store: Store,
      scrollPos: 0,
    };
  },
  computed: {
    currentSpace: function () {
      const target = this.flatIndex.find(
        (indexedSpace) => indexedSpace.uri === this.currentSpaceUri
      );

      if (target.children) {
        return target;
      } else {
        return this.flatIndex.find(
          (indexedSpace) => indexedSpace.uri === target.parentUri
        );
      }
    },
  },
  template: `
  <div 
    id="index-mobile" 
    :class="{ 'no-pointer': !store.state.viewfinder.isIndexMobileOpen }"
  >
    <div 
      v-if="store.state.viewfinder.isIndexMobileOpen" 
      id="index-mobile__background" 
      @click="toggleIsOpen"
    ></div>
    <ul v-if="store.state.viewfinder.isIndexMobileOpen" id="menu-mobile">
      <li>
        <span class="wrapper" :class="{ 'lock-active': isSameUrl(currentSpace) }">
          <button class="text-2 border index__button">
            <a :href="currentSpace.url">tout</a>
          </button>
        </span>
      </li>
      <li
        v-for="(child, childIndex) in currentSpace.children"
        v-if="currentSpace"
      >
        <span 
          class="wrapper"
          :class="{ 'lock-active': isUriIncludedInUrl(child) }"
        >
          <button
            class="text-2 border index__button"
            tabindex="-1"
          >
            <a 
              :href="child.url"
              :title="'Aller à la page ' + child.title + '.'"
              class="index__link"
              :class="{ 'index__link--full': child.children === null }"
            >{{ child.title }}</a>
          </button>
          
          <span 
            v-if="child.hasChildren"
            class="menu__arrow | text-2 border"
            tabindex="-1"
          ><a 
            @click="navTo(child.uri)"
            :title="'Aller à la page ' + child.title + '.'"
          >-></a></span>
        </span>
      </li>
    </ul>
    <div
        id="current-section"
        class="| text-2 lock-active"
        :class="{ 'current-section--open': store.state.viewfinder.isIndexMobileOpen }"
        title="Chercher une page du site."
        ref="current-section"
      >
        <button 
          v-if="store.state.viewfinder.isIndexMobileOpen" 
          @click="currentSpace.parentUri ? navTo(currentSpace.parentUri): toggleIsOpen()" 
          id="current-section__return" 
          class="no-hover border"><span class="menu__arrow" v-if="currentSpace.parentUri"><-</span></button>
        <button @click="toggleIsOpen" id="current-section__title" class="text-2 border" :class="{'current-section__title-active': store.state.viewfinder.isIndexMobileOpen}">{{ store.state.viewfinder.isIndexMobileOpen ? currentSpace.title : 'naviguer'}}</button>
        <button v-if="store.state.viewfinder.isIndexMobileOpen" @click="toggleIsOpen" id="current-section__close" class="no-hover border"><img id="current-section__close-image" src="/assets/svg/close.svg" /></button>
      </div>
  </div>
  `,
  methods: {
    toggleIsOpen: function () {
      document.body.classList.toggle("no-scroll");
      document.querySelector("#app").classList.toggle("no-scroll");
      document.querySelector(".blocks").classList.toggle("no-scroll");
      this.currentSpaceUri = this.pageUri;
      this.store.state.viewfinder.isIndexMobileOpen =
        !this.store.state.viewfinder.isIndexMobileOpen;
      this.store.state.viewfinder.isActive =
        !this.store.state.viewfinder.isActive;
    },
    navTo: function (uri) {
      if (!uri) return;
      this.currentSpaceUri = uri;
    },
    isSameUrl: function (space) {
      return (
        space.url === window.location.href ||
        space.url + "/" === window.location.href
      );
    },
    isUriIncludedInUrl: function (space) {
      // means that is current space or parent of the current space
      return window.location.href.includes(space.uri);
    },
    hideOnScrollDown: function () {
      if (this.store.state.viewfinder.isIndexMobileOpen) return;
      const windowY = window.scrollY;
      const currentSection = this.$refs["current-section"];
      if (this.scrollPos <= 0) {
        currentSection.classList.remove("current-section--hide");
      } else if (windowY < this.scrollPos) {
        // Scrolling UP
        currentSection.classList.remove("current-section--hide");
      } else {
        // Scrolling DOWN
        currentSection.classList.add("current-section--hide");
      }
      this.scrollPos = windowY;
    },
  },
  mounted() {
    window.addEventListener(
      "scroll",
      this.store.debounce(this.hideOnScrollDown)
    );
  },
};

export default IndexMobile;
