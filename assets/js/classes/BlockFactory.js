class Block {
  constructor(params) {
    this.type = "";
    this.content = {
      zindex: 0,
      isedit: "true",
      width: "500px",
      height: "450px",
      transform: "",
    };

    this.isNew = true;
    this.isEdited = false;
    this.isHighlight = false;
    this.isLocal = true;
    this.id = params.id || Date.now();

    this.content.transform = `translate(${
      params.left || this.getCenterCoordinates().x
    }px, ${params.top || this.getCenterCoordinates().y}px)`;
  }

  getCenterCoordinates() {
    const halfBlockHeight = parseInt(this.content.height, 10) / 2;
    const halfViewHeight = window.innerHeight / 2;

    const halfBlockWidth = parseInt(this.content.width, 10) / 2;
    const halfViewWidth = window.innerWidth / 2;

    const XCenterPos = window.scrollX + (halfViewWidth - halfBlockWidth);
    const YCenterPos = window.scrollY + (halfViewHeight - halfBlockHeight);

    return {
      x: XCenterPos,
      y: YCenterPos,
    };
  }
}

class BlockImage extends Block {
  constructor(params) {
    super(params);

    this.type = "image";
    this.content.caption = params.caption || "";
  }
}

class BlockMarkdown extends Block {
  constructor(params) {
    super(params);

    this.type = "markdown";
    this.content.text = params.text || "";
    this.content.refs = params.refs || "";
  }
}

export class BlockFactory {
  createBlock(type, params = {}) {
    switch (type) {
      case "image":
        return new BlockImage(params);
      case "markdown":
        return new BlockMarkdown(params);
      default:
        throw new Error(`Invalid block type: ${type}`);
    }
  }
}
