/*!
 * vue-interactjs v0.1.10 
 * (c) 2021 yoroshikudozo
 * Released under the MIT License.
 */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('vue'), require('interactjs')) :
  typeof define === 'function' && define.amd ? define(['vue', 'interactjs'], factory) :
  (global = global || self, global.VueInteractjs = factory(global.Vue, global.interact));
}(this, (function (Vue, interact) { 'use strict';

  Vue = Vue && Object.prototype.hasOwnProperty.call(Vue, 'default') ? Vue['default'] : Vue;
  interact = interact && Object.prototype.hasOwnProperty.call(interact, 'default') ? interact['default'] : interact;

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  var dragEvents = ["dragstart", "dragmove", "draginertiastart", "dragend"];
  var resizeEvents = ["resizestart", "resizemove", "resizeinertiastart", "resizeend"];
  var dropEvents = ["dropactivate", "dropdeactivate", "dragenter", "dragleave", "dropmove", "drop"];
  var gestureEvents = ["gesturestart", "gesturemove", "gestureend"];
  var pointerEvents = ["down", "move", "up", "cancel", "tap", "doubletap", "hold"];

  var bindEvents = function bindEvents(events) {
    return function (interact, emit) {
      events.forEach(function (eventName) {
        interact.on(eventName, function () {
          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          emit.apply(void 0, [eventName].concat(args));
        });
      });
    };
  };

  var bindPointerEvents = bindEvents(pointerEvents);
  var bindDragEvents = bindEvents(dragEvents);
  var bindDropEvents = bindEvents(dropEvents);
  var bindResizeEvents = bindEvents(resizeEvents);
  var bindGestureEvents = bindEvents(gestureEvents);

  var InteractInstance = "InteractInstance";
  function getInteractComponent(interact) {
    return Vue.extend({
      name: "Interact",
      props: {
        draggable: {
          type: Boolean,
          required: false
        },
        dragOption: {
          type: Object,
          default: function _default() {
            return {};
          }
        },
        resizable: {
          type: Boolean,
          required: false
        },
        resizeOption: {
          type: Object,
          default: function _default() {
            return {};
          }
        },
        droppable: {
          type: Boolean,
          required: false
        },
        dropOption: {
          type: Object,
          default: function _default() {
            return {};
          }
        },
        gesturable: {
          type: Boolean,
          required: false
        },
        gestureOption: {
          type: Object,
          default: function _default() {
            return {};
          }
        },
        tag: {
          type: String,
          default: "div"
        }
      },
      data: function data() {
        return _defineProperty({}, InteractInstance, null);
      },
      computed: {
        interactInstance: {
          cache: false,
          set: function set(interact) {
            this[InteractInstance] = interact;
          },
          get: function get() {
            return this[InteractInstance];
          }
        }
      },
      watch: {
        draggable: function draggable() {
          this.reset();
        },
        resizable: function resizable() {
          this.reset();
        },
        gesturable: function gesturable() {
          this.reset();
        },
        droppable: function droppable() {
          this.reset();
        }
      },
      mounted: function mounted() {
        this.init();
      },
      destroyed: function destroyed() {
        this.destroy();
      },
      methods: {
        init: function init() {
          if (!this.interactInstance) this.initInteract();
          if (this.draggable) this.initDrag();
          if (this.resizable) this.initResize();
          if (this.droppable) this.initDrop();
          if (this.gesturable) this.initGesture();
        },
        initInteract: function initInteract() {
          this.interactInstance = interact(this.$el);
          bindPointerEvents(this.interactInstance, this.$emit.bind(this));
          this.$emit("ready", this.interactInstance);
        },
        initDrag: function initDrag() {
          var _a;

          (_a = this.interactInstance) === null || _a === void 0 ? void 0 : _a.draggable(this.dragOption);
          bindDragEvents(this.interactInstance, this.$emit.bind(this));
        },
        initResize: function initResize() {
          var _a;

          (_a = this.interactInstance) === null || _a === void 0 ? void 0 : _a.resizable(this.resizeOption);
          bindResizeEvents(this.interactInstance, this.$emit.bind(this));
        },
        initDrop: function initDrop() {
          var _a;

          (_a = this.interactInstance) === null || _a === void 0 ? void 0 : _a.dropzone(this.dropOption);
          bindDropEvents(this.interactInstance, this.$emit.bind(this));
        },
        initGesture: function initGesture() {
          var _a;

          (_a = this.interactInstance) === null || _a === void 0 ? void 0 : _a.gesturable(this.gestureOption);
          bindGestureEvents(this.interactInstance, this.$emit.bind(this));
        },
        reset: function reset() {
          this.destroy();
          this.init();
        },
        destroy: function destroy() {
          var _a;

          (_a = this.interactInstance) === null || _a === void 0 ? void 0 : _a.unset();
          this.interactInstance = null;
        }
      },
      render: function render(createElement) {
        return createElement(this.tag, {
          staticClass: "interact"
        }, this.$slots.default);
      }
    });
  }

  var version = "0.1.10";

  var install = function install(Vue) {
    var InteractComponent = getInteractComponent(interact);
    Vue.component("Interact", InteractComponent);
  };

  var plugin = {
    install: install,
    version: version
  };

  if (typeof window !== "undefined" && window.Vue) {
    window.Vue.use(plugin);
  }

  return plugin;

})));
