const { src, dest } = require("gulp");
const cssnano = require("gulp-cssnano");
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const cssimport = require("gulp-cssimport");

cssimportOptions = {};

exports.default = function () {
  return src("assets/css/praticable/style.css")
    .pipe(cssimport(cssimportOptions))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cssnano())
    .pipe(rename("style.min.css"))
    .pipe(dest("assets/dist"));
};
